<?php

use Illuminate\Database\Seeder;

class BrokersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brokers')->truncate();
        DB::table('brokers')->insert([
        	[
            	'first_name' => 'Health',
            	'last_name' => 'Choice',
	            'email' => 'broker@healthchoice.com',
	            'password' => bcrypt('123456'),
	            'agency_name' => 'Star Health',
	            'agency_address_line_1' => '5th Street Main Lane',
	            'agency_address_line_2' => 'Washington Dc',
	            'city' => 'Miami',
	            'state_id' => '1',
	            'phone' => '67789900054',
	            'phone_tollfree' => '170043533322',
	            'website' => 'www.healthapp.com',
	            'payment_status' => '1',
	            'status' => 'APPROVED',
	            'broker_subscription_plan_id' => '1',
    	    ],
    	]);
    }
}
