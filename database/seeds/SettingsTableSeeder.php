<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
        	[
            	'key' => 'authorizenet_login_id',
            	'value' => '5Sym98MpFV'
    	    ],
    	    [
            	'key' => 'authorizenet_transaction_key',
            	'value' => '8H432T4hdLq94Rm4'
    	    ],
    	    [
            	'key' => 'authorizenet_key',
            	'value' => 'Simon'
    	    ],
            [
                'key' => 'authorizenet_environment',
                'value' => '0'
            ],
            [
                'key' => 'authorizenet_sandbox_api',
                'value' => 'https://apitest.authorize.net/xml/v1/request.api'
            ],
            [
                'key' => 'authorizenet_production_api',
                'value' => 'https://api.authorize.net/xml/v1/request.api'
            ],
    	]);
    }
}
