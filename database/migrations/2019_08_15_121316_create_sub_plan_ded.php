<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubPlanDed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_plan_deds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('SUBPLAN_ID')->nullable();
            $table->decimal('DEDUCTIBLE_SINGLE_C',19,4)->nullable();
            $table->decimal('DEDUCTIBLE_FAMILY_C',19,4)->nullable();
            $table->enum('DEDUCTIBLE_EMBEDDED_B',['Yes','No'])->nullable();
            $table->decimal('OOP_SINGLE_C',19,4)->nullable();
            $table->decimal('OOP_INDIVIDUAL_C',19,4)->nullable();
            $table->decimal('OOP_FAMILY_C',19,4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_plan_deds');
    }
}
