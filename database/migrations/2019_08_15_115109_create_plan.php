<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('PRODUCT_ID')->nullable();
            $table->integer('CARRIER_ID')->nullable();
            $table->string('HIOS_PRODUCT_ID')->nullable();
            $table->string('PLAN_NAME_T')->nullable();
            $table->integer('MAX_DEP_2PARENT_I')->nullable();
            $table->integer('MAX_DEP_1PARENT_I')->nullable();
            $table->integer('DEP_STARTING_AGE_I')->nullable();
            $table->integer('DEP_MAX_AGE_I')->nullable();
            $table->integer('MAX_CHILD_ONLY_I')->nullable();
            $table->enum('DOM_PART_SAME_SUBSCRIBER_B',['Yes','No'])->nullable();
            $table->enum('SAME_SEX_SAME_SUBSCRIBER_B',['Yes','No'])->nullable();
            $table->integer('FAMILY_START_AT_I')->nullable();
            $table->longtext('PROVIDER_LOOKUP_URL_M')->nullable();
            $table->longtext('FORMULARY_LOOKUP_URL_M')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
