<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokerCarrierManagements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broker_carrier_managements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('broker_id')->nullable();
            $table->integer('carrier_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->text('enrollment')->nullable();
            $table->text('apply_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
