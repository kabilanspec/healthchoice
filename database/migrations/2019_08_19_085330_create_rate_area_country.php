<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateAreaCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_area_countries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('RATE_AREA_ID')->nullable();
            $table->integer('COUNTY_ID')->nullable();
            $table->string('SPECIFIC_ZIP_ONLY_T')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_area_countries');
    }
}
