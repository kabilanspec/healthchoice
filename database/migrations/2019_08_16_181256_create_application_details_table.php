<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('application_id')->nullable();
            $table->string('dob')->nullable();
            $table->enum('gender',['male','female'])->nullable();
            $table->enum('is_smoker',['yes','no'])->nullable();
            $table->enum('who',['applicant','spouse' , 'child'])->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_details');
    }
}
