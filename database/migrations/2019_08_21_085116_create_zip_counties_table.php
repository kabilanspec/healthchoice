<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZipCountiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zip_counties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('COUNTY_ID')->nullable();
            $table->integer('STATE_ID')->nullable();
            $table->string('CITY_T')->nullable();
            $table->string('ZIP_CODE_T')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zip_counties');
    }
}
