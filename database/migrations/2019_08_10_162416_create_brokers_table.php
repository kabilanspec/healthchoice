<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brokers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('email',50)->unique();
            $table->string('password');
            $table->string('agency_name',50);
            $table->string('agency_address_line_1');
            $table->string('agency_address_line_2')->nullable();
            $table->string('city',50);
            $table->integer('state_id');
            $table->bigInteger('phone');
            $table->bigInteger('phone_tollfree')->nullable();
            $table->string('website');
            $table->enum('payment_status',['PAID','UNPAID'])->default('UNPAID');
            $table->integer('broker_subscription_plan_id')->nullable();
            $table->enum('status',['APPROVED','UNAPPROVED'])->default('UNAPPROVED');
            $table->text('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brokers');
    }
}
