<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('PLAN_ID')->nullable();
            $table->string('HIOS_PLAN_ID')->nullable();
            $table->string('NETWORK_CODE_T')->nullable();
            $table->string('SERVICE_AREA_CODE_T')->nullable();
            $table->string('SUBPLAN_NAME_T')->nullable();
            $table->string('COINSURANCE_T')->nullable();
            $table->tinyInteger('HSA_B')->nullable();
            $table->string('METAL_LEVEL_T')->nullable();
            $table->string('NETWORK_NAME_T')->nullable();
            $table->enum('EXCHANGE_STATUS_T',['ON','OFF','ON/OFF'])->nullable();
            $table->enum('CATASTROPHIC_B',[1,0])->nullable();
            $table->tinyInteger('OFFICE_VISIT_COPAY_B')->nullable();
            $table->tinyInteger('RX_COPAY_B')->nullable();
            $table->text('OV_PRIMARY_CARE_M')->nullable();
            $table->text('OV_SPECIALIST_M')->nullable();
            $table->text('OV_OTHER_M')->nullable();
            $table->text('OV_PREVENTATIVE_M')->nullable();
            $table->text('TEST_DIAGNOSTIC_M')->nullable();
            $table->text('TEST_IMAGING_M')->nullable();
            $table->text('RX_GENERIC_M')->nullable();
            $table->text('RX_FORMULARY_M')->nullable();
            $table->text('RX_NONFORMULARY_M')->nullable();
            $table->text('RX_SPECIALTY_M')->nullable();
            $table->text('OUTPATIENT_FACILITY_M')->nullable();
            $table->text('OUTPATIENT_PHYS_FEES_M')->nullable();
            $table->text('ER_ER_M')->nullable();
            $table->text('ER_TRANSPORT_M')->nullable();
            $table->text('ER_URGENT_M')->nullable();
            $table->text('HOSPITAL_FACILITY_M')->nullable();
            $table->text('HOSPITAL_PHYSSUR_M')->nullable();
            $table->text('MH_OUTPATIENT_M')->nullable();
            $table->text('MH_INPATIENT_M')->nullable();
            $table->text('MH_SUBSTANCE_OUT_M')->nullable();
            $table->text('MH_SUBSTANCE_IN_M')->nullable();
            $table->text('BABY_PREPOST_M')->nullable();
            $table->text('BABY_DELIVER_IN_M')->nullable();
            $table->text('RECOVER_HOMEHEALTH_M')->nullable();
            $table->text('RECOVER_REHAB_M')->nullable();
            $table->text('RECOVER_HABIL_M')->nullable();
            $table->text('RECOVER_SKILLNURSE_M')->nullable();
            $table->text('RECOVER_MEDEQUIP_M')->nullable();
            $table->text('RECOVER_HOSPICE_M')->nullable();
            $table->text('PED_EYEEXAM_M')->nullable();
            $table->text('PED_GLASSES_M')->nullable();
            $table->text('PED_DENTAL_M')->nullable();
            $table->string('SBC_PATH_T')->nullable();
            $table->tinyInteger('ACTIVE_B')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_plans');
    }
}
