<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('SUBPLAN_ID')->nullable();
            $table->integer('RATE_AREA_ID')->nullable();
            $table->dateTime('RATE_EFFECTIVE_D')->nullable();
            $table->dateTime('RATE_EXPIRATION_D')->nullable();
            $table->integer('RATE_AGE_MIN_I')->nullable();
            $table->integer('RATE_AGE_MAX_I')->nullable();
            $table->decimal('RATE_SINGLE_C',19,4)->nullable();
            $table->decimal('RATE_SINGLE_TOBACCO_C',19,4)->nullable();
            $table->decimal('RATE_FAMILY_C',19,4)->nullable();
            $table->decimal('RATE_FAMILY_TOBACCO_C',19,4)->nullable();
            $table->enum('RATE_GENDER_T',['M','F','N/A'])->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
