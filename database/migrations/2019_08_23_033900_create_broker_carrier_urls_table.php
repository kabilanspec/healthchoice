<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokerCarrierUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broker_carrier_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('broker_id');
            $table->integer('carrier_id');
            $table->integer('state_id');
            $table->string('enrollment');
            $table->string('carrier_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broker_carrier_urls');
    }
}
