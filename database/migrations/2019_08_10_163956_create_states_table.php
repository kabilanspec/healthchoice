<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('postal_code');
            $table->string('full_name');
            $table->integer('state_use_gender');
            $table->string('state_code_t');
            $table->integer('create_by');
            $table->datetime('create_at');
            $table->integer('update_by');
            $table->datetime('update_at');
            $table->string('delete_by')->nullable();
            $table->datetime('delete_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
