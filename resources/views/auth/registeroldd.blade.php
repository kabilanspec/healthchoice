
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>HealthChoice - Individual Policy </title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('landing/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom fonts for this template -->
	  <script src="https://kit.fontawesome.com/d8c4db5f0e.js"></script>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
	
  <!-- Plugin CSS -->
  <link rel="stylesheet" href="{{asset('device-mockups/device-mockups.min.css')}}">

  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="{{asset('css/new-age.css')}}">
  <!-- <link href="css/new-age.css" rel="stylesheet"> -->

	<script>
	// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.getElementById("scroll_button").style.display = "block";
  } else {
    document.getElementById("scroll_button").style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}</script>
	
	
</head>

<body id="page-top">
	<a class=" js-scroll-trigger" id="scroll_button" title="Go to top" href="#page-top"><i class="fas fa-angle-double-up scroll_icon"></i></a>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="{{asset('img/honeycomb.svg')}}" width="49px" height="25px" alt="HealthChoice" class="brand-logo">HealthChoice</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#new_tag">Updates</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#features">Features</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#subscribe">Subscribe</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Sign Up</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-lg-7 my-auto">
          <div class="header-content mx-auto">
            <h1 class="mb-5">Provide your clients and their employees with an all-in-one individual policy search and application tool.</h1>
            <a href="#subscribe" class="btn btn-outline btn-xl js-scroll-trigger">Subscribe to HealthChoice!</a>
          </div>
        </div>
        <div class="col-lg-5 my-auto">
          <div class="device-container">
            <div class="device-mockup ipad_pro portrait gold">
              <div class="device">
                <div class="screen">
                  <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                  <img src="{{asset('img/screen_search_results.png')}}" class="img-fluid" alt="">
                </div>
                <div class="button">
                  <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>


  <!-- About -->
  <section class="page-section" id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">About</h2>
          <h3 class="section-subheading text-muted mb-5">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
           <form class="loginSectionform" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                <div class="form-group">
                    <label>
                        <p class="label-txt">Phone Number</p>
                        <input type="text" class="form-control" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Password</p>
                        <input type="password" class="form-control" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input type="text" class="input readOnly" placeholder="" readonly>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">I agree to the Terms and Conditions</label>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="submitBtn">Register</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </section>
  <section class="download bg-primary text-center" id="new_tag">
    <div class="container">
      <div class="row">
        <div class="col-md-8 mx-auto offset-xl-3">
          <h2 class="section-heading">Discover what all the buzz is about!</h2>
        <p class="text-muted">Sign up for our newsletter to get updates on state expansion and new features!</p>
			<form>
				<div class="input-group input-group-email my-2">
				  <input type="email" class="form-control form-control-email" aria-label="Default" aria-describedby="ContactListEmail2" placeholder="email@address.com">
			  	<div class="input-group-append"><button type="submit" class="btn btn-outline-email btn-xl">Receive Updates</button></div>
				</div><small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			</form>
        </div>
      </div>
    </div>
  </section>

  <section class="features" id="features">
    <div class="container">
      <div class="section-heading text-center">
        <h2>Unlimited Features, Unlimited Fun</h2>
        <p class="text-muted">Check out what you can do with this app theme!</p>
        <hr>
      </div>
      <div class="row">
        <div class="col-lg-4 my-auto">
          <div class="device-container">
            <div class="device-mockup iphone6_plus portrait white">
              <div class="device">
                <div class="screen">
                  <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                  <img src="{{asset('img/demo-screen-2.jpg')}}" class="img-fluid" alt="">
                </div>
                <div class="button">
                  <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8 my-auto">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="fas fa-tablet-alt text-primary"></i>
                  <h3>Responsive Design</h3>
                  <p class="text-muted">Laptop, tablet, or even a phone - your clients can request and review quotes!</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="fas fa-cogs text-primary"></i>
                  <h3>Customize Results</h3>
                  <p class="text-muted">On and/or Off Exchange Plans, Select Carriers</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="fas fa-paper-plane text-primary"></i>
                  <h3>Quote Dashboard</h3>
                  <p class="text-muted">Review quote history and resend emailed comparisons.</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="feature-item">
                  <i class="fas fa-lock-open text-primary"></i>
                  <h3>Open Source</h3>
                  <p class="text-muted">Since this theme is MIT licensed, you can use it commercially!</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="cta" id="subscribe">
    <div class="cta-content">
      <div class="container">
        <h2>Stop waiting.<br>Start building.</h2>
        <a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Let's Get Started!</a>
      </div>
    </div>
    <div class="overlay"></div>
  </section>

  <section class="contact bg-primary" id="contact">
    <div class="container">
      <h2>We
        <i class="fas fa-heart"></i>
        new friends!</h2>
      <ul class="list-inline list-social">
        <li class="list-inline-item social-twitter">
          <a href="#">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li class="list-inline-item social-facebook">
          <a href="#">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li class="list-inline-item social-linkedin">
          <a href="#">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
      </ul>
    </div>
  </section>

  <footer>
    <div class="container">
		<img src="{{asset('img/flyteHCM_Portal.png')}}" width="200px" height="49px" alt="" style="margin: 10px;"/>
      <p>&copy; Flyte HCM, LLC 2019. All Rights Reserved.</p>
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#PrivacyModal">Privacy</a>
        </li>
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#TermsModal">Terms</a>
        </li>
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#FAQModal">FAQ</a>
        </li>
      </ul>
    </div>
  </footer>
	
<div class="modal" tabindex="-1" role="dialog" id="FAQModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Frequently Asked Questions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
		  <p>Duis dolor cupim pork belly fugiat non. Occaecat ex sunt quis capicola. Esse rump ground round, est chicken prosciutto sed in dolor elit pig. Minim ut labore strip steak voluptate dolore in excepteur corned beef tongue tenderloin magna.</p>
		  <p>Elit aliqua excepteur veniam turducken. Picanha salami short ribs, ball tip proident veniam doner jerky fatback pork loin pancetta jowl. Minim cupim picanha, tempor non pork cillum buffalo laborum officia. Consectetur anim tenderloin, consequat laboris spare ribs nostrud non. Rump tri-tip ham, shankle in ground round tail id beef. Cillum chuck shoulder drumstick.</p>
		  <p>Aute andouille et est, ullamco dolore incididunt reprehenderit ut aliqua nisi tongue shankle. Incididunt ea burgdoggen bacon kevin shank nostrud. Dolor quis frankfurter sausage, kielbasa elit sunt landjaeger in flank fatback velit pork esse do. Ipsum kevin eu sunt shankle spare ribs, flank irure alcatra brisket capicola turducken boudin chicken ut.</p>
		  <p>Pork loin ut pariatur, beef ribs do dolore sint biltong ipsum buffalo tail. Ut porchetta do tempor ullamco, meatball kevin meatloaf chicken deserunt. Voluptate salami occaecat reprehenderit leberkas. Brisket excepteur beef esse. Laborum proident t-bone, adipisicing leberkas velit tri-tip pork loin commodo. Flank shankle duis jowl aliqua in doner fugiat voluptate turducken. Ut incididunt laboris, ex salami fugiat picanha hamburger.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	
<div class="modal" tabindex="-1" role="dialog" id="PrivacyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Privacy Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
		
<div class="modal" tabindex="-1" role="dialog" id="TermsModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Terms &amp; Conditions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	
  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('landing/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('landing/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Plugin JavaScript -->
  <script src="{{asset('landing/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{asset('js/new-age.min.js')}}"></script>

</body>

</html>
