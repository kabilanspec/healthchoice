@extends('layouts.auth')

@section('content')


            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <img src="{{url('asset/img/loginimg.svg')}}" class="img-fluid mobHide">
                        </div>
                        <div class="col-md-5">
                            <div class="text-center"><img src="{{url('asset/img/logo.png')}}" class="img-fluid"></div>
                            <form class="loginSectionform">
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Phone Number</p>
                                        <input type="text" class="input">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Password</p>
                                        <input type="password" class="input">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="forgetSec">
                                    <a href="#" class="float-left">Forget Password?</a>
                                    <a href="#" class="float-right">Forget Account?</a>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="submitBtn">Login</button>
                                </div>
                            </form>
                            <div class="text-center">
                                <h6 class="logFormh6">Login With</h6>
                                <a href="#" class="weChart"><i class="fab fa-weixin"></i></a>
                                <a href="#" class="qqChart"><i class="fab fa-qq"></i></a>
                                <a href="#" class="weiBo"><i class="fab fa-weibo"></i></a>
                            </div>
                            <p class="dontHave">Don't have an Account? <a href="{{url('register')}}">Create your Account</a></p>
                            <div class="copyRight text-center"><p>QueenBee © 2019 . All Rights Reserved.</p></div>
                        </div>
                    </div>
                </div>
            </section>



<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
