@extends('layouts.new')

@section('content')

<section>
<div class="container">
    <div class="row">

        <div class="col-md-8">
     
            <form class="loginSectionform" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                <div class="form-group">
                    <label>
                        <p class="label-txt">Phone Number</p>
                        <input type="text" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Password</p>
                        <input type="password" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input type="text" class="input readOnly" placeholder="" readonly>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">I agree to the Terms and Conditions</label>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="submitBtn">Register</button>
                </div>
            </form>
            <p class="dontHave">Do you have an Account? <a href="{{url('login')}}">Login</a></p>
            <div class="copyRight text-center"><p>QueenBee © 2019 . All Rights Reserved.</p></div>
        </div>
    </div>
</div>
</section>
  


<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
