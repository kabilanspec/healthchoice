@extends('admin.layout.base')

@section('title', 'Add Broker')

@section('content')

  <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Overview</span>
          <h3 class="page-title">Add New Broker</h3>
      </div>
  </div>
   <div class="row">
                        <div class="col-sm-12 col-md-6">
                          <!-- <strong class="text-muted d-block mb-2">Forms</strong> -->
                          <form class="form-horizontal" action="{{route('admin.broker.update')}}" method="POST" enctype="multipart/form-data" role="form">
                          {{csrf_field()}}
                            <div class="form-group">
                              <input type="hidden" value="{{ $id }}" name="broker_id">
                              <input type="text" class="form-control" id=""  name="first_name" placeholder="First Name" value="{{ @$broker->first_name }}"> </div>
                              <div class="form-group">
                              <input type="email" class="form-control" id="" name="email" placeholder="Email Address" value="{{ @$broker->email }}"> </div>
                             
                            <div class="form-group">
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" name="phone" value="{{ @$broker->phone }}" placeholder="Phone Number" aria-label="Username" aria-describedby="basic-addon1"> </div>
                            </div>
                             <!-- <div class="form-group">
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1"> </div>
                            </div> -->
                           
                            <div class="form-group">
                              <input type="text" class="form-control" id="inputAddress" name="agency_address_line_1" placeholder="Agency Address Line 1" value="{{ @$broker->agency_address_line_1 }}"> </div>
                            <div class="form-row">
                              <div class="form-group col-md-7">
                                <input type="text" class="form-control" name="city" placeholder="City"  id="inputCity" value="{{ @$broker->city }}"> </div>
                              <div class="form-group col-md-5">
                                <select id="inputState" name="state_id" class="form-control">
                                  <option selected>Choose State...</option>
                                  @foreach($state  as $index=>$states)
                                  <option @if($broker->state_id == $states->id) selected @endif value="{{ $states->id }}">{{  $states->full_name }}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                               
                            </div>
                         
                        </div>
                        <div class="col-sm-12 col-md-6">
                          <!-- <strong class="text-muted d-block mb-2">Form Validation</strong> -->
                            <div class="form-group">
                              <input type="text" class="form-control" id="" name="last_name" placeholder="Last Name" value="{{ @$broker->last_name }}"> </div>
                              <!-- <div class="form-group">
                              <input type="text" class="form-control" name="agency_name" id="" placeholder="Agency Name" value=""> </div> -->
                              <div class="form-group">
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" name="phone_tollfree" placeholder="Phone - Toll Free" aria-label="Username" aria-describedby="basic-addon1" value="{{ @$broker->phone_tollfree }}"> </div>
                            </div>
                            <!-- <div class="form-row">
                              <div class="form-group col-md-6">
                                <input type="text" class="form-control is-valid" id="validationServer01" placeholder="First name" value="Catalin" required>
                                <div class="valid-feedback">The first name looks good!</div>
                              </div>
                              <div class="form-group col-md-6">
                                <input type="text" class="form-control is-valid" id="validationServer02" placeholder="Last name" value="Vasile" required>
                                <div class="valid-feedback">The last name looks good!</div>
                              </div>
                            </div> -->
                            <!-- <div class="form-group">
                              <input type="text" class="form-control" name="website" id="" placeholder="Website Name" value=""> </div> -->
                            <!-- <div class="form-group">
                              <input type="text" class="form-control is-invalid" id="validationServer03" placeholder="Username" value="catalin.vasile" required>
                              <div class="invalid-feedback">This username is taken.</div>
                            </div> -->
                            <div class="form-group">
                              <input type="text" class="form-control" name="agency_address_line_2" value="{{ @$broker->agency_address_line_2 }}" id="inputAddress" placeholder="Agency Address Line 2" value=""> </div>
                            <!-- <div class="form-group">
                              <select class="form-control is-invalid">
                                <option selected>Choose...</option>
                                <option>...</option>
                              </select>
                              <div class="invalid-feedback">Please select your state.</div>
                            </div> -->
                            
                            
                            <div class="form-group">
                              <button class="btn btn-info btn-block">Submit</button> </div>
                          </form>
                        </div>
                      </div>
 

@endsection
