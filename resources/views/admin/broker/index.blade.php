@extends('admin.layout.base')

@section('title', 'Dashboard ')
@section('styles')
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection
@section('content')

  <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Overview</span>
          <h3 class="page-title">Brokers</h3>
          
      </div>
      <!-- <div class="col-12 col-sm-8 text-center text-sm-right mb-0">
        <a href="{{route('admin.broker.create')}}"> <button class="btn btn-info">Add New Broker</button> </a>
      </div> -->
      
  </div>
 
  <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Brokers</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0" id="table_id">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">Broker Unique ID</th>
                          <th scope="col" class="border-0">First Name</th>
                          <th scope="col" class="border-0">Last Name</th>
                          <th scope="col" class="border-0">Email</th>
                          <th scope="col" class="border-0">Agency</th>
                          <th scope="col" class="border-0">Carriers</th>
                          <th scope="col" class="border-0">Actions</th>
                          <th scope="col" class="border-0">Status</th>
                         </tr>
                      </thead>
                      <tbody>
                        @foreach($brokers as $index => $broker)
                         <tr>
                           <td>HC989878767</td>
                           <td>{{ $broker->first_name }}</td>
                           <td>{{ $broker->last_name }}</td>
                           <td>{{ $broker->email }}</td>
                           <td>{{ $broker->agency_name }}</td>
                           
                           <td><a href="" data-toggle="modal" data-target="#schedule_modal_{{ $broker->id }}">View</a></td>
                           <td><a class="btn btn-info" href="{{url('admin/edit/broker/'.$broker->id)}}">Edit</a>&nbsp;
                            <form action="{{ route('admin.broker.destroy', $broker->id) }}" method="POST">
                              {{ csrf_field() }}
                              <input type="hidden" name="_method" value="DELETE"><button class="btn btn-danger look-a-like" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i>Delete</button></form></td>
                           <td><label class="switch">
                                <input @if($broker->status == 'APPROVED') checked @endif type="checkbox" value="{{ $broker->id }}">
                                <span class="slider"></span>
                              </label></td>
                         </tr>
                         <div id="schedule_modal_{{ $broker->id }}" class="modal fade schedule-modal" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  
                                  <h4 class="modal-title"></h4>
                                </div>
                                
                                <div class="modal-body">
                                  
                                 <div>
                                   <h5>Chosen Carriers</h5>
                                   @foreach($broker->brokerc as $key1 => $bc)
                                   <h7>{{ $bc->carrier->carrier  }}</h7><br>&nbsp;
                                   @endforeach
                                 </div>
                                </div>
                                <div class="modal-footer">
                                 
                                </div>

                              </div>

                            </div>
                          </div>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
 

@endsection
@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
     $(document).on('change', '.switch', function(){
          var id = $(this).parent('td').find('input[type="checkbox"]').val();
          
          if($(this).parent('td').find('input[type="checkbox"]').prop('checked') == true) {
             var status = id+'APPROVED';
             
          }else{
             var status = id+'UNAPPROVED';
            
          }

          $.ajax({url: "{{ url('admin/broker/change/status') }}/"+status,dataType: "json",success: function(data){ 
               console.log(data);
          }}); 
     });
   });
  
</script>
<script type="text/javascript">
  $(document).ready( function () {
    
    $('#table_id').DataTable();
} );
</script>


@endsection
