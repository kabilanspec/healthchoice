@extends('admin.layout.auth')

@section('content')


   <div class="container-fluid">
      <div class="row">
     
      <main class="main-content col">
          <div class="main-content-container container-fluid px-4 my-auto h-100">
            <div class="row no-gutters h-100">
              <div class="col-lg-3 col-md-5 auth-form mx-auto my-auto">
                <div class="card">
                  <div class="card-body">
                    <img class="auth-form__logo d-table mx-auto mb-3" src="images/shards-dashboards-logo.svg" alt="Shards Dashboards - Register Template">
                    <h5 class="auth-form__title text-center mb-4">Access Your Account</h5>
                    <form id="signup" role="form" method="POST" action="{{ url('/admin/login') }}">
                    {{ csrf_field() }}

                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input placeholder="Enter email" type="email" name="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address." autocomplete="off" >
                        @if ($errors->has('email'))
                            <p class="help-block text-danger">{{ $errors->first('email') }}</p>
                        @endif
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                       
                         <input type="password" name="password" class="form-control" id="password" required data-validation-required-message="Password" autocomplete="off" >
                         
                         @if ($errors->has('password'))
                            <p class="help-block text-danger">{{ $errors->first('password') }}</p>
                         @endif


                      </div>


                      <div class="form-group mb-3 d-table mx-auto">
                        <div class="custom-control custom-checkbox mb-1">
                          <input type="checkbox" class="custom-control-input" id="customCheck2">
                          <label class="custom-control-label" for="customCheck2">Remember me for 30 days.</label>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-pill btn-accent d-table mx-auto">Access Account</button>
                    </form>
                  </div>
                <!--   <div class="card-footer border-top">
                    <ul class="auth-form__social-icons d-table mx-auto">
                      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fab fa-github"></i></a></li>
                      <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                    </ul>
                  </div> -->
                </div>
        
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>



@endsection
<script>

</script>
