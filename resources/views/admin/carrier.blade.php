@extends('admin.layout.base')

@section('title', 'Carrier List')
@section('styles')
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')

  <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Overview</span>
          <h3 class="page-title">Carriers</h3>
      </div>
  </div>
           <div class="row">
               <div class="col-sm-12 col-md-4">
                          <!-- <strong class="text-muted d-block mb-2">Forms</strong> -->
                          <form class="form-horizontal" action="{{route('admin.carrier.store')}}" method="POST" enctype="multipart/form-data" role="form">
                          {{csrf_field()}}
                            <div class="form-group">
                              <input type="text" style="height:53px;" class="form-control" id=""  name="carrier" placeholder="Carrier Name" value=""> </div>
                             
                        </div>
                        <div class="col-sm-12 col-md-8">
                        
                              
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <textarea name="description" class="form-control">Description About Carrier</textarea>
                                
                              </div>
                              <div class="form-group col-md-6">
                               <button class="btn btn-lg btn-info btn-block" >Save</button>
                              </div>
                            </div>
                           
                            
                          </form>
                        </div>
                      </div>
                        <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Brokers</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">#</th>
                          <th scope="col" class="border-0">Carrier Name</th>
                          <th scope="col" class="border-0">Description</th>
                          <th scope="col" class="border-0">States</th>
                          <th scope="col" class="border-0">Status</th>
                         </tr>
                      </thead>
                      <tbody>
                        @foreach($carrier as $index => $carriers)
                         <tr>
                           <td>{{ $index+1 }}</td>
                           <td>{{ $carriers->carrier }}</td>
                           <td>{{ $carriers->description }}</td>
                           <td><a href="" data-toggle="modal" data-target="#schedule_modal_{{ $carriers->id }}"> View/Add State Carriers</a></td>
                           <td><label class="switch">
                                <input @if($carriers->status == 'active') checked @endif  type="checkbox" value="{{ $carriers->id }}">
                                <span class="slider"></span>
                              </label></td>
                         </tr>
                         <div id="schedule_modal_{{ $carriers->id }}" class="modal fade schedule-modal" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  
                                  <h4 class="modal-title">State Carriers</h4>
                                </div>
                                
                                <div class="modal-body">
                                  <form action="{{route('admin.statecarrier')}}" method="POST" enctype="multipart/form-data" role="form">
                                    {{csrf_field()}}
                                    <input type="hidden" name="carrier_id" value="{{ $carriers->id }}">
                                  <select  name="state_id" class="form-control">
                                    <option>Choose State</option>
                                    @foreach($states as $key=>$state)
                                    <option value="{{ $state->id }}">{{ $state->full_name }}</option>
                                    @endforeach
                                  </select>
                                  <!-- <input type="hidden" id="stateid{{ $carriers->id }}" name="state_id">
                                  <input type="text" id="choosestate{{ $carriers->id }}" onchange="state({{ $carriers->id }})"  class="form-control" placeholder="State..."  name=""> -->
                                  <button type="submit"  class="btn btn-info" >Add</button>
                                 </form>
                                 <div>
                                   <h5>Chosen States</h5>
                                   @foreach($carriers->statecarrier as $key1 => $sc)
                                   <h7>{{ $sc->state->full_name  }}</h7>&nbsp;<a href="{{url('admin/delete/state/carrier/'.$sc->id)}}"><img src="{{ url('frontend/images/rem.png')}}" width="15px" height="15px"></a><br>&nbsp;
                                   @endforeach
                                 </div>
                                </div>
                                <div class="modal-footer">
                                 
                                </div>

                              </div>

                            </div>
                          </div>
 
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
 
@endsection
@section('scripts')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
     $(document).on('change', '.switch', function(){
          var id = $(this).parent('td').find('input[type="checkbox"]').val();
          
          if($(this).parent('td').find('input[type="checkbox"]').prop('checked') == true) {
             var status = id+'active';
             
          }else{
             var status = id+'inactive';
            
          }

          $.ajax({url: "{{ url('admin/carrier/change/status') }}/"+status,dataType: "json",success: function(data){ 
               console.log(data);
          }}); 
     });
   });
  function statecarrier($id)
  {
     var state  = $("#state_id").val();
     alert(state)
  }
</script>
<!-- <script type="text/javascript">
function state(id)
    {
       $('#choosestate'+id).autocomplete({
         
           source: function(request, response) {
           $.getJSON("/admin/autocomplete",{ term: $('#choosestate'+id).val() }, 
               response).done(function(data){
                //initMap();
               });
           },

           minlength:1,
           autoFocus:true,
           select:function(e,ui)
           {
             var country = $('#choosestate'+id).val();

             if(country!=ui.item.value)
             {
                $('.checks').val('');
             }
             $('#choosestate'+id).val(ui.item.value);
             var id = $('#stateid'+id).val(ui.item.id);
             
            
           }

       });
    }
</script> -->
@endsection
