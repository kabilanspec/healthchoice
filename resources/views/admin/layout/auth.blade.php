<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Title -->
    <title>HealthChoice</title>
    <link rel="shortcut icon" type="image/png" href="{{ Setting::get('site_icon') }}"/>

    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('admin/styles/shards-dashboards.1.1.0.css')}}">
    <link rel="stylesheet" href="{{asset('admin/styles/extras.1.1.0.min.css')}}">

</head>
<body class="h-100">
<div class="container-fluid">
    @yield('content')
</div>

    <script type="text/javascript" src="{{asset('asset/js/jquery.min.js')}}" ></script>
    <script type="text/javascript" src="{{asset('main/vendor/bootstrap4/bootstrap.min.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('main/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('asset/js/sharee.js')}}"></script>
    <script src="{{asset('admin/scripts/extras.1.1.0.min.js')}}"></script>
    <script src="{{asset('admin/scripts/shards-dashboards.1.1.0.min.js')}}" ></script>
    
    </body>
</html>
