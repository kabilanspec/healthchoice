<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HealthChoice</title>

    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('admin/styles/shards-dashboards.1.1.0.css')}}">
    <link rel="stylesheet" href="{{asset('admin/styles/extras.1.1.0.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/styles/css/my.css')}}">
    
    

  </head>
  <body class="h-100">

    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
         @include('admin.include.nav')
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            @include('admin.include.header')
          </div>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            @yield('content')
          </div>
          <footer class="main-footer d-flex p-2 px-3 bg-white border-top">

            <span class="copyright ml-auto my-auto mr-2">Copyright © 2019 Heathchoice
              
            </span>
          </footer>
        </main>
      </div>
    </div>

    <script type="text/javascript" src="{{asset('asset/js/jquery.min.js')}}" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{asset('main/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('asset/js/shards.js')}}"></script>
    <script type="text/javascript" src="{{asset('asset/js/sharee.js')}}"></script>
    <script src="{{asset('admin/scripts/extras.1.1.0.min.js')}}"></script>
    <script src="{{asset('admin/scripts/shards-dashboards.1.1.0.min.js')}}" ></script>
    <script src="{{asset('admin/scripts/app/app-blog-overview.1.1.0.js')}}"></script>
    
    
    @yield('scripts')


  </body>
</html>