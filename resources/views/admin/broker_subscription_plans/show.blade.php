@extends('admin.layout.base')

@section('title', 'Dashboard ')


@section('content')

<style type="text/css">
  .help-block{
     color: red
  }
</style>

 <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <h3 class="page-title">Create Broker Subscription Plans</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
        <!-- Quick Post -->
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">New Plan</h6>
            <div class="col text-right view-report">
              <a href="{{route('admin.broker_subscription_plan')}}">View Plans</a>
          </div>
          </div>
          <div class="card-body d-flex flex-column">
            <form class="quick-post-form" action="{{route('admin.broker_subscription_plan.store')}}" method="POST">
              {{ csrf_field() }}
              <div class="form-group">
                <input type="text" class="form-control" name="name" id="plan_name" value="{{old('name')}}" aria-describedby="emailHelp" placeholder="Plan name"> </div>
                 @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              <div class="form-group">
                <input type="text" class="form-control" name="amount" value="{{old('amount')}}" id="amount" aria-describedby="emailHelp" placeholder="Amount"> </div>
                 @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
              <div class="form-group">
                <textarea class="form-control" name="description"  placeholder="Description">{{old('amount')}}</textarea>
                 @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group mb-0">
                <button type="submit" class="btn btn-accent">Create Plan</button>
              </div>
            </form>
          </div>
        </div>
        <!-- End Quick Post -->
      </div>
      <!-- End Top Referrals Component -->
    </div>

@endsection
