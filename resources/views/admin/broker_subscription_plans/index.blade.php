@extends('admin.layout.base')

@section('title', 'Broker Subscription Plans ')


@section('content')

  <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle"></span>
          <h3 class="page-title">Broker Subscription Plans</h3>
          <a href="{{url('admin/broker_subscription_plan/show')}}">Add</a>
      </div>
  </div>
  <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Broker Subscription Plans</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">#</th>
                          <th scope="col" class="border-0">Name</th>
                          <th scope="col" class="border-0">Amount</th>
                          <th scope="col" class="border-0">Action</th>
                         </tr>
                      </thead>
                      <tbody>
                        @foreach($broker_subscription_plans as $index => $broker_subscription_plan)
                        <tr>
                          <td>{{ $index+1 }}</td>
                          <td>{{ $broker_subscription_plan->name }}</td>
                          <td>{{ $broker_subscription_plan->amount }}</td>
                          <td><a href="{{url('admin/broker_subscription_plan/edit/'.$broker_subscription_plan->id)}}">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{{url('admin/broker_subscription_plan/destroy/'.$broker_subscription_plan->id)}}">Delete</a>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
 

@endsection
