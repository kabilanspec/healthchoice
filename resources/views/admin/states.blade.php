@extends('admin.layout.base')

@section('title', 'Dashboard ')


@section('content')

  <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
          <span class="text-uppercase page-subtitle">Overview</span>
          <h3 class="page-title">US States</h3>
      </div>
  </div>
  <div class="row">
              <div class="col">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">US States</h6>
                  </div>
                  <div class="card-body p-0 pb-3 text-center">
                    <table class="table mb-0">
                      <thead class="bg-light">
                        <tr>
                          <th scope="col" class="border-0">#</th>
                          <th scope="col" class="border-0">State</th>
                          <th scope="col" class="border-0">Status</th>
                         </tr>
                      </thead>
                      <tbody>
                        @foreach($states as $index=>$state)
                        <tr>
                          <td>{{ $index+1 }}</td>
                          <td>{{ $state->full_name }}</td>
                          <td><label class="switch">
                                <input @if($state->status == 'active') checked @endif  type="checkbox" value="{{ $state->id }}">
                                <span class="slider"></span>
                              </label></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
 

@endsection
@section('scripts')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
     $(document).on('change', '.switch', function(){
          var id = $(this).parent('td').find('input[type="checkbox"]').val();
          
          if($(this).parent('td').find('input[type="checkbox"]').prop('checked') == true) {
             var status = id+'active';
             
          }else{
             var status = id+'inactive';
            
          }

          $.ajax({url: "{{ url('admin/state/change/status') }}/"+status,dataType: "json",success: function(data){ 
               console.log(data);
          }}); 
     });
   });
  
</script>

@endsection
