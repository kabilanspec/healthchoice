
  <nav class="navbar navbar-expand-lg navbar-light fixed-top innermainav" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="{{url('broker/dashboard')}}"><img src="{{url('img/honeycomb.svg')}}" width="49px" height="25px" alt="HealthChoice" class="brand-logo">HealthChoice</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#new_tag">Updates</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#features">Features</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#subscribe">Subscribe</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('broker.login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('broker.register') }}">Sign Up</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>