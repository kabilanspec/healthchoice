  <footer>
    <div class="container">
		<img src="{{asset('img/flyteHCM_Portal.png')}}" width="200px" height="49px" alt="" style="margin: 10px;"/>
      <p>&copy; Flyte HCM, LLC 2019. All Rights Reserved.</p>
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#PrivacyModal">Privacy</a>
        </li>
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#TermsModal">Terms</a>
        </li>
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#FAQModal">FAQ</a>
        </li>
      </ul>
    </div>
  </footer>
	
<div class="modal" tabindex="-1" role="dialog" id="FAQModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Frequently Asked Questions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
		  <p>Duis dolor cupim pork belly fugiat non. Occaecat ex sunt quis capicola. Esse rump ground round, est chicken prosciutto sed in dolor elit pig. Minim ut labore strip steak voluptate dolore in excepteur corned beef tongue tenderloin magna.</p>
		  <p>Elit aliqua excepteur veniam turducken. Picanha salami short ribs, ball tip proident veniam doner jerky fatback pork loin pancetta jowl. Minim cupim picanha, tempor non pork cillum buffalo laborum officia. Consectetur anim tenderloin, consequat laboris spare ribs nostrud non. Rump tri-tip ham, shankle in ground round tail id beef. Cillum chuck shoulder drumstick.</p>
		  <p>Aute andouille et est, ullamco dolore incididunt reprehenderit ut aliqua nisi tongue shankle. Incididunt ea burgdoggen bacon kevin shank nostrud. Dolor quis frankfurter sausage, kielbasa elit sunt landjaeger in flank fatback velit pork esse do. Ipsum kevin eu sunt shankle spare ribs, flank irure alcatra brisket capicola turducken boudin chicken ut.</p>
		  <p>Pork loin ut pariatur, beef ribs do dolore sint biltong ipsum buffalo tail. Ut porchetta do tempor ullamco, meatball kevin meatloaf chicken deserunt. Voluptate salami occaecat reprehenderit leberkas. Brisket excepteur beef esse. Laborum proident t-bone, adipisicing leberkas velit tri-tip pork loin commodo. Flank shankle duis jowl aliqua in doner fugiat voluptate turducken. Ut incididunt laboris, ex salami fugiat picanha hamburger.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
	
<div class="modal" tabindex="-1" role="dialog" id="PrivacyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Privacy Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
		
<div class="modal" tabindex="-1" role="dialog" id="TermsModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Terms &amp; Conditions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>