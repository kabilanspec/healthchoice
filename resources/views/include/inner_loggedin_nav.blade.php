
  <nav class="navbar navbar-expand-lg navbar-light fixed-top innermainav" id="mainNav" style="background-color: #FAA61A">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="{{url('broker/dashboard')}}"><img src="{{url('img/honeycomb.svg')}}" width="49px" height="25px" alt="HealthChoice" class="brand-logo">HealthChoice</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="javascript:void(0)">Logs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ url('broker/dashboard') }}">Carrier</a>
          </li>
           <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ url('broker/profile') }}">Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ url('broker/logout') }}">Logout</a>
          </li>
         
          
          
        </ul>
      </div>
    </div>
  </nav>