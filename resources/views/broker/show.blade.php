<body id="page-top">
    <a class=" js-scroll-trigger" id="scroll_button" title="Go to top" href="#page-top"><i class="fas fa-angle-double-up scroll_icon"></i></a>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top innermainav" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="{{url('broker/dashboard')}}"><img src="img/honeycomb.svg" width="49px" height="25px" alt="HealthChoice" class="brand-logo">HealthChoice</a>
    </div>
  </nav>

  @extends('layouts.loggedin')

@section('content')
  <!-- About -->
  <section class="page-section" id="about">
    <div class="container">
        @if (session()->has('flash_success'))
                        <div class="alert alert-success">
                            {!! session()->get('flash_success')!!}        
                        </div>
                @endif
                @if (session()->has('flash_error'))
                        <div class="alert alert-danger">
                            {!! session()->get('flash_error')!!}        
                        </div>
                @endif
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading">Profile Details</h2>
        </div>
      </div>
      <form method="POST" action="{{url('/broker/update_profile/')}}" enctype="multipart/form-data">
         {{csrf_field()}}
      <div class="row">
       
          <div class="col-lg-12 col-md-12 mb-1">
            @if(!empty($broker->avatar))
            <div class="form-group"> <!-- Agency Name -->
                <img src="{{$broker->avatar}}">
            </div>
            @endif
          </div>
          <div class="col-lg-3 col-md-12 mb-1">
            <div class="form-group"> <!-- Agency Name -->
                <label for="agency_name_id" class="control-label">Logo</label>
                <input type="file" class="form-control"  value="" name="avatar" placeholder="Agency Name">
            </div>
          </div>
        
           <div class="col-12 mt-4">
          <h3 class="section-subheading text-muted">Agency Information</h3>
          </div>
        <div class="col-lg-3 col-md-12 mb-1">
            <div class="form-group"> <!-- Agency Name -->
                <label for="agency_name_id" class="control-label">Agency Name*</label>
                <input type="text" class="form-control"  value="{{$broker->agency_name}}" name="agency_name" placeholder="Agency Name" readonly>
                @if ($errors->has('agency_name'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('agency_name') }}</strong>
                    </span>
                @endif
            </div>
          </div>
        <div class="col-lg-6 col-md-12 mb-1">

            <div class="form-group"> <!-- Subdomain -->
                <label for="subdomain_name_id" class="control-label">Website*</label>
                <div class="input-group" >
                <input type="text" class="form-control" id="subdomain_name_id" name="website" placeholder="Agency name" value="{{$broker->website}}" readonly>
                <div class="input-group-append">
                        <span class="input-group-text" id="web-addon">.healthchoice.app</span>
                      </div></div>
                      @if ($errors->has('agency_name'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('agency_name') }}</strong>
                    </span>
                @endif
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">

            <div class="form-group"> <!-- First Name -->
                <label for="first_name_id" class="control-label">First Name*</label>
                <input type="text" class="form-control" id="first_name_id" value="{{$broker->first_name}}" name="first_name" placeholder="First Name" >
                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">

            <div class="form-group"> <!-- Last Name -->
                <label for="last_name_id" class="control-label">Last Name*</label>
                <input type="text" class="form-control" id="last_name_id" value="{{$broker->last_name}}" name="last_name" placeholder="Last Name" >
                @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">

            <div class="form-group"> <!-- Email -->
                <label for="agency_email_id" class="control-label">Agency Email Address</label>
                <input type="text" value="{{$broker->agency_email}}" class="form-control" id="agency_email_id" name="agency_email" placeholder="Email Address" >
                @if ($errors->has('agency_email'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('agency_email') }}</strong>
                    </span>
                @endif
                
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">

            <div class="form-group"> <!-- Local Phone -->
                <label for="local_phone_id" class="control-label">Local Phone*</label>
                <input type="text" class="form-control" id="local_phone_id" name="phone" value="{{$broker->phone}}" placeholder="Local Phone" readonly>
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
                
            </div>  
          </div>
        <div class="col-lg-6 col-md-12 mb-1">               

            <div class="form-group"> <!-- Toll-Free Phone -->
                <label for="toll_free_id" class="control-label">Toll-Free Phone</label>
                <input type="text" class="form-control" value="{{$broker->phone_tollfree}}" id="toll_free_id" name="phone_tollfree" placeholder="Toll-Free Phone" >
               
            </div>
          </div>
        <div class="col-lg-6 col-md-12 mb-1">

            <div class="form-group"> <!-- Street 1 -->
                <label for="street1_id" class="control-label">Street Address 1*</label>
                <input type="text" value="{{$broker->agency_address_line_1}}" class="form-control" id="street1_id" name="agency_address_line_1" placeholder="Street address, P.O. box, company name, c/o" >
                @if ($errors->has('agency_address_line_1'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('agency_address_line_1') }}</strong>
                    </span>
                @endif
            </div>  
          </div>
        <div class="col-lg-6 col-md-12 mb-1">               

            <div class="form-group"> <!-- Street 2 -->
                <label for="street2_id" class="control-label">Street Address 2</label>
                <input type="text" class="form-control" id="street2_id" value="{{$broker->agency_address_line_2}}" name="agency_address_line_2" placeholder="Apartment, suite, unit, building, floor, etc." >
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">

            <div class="form-group"> <!-- City-->
                <label for="city_id" class="control-label">City*</label>
                <input type="text" class="form-control" value="{{$broker->city}}" id="city_id" name="city" placeholder="City" >
                @if ($errors->has('city'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('city') }}</strong>
                    </span>
                @endif
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">

            <div class="form-group"> <!-- State -->
                <label for="state_id" class="control-label">State*</label>
                <select class="form-control" id="state_id" name="state_id" >
                    <option value="">State</option>
                     @foreach($state  as $index => $states)
                      <option value="{{ $states->id }}" @if($broker->state_id == $states->id) {{'selected'}} @endif>{{  $states->full_name }}</option>
                      @endforeach
                </select>
                @if ($errors->has('state_id'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('state_id') }}</strong>
                    </span>
                @endif
            </div>
          </div>
        <div class="col-lg-4 col-md-12 mb-1">
            <div class="form-group"> <!-- Zip Code-->
                <label for="zip_id" class="control-label">Zip Code*</label>
                <input type="text" class="form-control" id="zip_id" value="{{$broker->zipcode}}" name="zipcode" placeholder="Zip Code" >
                @if ($errors->has('zipcode'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('zipcode') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <div class="col-lg-12 mb-1">
            <div class="form-group"> <!-- Email -->
                <label for="user_email_id" class="control-label">Email Address*</label>
                <input type="text" class="form-control" id="user_email_id" value="{{$broker->email}}" name="email" placeholder="Email Address" >
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong style="color: red">{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          
             <div class="row">
                <div class="col-md-6 pad-adjust">
                    <input type="submit"  class="btn btn-warning mr-2" value="Update Profile" />
                </div>

                <div class="col-md-6 pad-adjust">
                    <a href="{{url('/broker/dashboard')}}" class="btn btn-success mr-2"> Cancel </a>
                </div>
            </div>
          </form>
       
       
      </div>
    </div>
  </section>

  <section class="contact bg-primary" id="contact">
    <div class="container">
      <h2>We
        <i class="fas fa-heart"></i>
        new friends!</h2>
      <ul class="list-inline list-social">
        <li class="list-inline-item social-twitter">
          <a href="#">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li class="list-inline-item social-facebook">
          <a href="#">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        <li class="list-inline-item social-linkedin">
          <a href="#">
            <i class="fab fa-linkedin-in"></i>
          </a>
        </li>
      </ul>
    </div>
  </section>

  <footer>
    <div class="container">
        <img src="img/flyteHCM_Portal.png" width="200px" height="49px" alt="" style="margin: 10px;"/>
      <p>&copy; Flyte HCM, LLC 2019. All Rights Reserved.</p>
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#PrivacyModal">Privacy</a>
        </li>
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#TermsModal">Terms</a>
        </li>
        <li class="list-inline-item">
          <a href="#" data-toggle="modal" data-target="#FAQModal">FAQ</a>
        </li>
      </ul>
    </div>
  </footer>
    
<div class="modal" tabindex="-1" role="dialog" id="FAQModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Frequently Asked Questions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Spicy jalapeno bacon ipsum dolor amet beef culpa turducken officia frankfurter bresaola jerky andouille pork loin, et chuck cupim labore meatloaf. Turkey dolore tail boudin. Spare ribs ground round tri-tip pariatur, aute turducken leberkas incididunt drumstick pork. Deserunt tri-tip sirloin kevin, ut eiusmod bacon shankle.</p>
          <p>Duis dolor cupim pork belly fugiat non. Occaecat ex sunt quis capicola. Esse rump ground round, est chicken prosciutto sed in dolor elit pig. Minim ut labore strip steak voluptate dolore in excepteur corned beef tongue tenderloin magna.</p>
          <p>Elit aliqua excepteur veniam turducken. Picanha salami short ribs, ball tip proident veniam doner jerky fatback pork loin pancetta jowl. Minim cupim picanha, tempor non pork cillum buffalo laborum officia. Consectetur anim tenderloin, consequat laboris spare ribs nostrud non. Rump tri-tip ham, shankle in ground round tail id beef. Cillum chuck shoulder drumstick.</p>
          <p>Aute andouille et est, ullamco dolore incididunt reprehenderit ut aliqua nisi tongue shankle. Incididunt ea burgdoggen bacon kevin shank nostrud. Dolor quis frankfurter sausage, kielbasa elit sunt landjaeger in flank fatback velit pork esse do. Ipsum kevin eu sunt shankle spare ribs, flank irure alcatra brisket capicola turducken boudin chicken ut.</p>
          <p>Pork loin ut pariatur, beef ribs do dolore sint biltong ipsum buffalo tail. Ut porchetta do tempor ullamco, meatball kevin meatloaf chicken deserunt. Voluptate salami occaecat reprehenderit leberkas. Brisket excepteur beef esse. Laborum proident t-bone, adipisicing leberkas velit tri-tip pork loin commodo. Flank shankle duis jowl aliqua in doner fugiat voluptate turducken. Ut incididunt laboris, ex salami fugiat picanha hamburger.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
<div class="modal" tabindex="-1" role="dialog" id="PrivacyModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Privacy Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1>Privacy policy</h1>
<p>This privacy policy (&quot;Policy&quot;) describes how Flyte HCM, LLC (&quot;Flyte HCM, LLC&quot;, &quot;we&quot;, &quot;us&quot; or &quot;our&quot;) collects, protects and uses the personally identifiable information (&quot;Personal Information&quot;) you (&quot;User&quot;, &quot;you&quot; or &quot;your&quot;) may provide on the <a target="_blank" rel="nofollow" href="https://www.HealthChoice.app">HealthChoice.app</a> website and any of its products or services (collectively, &quot;Website&quot; or &quot;Services&quot;). It also describes the choices available to you regarding our use of your Personal Information and how you can access and update this information. This Policy does not apply to the practices of companies that we do not own or control, or to individuals that we do not employ or manage.</p>
<h2>Collection of personal information</h2>
<p>We receive and store any information you knowingly provide to us when you create an account, make a purchase, fill any online forms on the Website.  When required this information may include your email address, name, phone number, address, credit card information,  or other Personal Information. You can choose not to provide us with certain information, but then you may not be able to take advantage of some of the Website's features. Users who are uncertain about what information is mandatory are welcome to contact us.</p>
<h2>Automatic collection of information</h2>
<p>When you visit the Website our servers automatically record information that your browser sends. This data may include information such as your device's IP address, browser type and version, operating system type and version, language preferences or the webpage you were visiting before you came to our Website, pages of our Website that you visit, the time spent on those pages, information you search for on our Website, access times and dates, and other statistics.</p>
<h2>Managing personal information</h2>
<p>You are able to access, add to, update and delete certain Personal Information about you. The information you can view, update, and delete may change as the Website or Services change. When you update information, however, we may maintain a copy of the unrevised information in our records. Some information may remain in our private records after your deletion of such information from your account. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. We may use any aggregated data derived from or incorporating your Personal Information after you update or delete it, but not in a manner that would identify you personally. Once the retention period expires, Personal Information shall be deleted. Therefore, the right to access, the right to erasure, the right to rectification and the right to data portability cannot be enforced after the expiration of the retention period.</p>
<h2>Use and processing of collected information</h2>
<p>Any of the information we collect from you may be used to personalize your experience; improve our Website; improve customer service and respond to queries and emails of our customers; process transactions; send newsletters; send notification emails such as password reminders, updates, etc;  run and operate our Website and Services. Non-Personal Information collected is used only to identify potential cases of abuse and establish statistical information regarding Website usage. This statistical information is not otherwise aggregated in such a way that would identify any particular user of the system.</p>
<p>We may process Personal Information related to you if one of the following applies: (i) You have given your consent for one or more specific purposes. Note that under some legislations we may be allowed to process information until you object to such processing (by opting out), without having to rely on consent or any other of the following legal bases below. This, however, does not apply, whenever the processing of Personal Information is subject to European data protection law; (ii) Provision of information is necessary for the performance of an agreement with you and/or for any pre-contractual obligations thereof; (iii) Processing is necessary for compliance with a legal obligation to which you are subject; (iv) Processing is related to a task that is carried out in the public interest or in the exercise of official authority vested in us; (v) Processing is necessary for the purposes of the legitimate interests pursued by us or by a third party. In any case, we will be happy to clarify the specific legal basis that applies to the processing, and in particular whether the provision of Personal Data is a statutory or contractual requirement, or a requirement necessary to enter into a contract.</p>
<h2>Information transfer and storage</h2>
<p>Depending on your location, data transfers may involve transferring and storing your information in a country other than your own. You are entitled to learn about the legal basis of information transfers to a country outside the European Union or to any international organization governed by public international law or set up by two or more countries, such as the UN, and about the security measures taken by us to safeguard your information. If any such transfer takes place, you can find out more by checking the relevant sections of this document or inquire with us using the information provided in the contact section.</p>
<h2>The rights of users</h2>
<p>You may exercise certain rights regarding your information processed by us. In particular, you have the right to do the following: (i) you have the right to withdraw consent where you have previously given your consent to the processing of your information; (ii) you have the right to object to the processing of your information if the processing is carried out on a legal basis other than consent; (iii) you have the right to learn if information is being processed by us, obtain disclosure regarding certain aspects of the processing and obtain a copy of the information undergoing processing; (iv) you have the right to verify the accuracy of your information and ask for it to be updated or corrected; (v) you have the right, under certain circumstances, to restrict the processing of your information, in which case, we will not process your information for any purpose other than storing it; (vi) you have the right, under certain circumstances, to obtain the erasure of your Personal Information from us; (vii) you have the right to receive your information in a structured, commonly used and machine readable format and, if technically feasible, to have it transmitted to another controller without any hindrance. This provision is applicable provided that your information is processed by automated means and that the processing is based on your consent, on a contract which you are part of or on pre-contractual obligations thereof.</p>
<h2>The right to object to processing</h2>
<p>Where Personal Information is processed for the public interest, in the exercise of an official authority vested in us or for the purposes of the legitimate interests pursued by us, you may object to such processing by providing a ground related to your particular situation to justify the objection. You must know that, however, should your Personal Information be processed for direct marketing purposes, you can object to that processing at any time without providing any justification. To learn, whether we are processing Personal Information for direct marketing purposes, you may refer to the relevant sections of this document.</p>
<h2>How to exercise these rights</h2>
<p>Any requests to exercise User rights can be directed to the Owner through the contact details provided in this document. These requests can be exercised free of charge and will be addressed by the Owner as early as possible and always within one month.</p>
<h2>Billing and payments</h2>
<p>In case of services requiring payment, we request credit card or other payment account information, which will be used solely for processing payments. Your purchase transaction data is stored only as long as is necessary to complete your purchase transaction. After that is complete, your purchase transaction information is deleted. Where necessary for processing future payments and subject to your prior consent, your financial information will be stored in encrypted form on secure servers of our reputed payment gateway service provider who is beholden to treating your Personal Information in accordance with this Privacy Policy. All direct payment gateways adhere to the latest security standards as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover.  Sensitive and private data exchange happens over a SSL secured communication channel and is encrypted and protected with digital signatures, and our Website is also in compliance with PCI vulnerability standards in order to create as secure of an environment as possible for Users. Scans for malware are performed on a regular basis for additional security and protection.</p>
<h2>Privacy of children</h2>
<p>We do not knowingly collect any Personal Information from children under the age of 13. If you are under the age of 13, please do not submit any Personal Information through our Website or Service. We encourage parents and legal guardians to monitor their children's Internet usage and to help enforce this Policy by instructing their children never to provide Personal Information through our Website or Service without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Information to us through our Website or Service, please contact us. You must also be at least 16 years of age to consent to the processing of your personal data in your country (in some countries we may allow your parent or guardian to do so on your behalf).</p>
<h2>Newsletters</h2>
<p>We offer electronic newsletters to which you may voluntarily subscribe at any time. You may choose to stop receiving our newsletter or marketing emails by following the unsubscribe instructions included in these emails or by contacting us. However, you will continue to receive essential transactional emails.</p>
<h2>Cookies</h2>
<p>The Website uses &quot;cookies&quot; to help personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. We may use cookies to collect, store, and track information for statistical purposes to operate our Website and Services. You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the features of the Website and Services. To learn more about cookies and how to manage them, visit <a target="_blank" href="https://www.internetcookies.org">internetcookies.org</a></p>
<p>In addition to using cookies and related technologies as described above, we also may permit certain third-party companies to help us tailor advertising that we think may be of interest to users and to collect and use other data about user activities on the Website. These companies may deliver ads that might also place cookies and otherwise track user behavior.</p>
<h2>Do Not Track signals</h2>
<p>Some browsers incorporate a Do Not Track feature that signals to websites you visit that you do not want to have your online activity tracked. Tracking is not the same as using or collecting information in connection with a website. For these purposes, tracking refers to collecting personally identifiable information from consumers who use or visit a website or online service as they move across different websites over time. Our Website does not track its visitors over time and across third party websites. However, some third party sites may keep track of your browsing activities when they serve you content, which enables them to tailor what they present to you.</p>
<h2>Links to other websites</h2>
<p>Our Website contains links to other websites that are not owned or controlled by us. Please be aware that we are not responsible for the privacy practices of such other websites or third-parties. We encourage you to be aware when you leave our Website and to read the privacy statements of each and every website that may collect Personal Information.</p>
<h2>Information security</h2>
<p>We secure information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use, or disclosure. We maintain reasonable administrative, technical, and physical safeguards in an effort to protect against unauthorized access, use, modification, and disclosure of Personal Information in its control and custody. However, no data transmission over the Internet or wireless network can be guaranteed. Therefore, while we strive to protect your Personal Information, you acknowledge that (i) there are security and privacy limitations of the Internet which are beyond our control; (ii) the security, integrity, and privacy of any and all information and data exchanged between you and our Website cannot be guaranteed; and (iii) any such information and data may be viewed or tampered with in transit by a third-party, despite best efforts.</p>
<h2>Data breach</h2>
<p>In the event we become aware that the security of the Website has been compromised or users Personal Information has been disclosed to unrelated third parties as a result of external activity, including, but not limited to, security attacks or fraud, we reserve the right to take reasonably appropriate measures, including, but not limited to, investigation and reporting, as well as notification to and cooperation with law enforcement authorities. In the event of a data breach, we will make reasonable efforts to notify affected individuals if we believe that there is a reasonable risk of harm to the user as a result of the breach or if notice is otherwise required by law. When we do, we will send you an email, get in touch with you over the phone.</p>
<h2>Legal disclosure</h2>
<p>We will disclose any information we collect, use or receive if required or permitted by law, such as to comply with a subpoena, or similar legal process, and when we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request. In the event we go through a business transition, such as a merger or acquisition by another company, or sale of all or a portion of its assets, your user account, and personal data will likely be among the assets transferred.</p>
<h2>Changes and amendments</h2>
<p>We reserve the right to modify this Policy relating to the Website or Services at any time, effective upon posting of an updated version of this Policy on the Website. When we do we will send you an email to notify you. Continued use of the Website after any such changes shall constitute your consent to such changes.</p>
<h2>Acceptance of this policy</h2>
<p>You acknowledge that you have read this Policy and agree to all its terms and conditions. By using the Website or its Services you agree to be bound by this Policy. If you do not agree to abide by the terms of this Policy, you are not authorized to use or access the Website and its Services.</p>
<h2>Contacting us</h2>
<p>If you have any questions about this Policy, please contact us.</p>
<p>This document was last updated on August 1, 2019</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
        
<div class="modal" tabindex="-1" role="dialog" id="TermsModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Terms &amp; Conditions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<h1>Terms and conditions</h1>
<p>These terms and conditions (&quot;Terms&quot;, &quot;Agreement&quot;) are an agreement between Flyte HCM, LLC (&quot;Flyte HCM, LLC&quot;, &quot;us&quot;, &quot;we&quot; or &quot;our&quot;) and you (&quot;User&quot;, &quot;you&quot; or &quot;your&quot;). This Agreement sets forth the general terms and conditions of your use of the <a target="_blank" rel="nofollow" href="https://www.HealthChoice.app">HealthChoice.app</a> website and any of its products or services (collectively, &quot;Website&quot; or &quot;Services&quot;).</p>
<h2>Accounts and membership</h2>
<p>If you create an account on the Website, you are responsible for maintaining the security of your account and you are fully responsible for all activities that occur under the account and any other actions taken in connection with it. We may, but have no obligation to, monitor and review new accounts before you may sign in and use our Services. Providing false contact information of any kind may result in the termination of your account. You must immediately notify us of any unauthorized uses of your account or any other breaches of security. We will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions. We may suspend, disable, or delete your account (or any part thereof) if we determine that you have violated any provision of this Agreement or that your conduct or content would tend to damage our reputation and goodwill. If we delete your account for the foregoing reasons, you may not re-register for our Services. We may block your email address and Internet protocol address to prevent further registration.</p>
<h2>Billing and payments</h2>
<p>You shall pay all fees or charges to your account in accordance with the fees, charges, and billing terms in effect at the time a fee or charge is due and payable. If auto-renewal is enabled for the Services you have subscribed for, you will be charged automatically in accordance with the term you selected. Sensitive and private data exchange happens over a SSL secured communication channel and is encrypted and protected with digital signatures, and our Website is also in compliance with PCI vulnerability standards in order to create as secure of an environment as possible for Users. Scans for malware are performed on a regular basis for additional security and protection. If, in our judgment, your purchase constitutes a high-risk transaction, we will require you to provide us with a copy of your valid government-issued photo identification, and possibly a copy of a recent bank statement for the credit or debit card used for the purchase. We reserve the right to change products and product pricing at any time.</p>
<h2>Accuracy of information</h2>
<p>Occasionally there may be information on the Website that contains typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, availability, promotions and offers. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information on the Website or on any related Service is inaccurate at any time without prior notice (including after you have submitted your order). We undertake no obligation to update, amend or clarify information on the Website including, without limitation, pricing information, except as required by law. No specified update or refresh date applied on the Website should be taken to indicate that all information on the Website or on any related Service has been modified or updated.</p>
<h2>Backups</h2>
<p>We are not responsible for Content residing on the Website. In no event shall we be held liable for any loss of any Content. It is your sole responsibility to maintain appropriate backup of your Content. Notwithstanding the foregoing, on some occasions and in certain circumstances, with absolutely no obligation, we may be able to restore some or all of your data that has been deleted as of a certain date and time when we may have backed up data for our own purposes. We make no guarantee that the data you need will be available.</p>
<h2>Links to other websites</h2>
<p>Although this Website may link to other websites, we are not, directly or indirectly, implying any approval, association, sponsorship, endorsement, or affiliation with any linked website, unless specifically stated herein. We are not responsible for examining or evaluating, and we do not warrant the offerings of, any businesses or individuals or the content of their websites. We do not assume any responsibility or liability for the actions, products, services, and content of any other third-parties. You should carefully review the legal statements and other conditions of use of any website which you access through a link from this Website. Your linking to any other off-site websites is at your own risk.</p>
<h2>Prohibited uses</h2>
<p>In addition to other terms as set forth in the Agreement, you are prohibited from using the Website or its Content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.</p>
<h2>Intellectual property rights</h2>
<p>This Agreement does not transfer to you any intellectual property owned by Flyte HCM, LLC or third-parties, and all rights, titles, and interests in and to such property will remain (as between the parties) solely with Flyte HCM, LLC. All trademarks, service marks, graphics and logos used in connection with our Website or Services, are trademarks or registered trademarks of Flyte HCM, LLC or Flyte HCM, LLC licensors. Other trademarks, service marks, graphics and logos used in connection with our Website or Services may be the trademarks of other third-parties. Your use of our Website and Services grants you no right or license to reproduce or otherwise use any Flyte HCM, LLC or third-party trademarks.</p>
<h2>Disclaimer of warranty</h2>
<p>You agree that your use of our Website or Services is solely at your own risk. You agree that such Service is provided on an &quot;as is&quot; and &quot;as available&quot; basis. We expressly disclaim all warranties of any kind, whether express or implied, including but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement. We make no warranty that the Services will meet your requirements, or that the Service will be uninterrupted, timely, secure, or error-free; nor do we make any warranty as to the results that may be obtained from the use of the Service or as to the accuracy or reliability of any information obtained through the Service or that defects in the Service will be corrected. You understand and agree that any material and/or data downloaded or otherwise obtained through the use of Service is done at your own discretion and risk and that you will be solely responsible for any damage to your computer system or loss of data that results from the download of such material and/or data. We make no warranty regarding any goods or services purchased or obtained through the Service or any transactions entered into through the Service. No advice or information, whether oral or written, obtained by you from us or through the Service shall create any warranty not expressly made herein.</p>
<h2>Limitation of liability</h2>
<p>To the fullest extent permitted by applicable law, in no event will Flyte HCM, LLC, its affiliates, officers, directors, employees, agents, suppliers or licensors be liable to any person for (a): any indirect, incidental, special, punitive, cover or consequential damages (including, without limitation, damages for lost profits, revenue, sales, goodwill, use of content, impact on business, business interruption, loss of anticipated savings, loss of business opportunity) however caused, under any theory of liability, including, without limitation, contract, tort, warranty, breach of statutory duty, negligence or otherwise, even if Flyte HCM, LLC has been advised as to the possibility of such damages or could have foreseen such damages. To the maximum extent permitted by applicable law, the aggregate liability of Flyte HCM, LLC and its affiliates, officers, employees, agents, suppliers and licensors, relating to the services will be limited to an amount greater of one dollar or any amounts actually paid in cash by you to Flyte HCM, LLC for the prior one month period prior to the first event or occurrence giving rise to such liability. The limitations and exclusions also apply if this remedy does not fully compensate you for any losses or fails of its essential purpose.</p>
<h2>Indemnification</h2>
<p>You agree to indemnify and hold Flyte HCM, LLC and its affiliates, directors, officers, employees, and agents harmless from and against any liabilities, losses, damages or costs, including reasonable attorneys' fees, incurred in connection with or arising from any third-party allegations, claims, actions, disputes, or demands asserted against any of them as a result of or relating to your Content, your use of the Website or Services or any willful misconduct on your part.</p>
<h2>Severability</h2>
<p>All rights and restrictions contained in this Agreement may be exercised and shall be applicable and binding only to the extent that they do not violate any applicable laws and are intended to be limited to the extent necessary so that they will not render this Agreement illegal, invalid or unenforceable. If any provision or portion of any provision of this Agreement shall be held to be illegal, invalid or unenforceable by a court of competent jurisdiction, it is the intention of the parties that the remaining provisions or portions thereof shall constitute their agreement with respect to the subject matter hereof, and all such remaining provisions or portions thereof shall remain in full force and effect.</p>
<h2>Dispute resolution</h2>
<p>The formation, interpretation, and performance of this Agreement and any disputes arising out of it shall be governed by the substantive and procedural laws of Minnesota, United States without regard to its rules on conflicts or choice of law and, to the extent applicable, the laws of United States. The exclusive jurisdiction and venue for actions related to the subject matter hereof shall be the state and federal courts located in Minnesota, United States, and you hereby submit to the personal jurisdiction of such courts. You hereby waive any right to a jury trial in any proceeding arising out of or related to this Agreement. The United Nations Convention on Contracts for the International Sale of Goods does not apply to this Agreement.</p>
<h2>Changes and amendments</h2>
<p>We reserve the right to modify this Agreement or its policies relating to the Website or Services at any time, effective upon posting of an updated version of this Agreement on the Website. When we do, we will send you an email to notify you. Continued use of the Website after any such changes shall constitute your consent to such changes.</p>
<h2>Acceptance of these terms</h2>
<p>You acknowledge that you have read this Agreement and agree to all its terms and conditions. By using the Website or its Services you agree to be bound by this Agreement. If you do not agree to abide by the terms of this Agreement, you are not authorized to use or access the Website and its Services.</p>
<h2>Contacting us</h2>
<p>If you have any questions about this Agreement, please contact us.</p>
<p>This document was last updated on August 1, 2019</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#Registration').submit(function(){
            var response = grecaptcha.getResponse();
            if(response.length == 0) {
                if($('.captcha-error').length <= 0){
                    $('.g-recaptcha').after('<p class="captcha-error" style="color:red">Invalid captcha</p>');
                }
                return false;
            }else{
                if($('.captcha-error').length > 0) {
                    $('.captcha-error').remove();
                }
            }
            return true;
        });
    });
</script>

</body>