@extends('broker.layout.auth')

@section('content')

   <div class="row search-info-compare-plans">
          <div class="col-xl-4 col-sm-12 left-border-accent">
			<table class="table table-borderless table-xs">
				<thead>
				  <tr>
					<th colspan="2">Your Search for {{ @$data->zipcode }} ({{ @$data->county->county_name }})</th>
				  </tr>
				</thead>
				<tbody>
					
					
					
				  <tr>
				  	
					<td><strong>Applicant</strong></td>
					<?php
					  if(empty($spouse) && sizeof($child) == 0){
					  	 $ded_count = 1;
					  	}else{
					  	 $ded_count = 2;	
					  	}
					  	
                      
				     ?>
					<td>{{ ucfirst($self['gender'])}}, {{ $self['age'] }}, @if($self['smoker'] == 'yes') Smoker @else  Non Smoker @endif  </td>
				  <td><a href="{{ url('/broker/page/plan') }}"><button type="submit" class="btn btn-primary btn-xs float-right mx-2">Edit</button></a></td>
				  </tr>
				  
				  @if(!empty($spouse))
				  
				  <tr>
				  	
					<td><strong>Spouse</strong></td>
					
					<td>{{ ucfirst(@$spouse['gender'])}}, {{ @$spouse['age'] }}, @if(@$spouse['smoker'] == 'yes') Smoker @else  Non Smoker @endif  </td>
				  </tr>
				  @endif
				  @foreach($child as $key => $children)
                      <?php 
                            $userDob = $children->dob;
				            $dob = new DateTime($userDob);
				            $now = new DateTime();
				            $difference = $now->diff($dob);
				            $age = $difference->y;
                        ?>
                    <tr>
				  	
					<td><strong>Child {{ $key +1 }}</strong></td>
					
					<td>{{ ucfirst($children['gender'])}}, {{ $age}}, @if($children['smoker'] == 'yes') Smoker @else  Non Smoker @endif  </td>
				  </tr>
				  @endforeach
				  

				</tbody>
			  </table>  
		   </div>
          <div class="col-xl-8 col-sm-12 d-none d-xl-block d-lg-block d-md-block left-border-accent">
       			<div class="row">
				<div class="col-xl-12 p-2"><strong>Compare Plans:</strong><button type="submit" class="btn btn-primary btn-xs float-right mx-2">Email <i class="fas fa-paper-plane"></i></button></div></div>
       			<div class="row">
				  <div class="col-xl-12 p-1">
					  <ul class="list-compare-plans">
					  	
						  <!-- <li class="list-compare-plans"><div class="card card-plan-compare">
								  <div class="card-plan-compare-remove"><img class="card-img-top img-fluid card-carrier-logo" src="{{ url('frontend/images/carrier_logo.png')}}"  alt="Card image"><i class="fas fa-minus-circle remove-glyphicon"></i></div>
								  <div class="card-body card-plan-data">
    								<h4 class="card-title card-plan-name">Peak Individual $6650 HSA Bronze</h4>
									<p class="card-text card-plan-premium">$292.19</p>
								  </div>
								</div></li> -->
								<form class="form-horizontal" id="formcompare" action="{{route('broker.page.compare')}}" method="POST" enctype="multipart/form-data" role="form">
                                    {{csrf_field()}}
								<ul class="list-compare-plans checkk">
									<input type="hidden" value="{{ $broker->id }}" name="broker_id">
									<input type="hidden" value="{{ @$data->zipcode }}" name="zipcode">
									<input type="hidden" value="{{ @$data->county->county_name }}" name="county">
									<input type="hidden" value="{{ @$data->id }}" name="application_id">
					  		             <button  type="submit" class="btn btn-primary btn-xs float-right mx-2">Compare <i class="fas fa-clone"></i></button>
					  	             
								</ul>
								</form>
						  
						</ul></div>
		   		</div></div>
       </div>
       <div class="row">
          <div class="col-2 align-middle"><select class="form-control"><option>choose Carrier</option></select></div>
          <div class="col-2 align-middle">Premium <input  type="range"  name=""></div>
          <div class="col-2 align-middle">Deductible <input type="range" name=""></div>
          <div class="col-2 align-middle"><select class="form-control"><option>HSA Qualified</option>
                                                  <option>Yes</option>
                                                  <option>No</option>
                                                 </select></div>
          <div class="col-2 align-middle"><select id="metallevel" class="form-control"><option>Metal level</option>
                                                  <option value="Gold">Gold</option>
                                                  <option value="Silver">Silver</option>
                                                  <option value="Bronze">Bronze</option>
                                            </select></div>
          <div class="col-2 align-middle"><select id="sortby" name="sortby" class="form-control"><option>Sort By</option>
                                                  <option value="low">Lowest Premium</option>
                                                  <option value="high">Highest Premium</option>
                                                 </select></div>
		</div>
		<input type="hidden" value="1" id="update_id" name="update_id">
		<div class="filer">
       @foreach($goldplan as $index => $plan)
       <div class="row plan_result gold_metal">

		   <div class="col-xl-4 col-lg-4 col-md-8 col-sm-8 col-xs-8 align-self-center fill py-2">
			   <div class="row">

				  <div class="col-12 align-middle d-none d-lg-flex d-md-flex d-xl-flex"><img id="im{{ $index + 1 }}" src="{{ @$plan->carrier->carrier_logo }}" width="150px" height="100px"  alt="" ></div>
				  <div class="col-12 align-middle pt-2 text-xs-center" id="goldtext{{ $index + 1 }}"><strong>{{ $plan->PLAN_NAME_T}}</strong></div>
				  <input type="hidden" id="goldidd{{ $index + 1 }}" value="{{ $plan->id }}" name="goldidd">
				  <div class="col-12 align-middle text-xs-center">Gold</div>
			   </div>
			   <div class="row mt-2 d-none d-xl-block d-lg-block d-md-block ">
				  <div class="col-1 align-middle"><div class="form-check">
					  <input class="form-check-input ddd{{ $plan->id }}" onclick="comparegold({{ $index + 1 }})" type="checkbox" value="gold{{ $index + 1 }}" id="comparegold{{ $index + 1 }}">
					  <label class="form-check-label" for="compare5">
						Compare
					  </label>
					</div></div>
			   </div>
		   </div>
		  <div class="col-5 d-none d-xl-block d-lg-block align-self-center py-2">
		  	@foreach($plan->subplan as $v)
				   @foreach($v->subplanded as $va)
			  <div class="row">
			  	<div class="col-6 text-center">
				  	<strong>Deductible</strong><br> 
				  	                                
				  	                                 
					  	                                
					  	                                 @if($ded_count == 1)
					  	                                  ${{ round($va->DEDUCTIBLE_SINGLE_C) }}
					  	                                 @else
					  	                                  ${{ round($va->DEDUCTIBLE_FAMILY_C) }} 
					  	                                 @endif   

				  	                                   
				  </div>
			  	<div class="col-6 text-center">
				  	<strong>Max Out-of-Pocket</strong><br>@if($ded_count == 1)
					  	                                  ${{ round($va->OOP_SINGLE_C) }}
					  	                                 @else
					  	                                  ${{ round($va->OOP_FAMILY_C) }} 
					  	                                 @endif
				  </div>
			  </div>
			   
			  <div class="row mt-2">
			  	<div class="col-6 text-center">
				  	<strong>Office Visits</strong><br>{{ $v->OV_PRIMARY_CARE_M }}
				  </div>
			  	<div class="col-6 text-center">
				  	<strong>Prescriptions</strong><br>{{ $v->RX_GENERIC_M }}
				  </div>
			  </div>
			   @endforeach
			 @endforeach			  
		   </div>
		  <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-4 align-self-center py-2">
		   	<div class="row">
				<div class="col-12 text-center">Monthly Premium</div>
				<div class="col-12 text-center search-result-premium" id="goldpremium{{ $index +1 }}">${{ @$plan->premium ?? 0.00 }}</div>
				<div class="col-12 text-center py-1"><a href="{{url('/')}}" class="btn btn-primary btn-block btn-sm">Apply</a></div>
			  </div>
		   	<div class="row mt-2">
				@foreach($plan->subplan as $v)
				   @foreach($v->subplanded as $va)
				<div class="col-6 text-center"><a href="{{$v->SBC_PATH_T}}"  class="btn btn-sbc-doc btn-xs">See SBC</a></div>
				  @endforeach
			 @endforeach
				<div class="col-6 text-center"><a href="{{ $plan->PROVIDER_LOOKUP_URL_M}}" class="btn btn-sbc-doc btn-xs">Find Doctor</a></div>
			  </div>
		   
		   
		   
		   </div>
		   
		   </div>
		   @endforeach
       @foreach($silverplan as $index => $plan)
       <div class="row plan_result silver_metal">

		   <div class="col-xl-4 col-lg-4 col-md-8 col-sm-8 col-xs-8 align-self-center fill py-2">
			   <div class="row">

				  <div class="col-12 align-middle d-none d-lg-flex d-md-flex d-xl-flex"><img id="im{{ $index + 1 }}" src="{{ @$plan->carrier->carrier_logo }}" width="150px" height="100px"  alt="" ></div>
				  <div class="col-12 align-middle pt-2 text-xs-center" id="silvertext{{ $index + 1 }}"><strong>{{ $plan->PLAN_NAME_T}}</strong></div>
				  <input type="hidden" id="silveridd{{ $index + 1 }}" value="{{ $plan->id }}" name="silveridd">
				  <div class="col-12 align-middle text-xs-center">Silver</div>
			   </div>
			   <div class="row mt-2 d-none d-xl-block d-lg-block d-md-block ">
				  <div class="col-1 align-middle"><div class="form-check">
					  <input class="form-check-input ddd{{ $plan->id }}" onclick="comparesilver({{ $index + 1 }})" type="checkbox" value="silver{{ $index + 1 }}" id="comparesilver{{ $index + 1 }}">
					  <label class="form-check-label" for="compare3">
						Compare
					  </label>
					</div></div>
			   </div>
		   </div>
		  <div class="col-5 d-none d-xl-block d-lg-block align-self-center py-2">
		  	@foreach($plan->subplan as $v)
				   @foreach($v->subplanded as $va)
			  <div class="row">
			  	<div class="col-6 text-center">
				  	<strong>Deductible</strong><br>
					  	                                 @if($ded_count == 1)
					  	                                  ${{ round($va->DEDUCTIBLE_SINGLE_C) }}
					  	                                 @else
					  	                                  ${{ round($va->DEDUCTIBLE_FAMILY_C) }} 
					  	                                 @endif
				  </div>
			  	<div class="col-6 text-center">
				  	<strong>Max Out-of-Pocket</strong><br>@if($ded_count == 1)
					  	                                  ${{ round($va->OOP_SINGLE_C) }}
					  	                                 @else
					  	                                  ${{ round($va->OOP_FAMILY_C) }} 
					  	                                 @endif
				  </div>
			  </div>
			  <div class="row mt-2">
			  	<div class="col-6 text-center">
				  	<strong>Office Visits</strong><br>{{ $v->OV_PRIMARY_CARE_M }}
				  </div>
			  	<div class="col-6 text-center">
				  	<strong>Prescriptions</strong><br>{{ $v->RX_GENERIC_M }}
				  </div>
			  </div>
			  @endforeach
			 @endforeach			  
		   </div>
		  <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-4 align-self-center py-2">
		   	<div class="row">
				<div class="col-12 text-center">Monthly Premium</div>
				<div class="col-12 text-center search-result-premium" id="silverpremium{{ $index +1 }}">${{ @$plan->premium ?? 0.00 }}</div>
				<div class="col-12 text-center py-1"><a href="{{url('/')}}" class="btn btn-primary btn-block btn-sm">Apply</a></div>
			  </div>
		   	<div class="row mt-2">
				@foreach($plan->subplan as $v)
				   @foreach($v->subplanded as $va)
				<div class="col-6 text-center"><a href="{{$v->SBC_PATH_T}}"  class="btn btn-sbc-doc btn-xs">See SBC</a></div>
				  @endforeach
			 @endforeach
				<div class="col-6 text-center"><a href="{{ $plan->PROVIDER_LOOKUP_URL_M}}" class="btn btn-sbc-doc btn-xs">Find Doctor</a></div>
			  </div>
		   </div>
		</div>
		@endforeach


       @foreach($bronzeplan as $index => $plan)
       <div class="row plan_result bronze_metal">

		   <div class="col-xl-4 col-lg-4 col-md-8 col-sm-8 col-xs-8 align-self-center fill py-2">
			   <div class="row">

				  <div class="col-12 align-middle d-none d-lg-flex d-md-flex d-xl-flex"><img id="im{{ $index + 1 }}" src="{{ @$plan->carrier->carrier_logo }}" width="150px" height="100px"  alt="" ></div>
				  <div class="col-12 align-middle pt-2 text-xs-center" id="bronzetext{{ $index + 1 }}"><strong>{{ $plan->PLAN_NAME_T}}</strong></div>
				  <input type="hidden" id="bronzeidd{{ $index + 1 }}" value="{{ $plan->id }}" name="bronzeidd">
				  <div class="col-12 align-middle text-xs-center">Bronze</div>
			   </div>
			   <div class="row mt-2 d-none d-xl-block d-lg-block d-md-block ">
				  <div class="col-1 align-middle"><div class="form-check">
					  <input class="form-check-input ddd{{ $plan->id }}" onclick="comparebronze({{ $index + 1 }})" type="checkbox" value="bronze{{ $index + 1 }}" id="comparebronze{{ $index + 1 }}">
					  <label class="form-check-label" for="compare2">
						Compare
					  </label>
					</div></div>
			   </div>
		   </div>
		  <div class="col-5 d-none d-xl-block d-lg-block align-self-center py-2">
		  		@foreach($plan->subplan as $v)
				   @foreach($v->subplanded as $va)
			  <div class="row">
			  	<div class="col-6 text-center">
				  	<strong>Deductible</strong><br>@if($ded_count == 1)
					  	                                  ${{ round($va->DEDUCTIBLE_SINGLE_C) }}
					  	                                 @else
					  	                                  ${{ round($va->DEDUCTIBLE_FAMILY_C) }} 
					  	                                 @endif
				  </div>
			  	<div class="col-6 text-center">
				  	<strong>Max Out-of-Pocket</strong><br>@if($ded_count == 1)
					  	                                  ${{ round($va->OOP_SINGLE_C) }}
					  	                                 @else
					  	                                  ${{ round($va->OOP_FAMILY_C) }} 
					  	                                 @endif
				  </div>
			  </div>
			  <div class="row mt-2">
			  	<div class="col-6 text-center">
				  	<strong>Office Visits</strong><br>{{ $v->OV_PRIMARY_CARE_M }}
				  </div>
			  	<div class="col-6 text-center">
				  	<strong>Prescriptions</strong><br>{{ $v->RX_GENERIC_M }}
				  </div>
			  </div>
			  @endforeach
			 @endforeach			  
		   </div>
		  <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-xs-4 align-self-center py-2">
		   	<div class="row">
				<div class="col-12 text-center">Monthly Premium</div>
				<div class="col-12 text-center search-result-premium" id="bronzepremium{{ $index +1 }}">${{ @$plan->premium ?? 0.00 }}</div>
				<div class="col-12 text-center py-1"><a href="{{url('/')}}" class="btn btn-primary btn-block btn-sm">Apply</a></div>
			  </div>
		   	<div class="row mt-2">
			@foreach($plan->subplan as $v)
				   @foreach($v->subplanded as $va)
				<div class="col-6 text-center"><a href="{{$v->SBC_PATH_T}}"  class="btn btn-sbc-doc btn-xs">See SBC</a></div>
				  @endforeach
			 @endforeach
				<div class="col-6 text-center"><a href="{{ $plan->PROVIDER_LOOKUP_URL_M}}" class="btn btn-sbc-doc btn-xs">Find Doctor</a></div>
			  </div>
		   </div>
		</div>
		@endforeach
	</div>

@endsection
@section('scripts')
<script type="text/javascript">
	var update_id=$("#update_id").val();
	
	function comparegold(id){
       if($("#comparegold"+id).prop("checked") == true){
       	   var text = $("#goldtext"+id).text();
       	   var amount = $("#goldpremium"+id).text();
       	   var plan = $("#goldidd"+id).val();
		   var img = $("#goldimg"+id).attr("src");
       	   //alert(img);
       	    $(".checkk").append("<p><input type='hidden' name='plan_id[]' value="+plan+"><li   class='list-compare-plans'><div id='goldremov"+plan+"'  class='card card-plan-compare'><div class='card-plan-compare-remove'><img class='card-img-top img-fluid card-carrier-logo' src="+img+"  alt='Card image'><i  class='fas fa-minus-circle remove-glyphicon ' onclick='gremove("+plan+")' ></i></div><div class='card-body card-plan-data'><h4 class='card-title card-plan-name'>"+text+"</h4><p class='card-text card-plan-premium'>"+amount+"</p></div></div></li></p>");
       	   var update_id=$("#update_id").val();
       	   var imgg = $('#im'+id).attr('src');
     
       	    var MaxInputs = 5;
       	   if(update_id <= MaxInputs) {
       	  
       	   	update_id++;
       	    $(".checkk").append("<p><input type='hidden' name='plan_id[]' value="+plan+"><li   class='list-compare-plans'><div id='goldremov"+plan+"'  class='card card-plan-compare'><div class='card-plan-compare-remove'><img class='card-img-top img-fluid card-carrier-logo' src='"+imgg+"'  alt='Card image'><i  class='fas fa-minus-circle remove-glyphicon ' onclick='gremove("+plan+")' ></i></div><div class='card-body card-plan-data'><h4 class='card-title card-plan-name'>"+text+"</h4><p class='card-text card-plan-premium'>"+amount+"</p></div></div></li></p>");
       	    $("#update_id").val(update_id);
       	    $("#comparegold"+id).prop('disabled', true);
       	   }
       }else{
       	  
       }
	}

	function comparesilver(id){
       if($("#comparesilver"+id).prop("checked") == true){
       	   var text = $("#silvertext"+id).text();
       	   var amount = $("#silverpremium"+id).text();
       	   var plan = $("#silveridd"+id).val();
		   var img = $("#silverimg"+id).attr("src");
       	    $(".checkk").append("<p><input type='hidden' name='plan_id[]' value="+plan+"><li  class='list-compare-plans'><div id='goldremov"+plan+"' class='card card-plan-compare'><div class='card-plan-compare-remove'><img class='card-img-top img-fluid card-carrier-logo' src="+img+"  alt='Card image'><i  class='fas fa-minus-circle remove-glyphicon' onclick='gremove("+plan+")'></i></div><div class='card-body card-plan-data'><h4 class='card-title card-plan-name'>"+text+"</h4><p class='card-text card-plan-premium'>"+amount+"</p></div></div></li></p>");
       	   var update_id=$("#update_id").val();
       	   var imgg = $('#im'+id).attr('src');
       	   
       	 
       	   var MaxInputs = 5;
       	   if(update_id <= MaxInputs) {
       	   	update_id++;
       	    $(".checkk").append("<p><input type='hidden' name='plan_id[]' value="+plan+"><li  class='list-compare-plans'><div id='goldremov"+plan+"' class='card card-plan-compare'><div class='card-plan-compare-remove'><img class='card-img-top img-fluid card-carrier-logo' src='"+imgg+"'  alt='Card image'><i  class='fas fa-minus-circle remove-glyphicon' onclick='gremove("+plan+")'></i></div><div class='card-body card-plan-data'><h4 class='card-title card-plan-name'>"+text+"</h4><p class='card-text card-plan-premium'>"+amount+"</p></div></div></li></p>");
              $("#update_id").val(update_id);
       	      $("#comparesilver"+id).prop('disabled', true);
       	}
       }else{
       	  
       }
	}

	function comparebronze(id){
       if($("#comparebronze"+id).prop("checked") == true){
       	   var text = $("#bronzetext"+id).text();
       	   var amount = $("#bronzepremium"+id).text();
       	   var plan = $("#bronzeidd"+id).val();
		   var img = $("#bronzeimg"+id).attr("src");
       	    $(".checkk").append("<p><input type='hidden' name='plan_id[]' value="+plan+"><li  class='list-compare-plans'><div id='goldremov"+plan+"' class='card card-plan-compare'><div class='card-plan-compare-remove'><img class='card-img-top img-fluid card-carrier-logo' src="+img+"  alt='Card image'><i class='fas fa-minus-circle remove-glyphicon' onclick='gremove("+plan+")'></i></div><div class='card-body card-plan-data'><h4 class='card-title card-plan-name'>"+text+"</h4><p class='card-text card-plan-premium'>"+amount+"</p></div></div></li></p>");
       	   var update_id=$("#update_id").val();
       	   var imgg = $('#im'+id).attr('src');
       	   
       	  
       	   var MaxInputs = 5;
       	   if(update_id <= MaxInputs) {
       	   	update_id++;
       	    $(".checkk").append("<p><input type='hidden' name='plan_id[]' value="+plan+"><li  class='list-compare-plans'><div id='goldremov"+plan+"' class='card card-plan-compare'><div class='card-plan-compare-remove'><img class='card-img-top img-fluid card-carrier-logo' src='"+imgg+"'  alt='Card image'><i class='fas fa-minus-circle remove-glyphicon' onclick='gremove("+plan+")'></i></div><div class='card-body card-plan-data'><h4 class='card-title card-plan-name'>"+text+"</h4><p class='card-text card-plan-premium'>"+amount+"</p></div></div></li></p>");
       	    $("#update_id").val(update_id);
       	    $("#comparebronze"+id).prop('disabled', true);
       	}
       }else{
       	  
       }


	}

	function gremove(id)
	{
		
      $('#goldremov'+id).hide();
      $('.ddd'+id).prop('disabled', false);
      $('.ddd'+id).prop('checked', false);

      var update_id=$("#update_id").val();
      
      var up = update_id - 1;

      $("#update_id").val(up);


	}

	$(document).ready(function(){
            $("#metallevel").change(function(){

                  var v = $("#metallevel").val();
                  
                  if(v == 'Gold')
                  {
                  	$(".gold_metal").show();
                    $(".silver_metal").hide();
                    $(".bronze_metal").hide();
                  }
                  if(v == 'Silver')
                  {
                  	$(".silver_metal").show();
                    $(".gold_metal").hide();
                    $(".bronze_metal").hide();
                  }
                  if(v == 'Bronze')
                  {
                  	$(".bronze_metal").show();
                    $(".silver_metal").hide();
                    $(".gold_metal").hide();
                  }

             });

            $("#sortby").change(function(){
                 var p = $("#sortby").val();
                 alert(p)
             });
       });

	
</script>

<script type="text/javascript">
  function scrollToTop() {
        $('html, body').animate({scrollTop:0}, 'slow');
    }

	
</script>
<a href="javascript:scrollToTop()" style="float: right;
    font-weight: bold;
    margin-right: 35px;">Scroll to Top</a>

@endsection