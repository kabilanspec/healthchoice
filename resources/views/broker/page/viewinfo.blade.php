
<h2>Please review the information Below and begin Enrollment.</h2>


<h4>Applicant Information:</h4>
<p>{{$data['firstname']}}</p>
<p>{{$data['lastname']}}</p>
<p>{{$data['email']}}</p>
<p>{{$data['phone']}}</p>
<p>{{$data['address1']}}</p>
<p>{{$data['address2']}}</p>
<p>{{$data['city']}}</p>
<p>{{$data['state']}}</p>
<p>{{$data['plan_effective']}}</p>
<p>{{$data['zipcode']}}</p>
<p>{{$data['country']}}</p>

<h4>Plan Information:</h4>
<p>{{$data['id']}}</p>
<p>{{$data['name']}}</p>
<p>{{$data['price']}}</p>



<div class="enrollConfirmation">
								<span> 
									<input type="checkbox" tabindex="13"><label> I acknowledge Freedom Services's <a href="javascript:void(0);" name="Privacy Statement" title="Privacy Statement" onclick="window.open('https://guide.getmyhealthquotes.com/index.php/privacy',&quot;&quot;,&quot;width=600,height=300,0,status=0,&quot;);">Privacy Statement</a>  and <a href="javascript:void(0);" name="Site Agreement" title="Site Agreement" onclick="window.open('https://guide.getmyhealthquotes.com/index.php/quote_request_agreement',&quot;&quot;,&quot;width=600,height=300,0,status=0,&quot;);">Site Agreement</a> </label>
									<p id="accept_error" style="color:red;"></p>
								</span>
                         </div>
						 
<div class="col-md-6 offset-md-4">
<button style="cursor:pointer" type="submit" class="btn btn-primary">
<a href="/broker/page/userinfo/{{$data['zipcode']}}/{{$data['country']}}/{{$data['id']}}/{{$data['name']}}/{{$data['price']}}">Back</a></button>
</div>


<div class="col-md-6 offset-md-4">
<button style="cursor:pointer" type="submit" class="btn btn-primary">
<a href="https://www.bluecrossmn.com/shop-plans/2019-individual-family-health-plans">Begin Enrollment</a></button>
</div>