@extends('broker.layout.auth')

@section('content')

<div class="row">
          <div class="col-3 align-middle offset-1 px-4 py-4 highlighted_container">Thank you for visiting the <B>{{ @$broker->agency_name }}</B> Exchange.  Please follow the prompts to generate your quote.</div>
          <div class="col-7  align-self-center text-center">
          	<label>Plan Effective Month</label>
          	<form class="form-horizontal" action="{{route('broker.page.effectiveplan')}}" method="GET" enctype="multipart/form-data" role="form">
                          {{csrf_field()}}
             <input type="hidden" value="{{ $broker->id }}" name="broker_id">
             <select class="form-control" name="plan">
             	 
             	 <option value="{{ \Carbon\Carbon::now() }}">{{ \Carbon\Carbon::now()->format('F') }}&nbsp;{{ \Carbon\Carbon::now()->year }}</option>
             	 <option value="{{ \Carbon\Carbon::now()->addMonths(1) }}">{{ \Carbon\Carbon::now()->addMonths(1)->format('F') }}&nbsp;{{ \Carbon\Carbon::now()->addMonths(1)->year }}</option>
             	 <option value="{{ \Carbon\Carbon::now()->addMonths(2) }}">{{ \Carbon\Carbon::now()->addMonths(2)->format('F') }}&nbsp;{{ \Carbon\Carbon::now()->addMonths(2)->year }}</option>
             	 <option value="{{ \Carbon\Carbon::now()->addMonths(3) }}">{{ \Carbon\Carbon::now()->addMonths(3)->format('F') }}&nbsp;{{ \Carbon\Carbon::now()->addMonths(3)->year }}</option>
             	 <option value="{{ \Carbon\Carbon::now()->addMonths(4) }}">{{ \Carbon\Carbon::now()->addMonths(4)->format('F') }}&nbsp;{{ \Carbon\Carbon::now()->addMonths(4)->year }}</option>
             	 <option value="{{ \Carbon\Carbon::now()->addMonths(5) }}">{{ \Carbon\Carbon::now()->addMonths(5)->format('F') }}&nbsp;{{ \Carbon\Carbon::now()->addMonths(5)->year }}</option>
             </select> 
             <button type="submit" class="btn btn-info">Save</button> 
             </form>       
           </div>
          
</div>

@endsection
