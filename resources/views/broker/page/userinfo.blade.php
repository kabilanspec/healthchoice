

<main class="my-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">(* Required Field)</div>
                        <div class="card-body">
                            <form name="my-form" id="userform" action="{{ action('BrokerPageController@apply_online') }}" method="post">
							{{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">*Applicant First Name</label>
                                    <div class="col-md-6">
                                        <input type="text" id="first_name" class="form-control" name="firstname" required>
                                    </div>
                                </div>
								
								<div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">*Applicant Last Name</label>
                                    <div class="col-md-6">
                                        <input type="text" id="last_name" class="form-control" name="lastname" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">*E-mail Address</label>
                                    <div class="col-md-6">
                                        <input type="email" id="email" class="form-control" name="email" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">*Phone</label>
                                    <div class="col-md-6">
                                       <input type="text" name="phone" id="phone" class="form-control" pattern="[7-9]{1}[0-9]{9}" 
										title="Phone number with 7-9 and remaing 9 digit with 0-9" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-4 col-form-label text-md-right">*Address Line 1</label>
                                    <div class="col-md-6">
                                        <input type="text" id="address" class="form-control" name="address1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="present_address" class="col-md-4 col-form-label text-md-right">Address Line2</label>
                                    <div class="col-md-6">
                                        <input type="text" id="address" class="form-control" name="address2">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">*City</label>
                                    <div class="col-md-6">
                                        <input type="text" id="city" class="form-control" name="city" required>
                                    </div>
                                </div>
								
								<div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">*State</label>
                                    <div class="col-md-6">
                                        <input type="text" id="state" class="form-control" name="state" value="{{$data['state_name']}}" required>
                                    </div>
                                </div>
								
								<div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">*Plan Effective From</label>
                                    <div class="col-md-6">
                                        <select id="plan_effective" class="form-control" name="plan_effective" required>
										<option value="Aug 2019">Aug 2019</option>
										<option value="Sep 2019">Sep 2019</option>
										<option value="Oct 2019">Oct 2019</option>
										<option value="Nov 2019">Nov 2019</option>
										<option value="Dec 2019">Dec 2019</option>
										<option value="Jan 2020">Jan 2020</option>
										</select>
                                    </div>
                                </div>
								
								<div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">*Zip Code</label>
                                    <div class="col-md-6">
                                        <input type="text" id="zipcode" class="form-control" name="zipcode" value="{{$data['zipcode']}}" required>
                                    </div>
                                </div>
								
								<div class="form-group row">
                                    <label for="permanent_address" class="col-md-4 col-form-label text-md-right">*Country</label>
                                    <div class="col-md-6">
                                        <input type="text" id="country" class="form-control" name="country" value="{{$data['county']}}" required>
                                    </div>
                                </div>
									<input type="hidden" name="id" value="{{$data['id']}}">
									<input type="hidden" name="name" value="{{$data['name']}}">
									<input type="hidden" name="price" value="{{$data['price']}}">
								
                                    <div class="col-md-6 offset-md-4">
                                        <button style="cursor:pointer" type="submit" class="btn btn-primary">Next</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
            </div>
        </div>
    </div>


