@extends('broker.layout.auth')

@section('content')

<div class="row">
          <div class="col-3 offset-1 px-4 py-4 highlighted_container">Thank you for visiting the <B>{{ @$broker->agency_name }}</B> Exchange.  Please follow the prompts to generate your quote.</div>
          <div class="col-7 align-self-center">
			  
			  		<div class="row">
					  <div class="col-sm-12">
						  <form class="form-horizontal" action="{{route('broker.page.applicant')}}" method="GET" enctype="multipart/form-data" role="form">
                          {{csrf_field()}}
                          <input type="hidden" value="{{ $broker->id }}" name="broker_id">
                          <input type="hidden" value="{{ $county->id }}" name="county_id">
                          <input type="hidden" value="{{ $zip->zipcode }}" name="zipcode">
							<div class="form-row">
								<div class="col-sm-9">Applicant</div>
								<div class="col-sm-3">Tobacco User</div>
						  	</div>
						  	<div class="form-row">
								<div class="form-group col-sm-6">
									  <div class="input-group">
										<div class="input-group-prepend">
										  <span class="input-group-text">Date of Birth</span>
										</div>
										<input required="" id="datefield" type="date" min="1919-01-01"  name="applicant_dob" class="form-control">
									  </div>
								</div>
								<div class="form-group col-sm-3 ">
									<div  class="btn-group btn-group-toggle" data-toggle="buttons">
									  <label id="applicant_male" class="btn btn-light active1">
										<input type="radio"  required="" name="applicant_gender" value="male"  autocomplete="off" onclick="active_btn('applicant_male','active1')" >Male
									  </label>
									  <label id="applicant_female" class="btn btn-light active1">
										<input type="radio" required="" name="applicant_gender" value="female"  autocomplete="off" 
										onclick="active_btn('applicant_female','active1')"
										 >Female
									  </label>
									</div>
								</div>
								<div class="form-group col-sm-2">
									<div id="applicant_tobacco" class="btn-group btn-group-toggle" data-toggle="buttons">
									  <label id="applicant_smoker" class="btn btn-light active2">
										<input type="radio" required="" name="applicant_tobacco" value="yes"  
										onclick="active_btn('applicant_smoker','active2')"	

										autocomplete="off" >Yes
									  </label>
									  <label id="applicant_nonsmoker"  class="btn btn-light active2">
										<input type="radio" required="" name="applicant_tobacco" value="no" 

										onclick="active_btn('applicant_nonsmoker','active2')"	
										autocomplete="off" >No
									  </label>
									</div>
								</div>
								<div class="form-group col-sm-1">&nbsp;</div>
						  	</div>
						  	<div class="form-row">
								<div class="col-sm-9">Spouse</div><div id="addspouse" class="form-group col-sm-1 addspouse"><button type="button"  class="btn btn-success float-right"><i class="fas fa-plus-circle remove_applicant"></i></button></div>
								<div class="col-sm-3 st">Tobacco User</div>
						  	</div>
						  	<div class="form-row st" id="spousedata">
								<div class="form-group col-sm-6">
									  <div class="input-group">
										<div class="input-group-prepend">
										  <span class="input-group-text">Date of Birth</span>
										</div>
										<input type="date" id="spouse_dob" name="spouse_dob" class="form-control">
									  </div>
								</div>
								<div class="form-group col-sm-3 ">
									<div class="btn-group btn-group-toggle" data-toggle="buttons">
									  <label id="spouse_male" class="btn btn-light active3">
										<input  type="radio"  name="spouse_gender" value="male"  autocomplete="off"
										onclick="active_btn('spouse_male','active3')"

										>Male
									  </label>
									  <label id="spouse_female" class="btn btn-light active3">
										<input type="radio" name="spouse_gender" value="female" 
										onclick="active_btn('spouse_female','active3')"
										 autocomplete="off">Female
									  </label>
									</div>
								</div>
								<div class="form-group col-sm-2">
									<div id="spouse_tobacco" class="btn-group btn-group-toggle" data-toggle="buttons">
									  <label id="spouse_smoker" class="btn  btn-light active4">
										<input type="radio" class="stt" name="spouse_tobacco" value="yes"  autocomplete="off"
										onclick="active_btn('spouse_smoker','active4')"
										>Yes
									  </label>
									  <label id="spouse_nonsmoker" class="btn btn-light active4">
										<input type="radio" class="stt" name="spouse_tobacco" value="no"  autocomplete="off"
										onclick="active_btn('spouse_nonsmoker','active4')"
										>No
									  </label>
									</div>
								</div>
								<div class="form-group col-sm-1"><button id="spouseremove" type="button" class="btn btn-danger float-right"><i class="fas fa-minus-circle remove_applicant"></i></button></div>
						  	</div>
						<!--   	<div class="form-row">
								<div class="col-sm-9">Child 1</div>
								<div class="col-sm-3">Tobacco User</div>
						  	</div>
						  	<div class="form-row">
								<div class="form-group col-sm-6">
									  <div class="input-group">
										<div class="input-group-prepend">
										  <span class="input-group-text">Date of Birth</span>
										</div>
										<input type="date" class="form-control">
									  </div>
								</div>
								<div class="form-group col-sm-3 ">
									<div class="btn-group btn-group-toggle" data-toggle="buttons">
									  <label class="btn btn-light">
										<input type="radio" name="options" id="child_male#1" autocomplete="off">Male
									  </label>
									  <label class="btn btn-light">
										<input type="radio" name="options" id="child_female#1" autocomplete="off">Female
									  </label>
									</div>
								</div>
								<div class="form-group col-sm-2">
									<div id="child_tobacco#1" class="btn-group btn-group-toggle" data-toggle="buttons">
									  <label class="btn btn-light">
										<input type="radio" name="options" id="child_smoker#1" autocomplete="off">Yes
									  </label>
									  <label class="btn btn-light">
										<input type="radio" name="options" id="child_nonsmoker#1" autocomplete="off">No
									  </label>
									</div>
								</div>
								<div class="form-group col-sm-1"><button type="submit" class="btn btn-danger float-right"><i class="fas fa-minus-circle remove_applicant"></i></button></div>
						  	</div> -->
     
                            <div  id="InputsWrapper" class="Shuttle-points" style="margin-bottom: 10px;">
					          <input type="hidden" id="check_id" value="1" >
									</div>
 

						  	<div class="form-row"><button id="AddMoreFileBox" type="button" class="btn btn-success"><i class="fas fa-plus-circle"></i> Add Child</button></div>
						  	<button type="submit" class="btn btn-primary float-right">View Plans <i class="fas fa-arrow-circle-right"></i></button>
						  
						  
						  </form>
						  
						  </div></div>
			  

			  </div>
	   		</div>

@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function () {
        
        $("#addspouse").hide();
        var MaxInputs       = 4;
        var InputsWrapper   = $("#InputsWrapper");
        var AddButton       = $("#AddMoreFileBox");
        var spouseremove       = $("#spouseremove");
        var spouseshow =  $("#addspouse"); 
        var x = InputsWrapper.length; 
        var FieldCount=0; 
        var childCount=1;
        var checkval = $('#check_id').val();

        /*if(x == 0){
          var FieldCount=0;
        }*/
      
        $(AddButton).click(function (e) 
        {
        	var checkval = $('#check_id').val();
        	
        	if(checkval == 0) {

        		FieldCount=0;
        		
        	}

        	if(checkval <= MaxInputs)
            {
                    FieldCount++;
                    childCount++;
                    $('#check_id').val(FieldCount);
                    $(InputsWrapper)
                    .append('<div id="po'+FieldCount+'" ><div class="form-row"><div id="childrem'+FieldCount+'" class="col-sm-9">Child '+FieldCount+'</div><div class="col-sm-3">Tobacco User</div></div><div class="form-row"><div class="form-group col-sm-6"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">Date of Birth</span></div><input  id="child_dob'+FieldCount+'" type="date" name="child_dob[]" class="form-control"></div></div><div class="form-group col-sm-3 "><div class="btn-group btn-group-toggle" data-toggle="buttons"><label id="child_male'+FieldCount+'"  class="btn btn-light active5"><input onclick="activee_btn('+FieldCount+',1)" type="radio" name="child_gender[]" value="male" autocomplete="off">Male</label><label id="child_female'+FieldCount+'"  class="btn btn-light gender'+FieldCount+'"><input type="radio" onclick="activee_btn('+FieldCount+',2)" name="child_gender[]" value="female" id="child_female#1" autocomplete="off">Female</label></div></div><div class="form-group col-sm-2"><div id="child_tobacco#1" class="btn-group btn-group-toggle" data-toggle="buttons"><label class="btn btn-light tobacco'+FieldCount+'" id="child_smoker'+FieldCount+'" ><input type="radio"  onclick="activee_btn('+FieldCount+',3)" name="child_tobacco[]" value="yes"   autocomplete="off">Yes</label><label class="btn btn-light tobacco'+FieldCount+'"  id="child_nonsmoker'+FieldCount+'" ><input type="radio" name="child_tobacco[]" onclick="activee_btn('+FieldCount+',4)" value="no" autocomplete="off">No</label></div></div><div class="form-group col-sm-1"><button  onclick="childremove('+FieldCount+')" type="button" class="btn btn-danger float-right"><i class="fas fa-minus-circle remove_applicant"></i></button></div></div></div>');
                    x++;
                    } 

                   $("#spouse_dob").change(function (e) 
                   {
                    if($('#po'+FieldCount).is(":visible")){
            	
		            	$('#child_dob'+FieldCount).prop('required',true);
		            	$("input[name*='child_gender']").prop('required',true);
            	        $("input[name*='child_tobacco']").prop('required',true);
                       
		            }else{
		            	$('#child_dob'+FieldCount).prop('required',false);
		            }
		            })

                    
                    initMap();
                    return false;
        })
        $(spouseremove).click(function (e) 
        {
           $(".st").hide();
           var doc_val_check = "";
           $('#spouse_dob').val(doc_val_check);
           $("#addspouse").show();
           /*$("#spouse_dob").prop('required',false);
           $("input[name*='spouse_gender']").prop('required',false);
           $("input[name*='spouse_tobacco']").prop('required',false);*/
            
        })
        $(spouseshow).click(function (e) 
        {
           $(".st").show();
           $("#addspouse").hide();

        })

        $("#spouse_dob").change(function (e) 
        {
        	
            if($(".st").is(":visible")){
            	
            	$("#spouse_dob").prop('required',true);
            	$("input[name*='spouse_gender']").prop('required',true);
            	$("input[name*='spouse_tobacco']").prop('required',true);
            }

        })
        
        
       

        });

    function childremove(id)
    {
    	
        $('#po'+id).hide();
        var doc_val_check = "";
        $('#child_dob'+id).val(doc_val_check);
        var up = $('#check_id').val();
        var  upp = up - 1;
        $('#check_id').val(upp);
        
    }

	function active_btn(id,selector)
	{
		//alert(selector)
		
		$('.'+selector).removeClass('checks');
		$('#'+id).addClass('checks');
	}

	function activee_btn(id,choose)
	{
		if(choose == 1){
			$('#child_male'+id).addClass('checks');
			$('#child_female'+id).removeClass('checks');
		}if(choose == 2){
			$('#child_male'+id).removeClass('checks');
			$('#child_female'+id).addClass('checks');
		}if(choose == 3){
            $('#child_smoker'+id).addClass('checks');
			$('#child_nonsmoker'+id).removeClass('checks');
		}if(choose == 4){
			$('#child_smoker'+id).removeClass('checks');
			$('#child_nonsmoker'+id).addClass('checks');
		}
		
		//$('.'+selector).removeClass('checks');
		//$('#child_male'+id).addClass('checks');
	}

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	 if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 

	today = yyyy+'-'+mm+'-'+dd;
	document.getElementById("datefield").setAttribute("max", today);

</script>

@endsection