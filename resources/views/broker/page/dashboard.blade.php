@extends('broker.layout.auth')

@section('content')

<div class="row">
          <div class="col-3 align-middle offset-1 px-4 py-4 highlighted_container">Thank you for visiting the <B>{{ @$broker->agency_name }}</B> Exchange.  Please follow the prompts to generate your quote.</div>
          <div class="col-7  align-self-center text-center">
          	
                   <a href="{{ route('broker.page.plan' , @$broker->website ) }}" class="btn btn-primary">START QUOTE NOW  <i class="fas fa-arrow-circle-right"></i></a></div>
</div>

@endsection
