@extends('broker.layout.auth')

@section('content')

 <div class="row search-info-compare-plans">
          <div class="col-12 left-border-accent">
			  <strong>Your Search for {{ $zipcode }} ({{ $county }})</strong>
			<ul class="search-criteria">
				<?php
					  if(empty($spouse) && sizeof($child) == 0){
					  	 $ded_count = 1;
					  	}else{
					  	 $ded_count = 2;	
					  	}
				?>
				<li><strong>Applicant</strong> {{ $self['gender']}}, {{ $self['age'] }}, @if($self['smoker'] == 'yes') Smoker @else  Non Smoker @endif</li>
				@if(!empty($spouse))
				<li><strong>Spouse</strong> {{ @$spouse['gender']}}, {{ @$spouse['age'] }}, @if(@$spouse['smoker'] == 'yes') Smoker @else  Non Smoker @endif</li>
				@endif
				@foreach($child as $key => $children)
                      <?php 
                            $userDob = $children->dob;
				            $dob = new DateTime($userDob);
				            $now = new DateTime();
				            $difference = $now->diff($dob);
				            $age = $difference->y;
                        ?>
				<li><strong>Child {{ $key +1 }}</strong> {{ $children['gender']}},{{ $age}}, @if($children['smoker'] == 'yes') Smoker @else  Non Smoker @endif  </td></li>
				@endforeach
			  </ul>
		   </div>
		</div>

		<div class="row">
          <div class="col-12">
			<table class="table compare-plans">
				<thead>
				  <tr>
				
				  	<th scope="col" width="20%" class="align-content-center"><p>{{ count($plans)}} Plan(s) Selected</p>
					<a href="{{ url()->previous() }}"><button class="btn btn-primary btn-xs float-left mx-2" type="submit">Back</button></a>
					<button type="submit" class="btn btn-primary btn-xs float-left mx-2">Email <i class="fas fa-paper-plane"></i></button>
					<button type="submit" onclick="compare_print()" class="btn btn-primary btn-xs float-left mx-2">Print <i class="fas fa-print"></i></button></th>
				  	@foreach($plans as $key => $plan)
					
					
					<th scope="col" width="16%"><div class="card compare-card">
						  <img class="card-img-top img-fluid compare-card-logo" src="{{ url('frontend/images',$plan->image) }}"  alt="Card image">
						  <div class="card-body compare-card-data">
							<h4 class="card-title compare-card-plan-name">{{ $plan->PLAN_NAME_T }}</h4>
							<p class="card-text compare-card-premium">${{ @$plan->premium ?? 0.00 }}</p>
							<p class="card-text compare-card-premium">
							<a href="/broker/page/userinfo/{{ $zipcode }}/{{ $county }}/{{ $plan->id }}/{{ $plan->PLAN_NAME_T }}/${{ @$plan->premium ?? 0.00 }}" class="btn btn-primary btn-block btn-sm">Apply</a>
							</p>
						  </div>
						</div></th>

						 
						@endforeach
					
				  </tr>
				</thead>
				<tbody>
				  <tr>
					<th scope="row">Plan Type</th>
					<!-- <td>Plan 1</td>
					<td>Plan 2</td>
					<td>Plan 3</td>
					<td>Plan 4</td>
					<td>Plan 5</td> -->
				  </tr>
				  <tr>
					<th scope="row">Metal Level</th>
					
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->METAL_LEVEL_T }}</td>
                      @endforeach
					@endforeach
					
				  </tr>
				  <tr>
					<th scope="row">HSA Qualified</th>
				  @foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->HSA_B }}</td>
                      @endforeach
				  @endforeach
				  </tr>
				  <tr>
					<th scope="row">In-Network Deductible (Single/Family)</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
					    @foreach($mta->subplanded as $key2 => $ma)
					     @if($ded_count == 1)
                          <td>${{ floatval(@$ma->DEDUCTIBLE_SINGLE_C) }}</td>
                         @else 
                         <td>${{ floatval(@$ma->DEDUCTIBLE_FAMILY_C) }}</td>
                         @endif
                       @endforeach
                      @endforeach
					@endforeach
					
				  </tr>
				  <tr>
					<th scope="row">In-Network Out-of-Pocket (Single/Family)</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
					    @foreach($mta->subplanded as $key2 => $ma)
					     @if($ded_count == 1)
                          <td>${{ floatval(@$ma->DEDUCTIBLE_SINGLE_C) }}</td>
                         @else 
                         <td>${{ floatval(@$ma->DEDUCTIBLE_FAMILY_C) }}</td>
                         @endif
                       @endforeach
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Primary care visit to treat an injury or illness</th>
					 @foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->OV_PRIMARY_CARE_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Specialist visit</th>
					 @foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->OV_SPECIALIST_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Other practitioner office visit</th>
					 @foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->OV_OTHER_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Diagnostic test (x-ray, blood work)</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->TEST_DIAGNOSTIC_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Imaging (CT/PET scans, MRIs)</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->TEST_IMAGING_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Generic drugs</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RX_GENERIC_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Formulary or Preferred brand drugs</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RX_FORMULARY_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Non-formulary or non-preferred brand drugs</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RX_NONFORMULARY_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Specialty drugs</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RX_SPECIALTY_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Facility fee (e.g., ambulatory surgery center)</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->HOSPITAL_FACILITY_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Physician/surgeon fees</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->HOSPITAL_PHYSSUR_M }}</td>
                      @endforeach
					@endforeach
				  </tr>
				  <tr>
					<th scope="row">Emergency room services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->ER_ER_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Emergency medical transportation</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->ER_TRANSPORT_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Urgent care</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->ER_URGENT_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Facility fee (e.g., hospital room)</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->HOSPITAL_FACILITY_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  
				  <tr>
					<th scope="row">Mental/Behavioral health outpatient services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->MH_OUTPATIENT_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Mental/Behavioral health inpatient services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->MH_INPATIENT_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Substance use disorder outpatient services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->MH_SUBSTANCE_OUT_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Substance use disorder inpatient services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->MH_SUBSTANCE_IN_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Prenatal and postnatal care</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->BABY_PREPOST_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Delivery and all inpatient services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->BABY_DELIVER_IN_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Home health care</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RECOVER_HOMEHEALTH_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Rehabilitation services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RECOVER_REHAB_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Habilitation services</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RECOVER_HABIL_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Skilled nursing care</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RECOVER_SKILLNURSE_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Durable medical equipment</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RECOVER_MEDEQUIP_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Hospice service</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->RECOVER_HOSPICE_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Pediatric eye exam</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->PED_EYEEXAM_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Pediatric glasses</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->PED_GLASSES_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				  <tr>
					<th scope="row">Pediatric dental check-up</th>
					@foreach($plans as $key => $meta)
					  @foreach($meta->subplan as $key1 => $mta)
                      <td>{{ @$mta->PED_DENTAL_M }}</td>
                      @endforeach
                      @endforeach
				  </tr>
				</tbody>
			  </table>
		   	
			  
		   </div>
		</div>

@endsection

<script>
function compare_print() {
  window.print();
}
</script>