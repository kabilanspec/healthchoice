@extends('broker.layout.auth')

@section('content')

<div class="row">
          <div class="col-3 align-middle offset-1 px-4 py-4 highlighted_container">Thank you for visiting the <B>{{ @$broker->agency_name }}</B> Exchange.  Please follow the prompts to generate your quote.</div>
          <div class="col-7 align-self-center">
          	<form class="form-horizontal" action="{{route('broker.page.store')}}" method="GET" enctype="multipart/form-data" role="form">
                          {{csrf_field()}}
			  		<div class="form-row text-center">
					  <div class="form-group col-6 offset-3">
						<label for="exampleInputEmail1">What zip code do you live in?</label>
						<input type="hidden" value="{{ $broker->id }}" name="broker_id">
						<input type="hidden" value="{{ $zipcode_id }}" name="zip_id">
						<input required="" type="text" class="form-control" name="zipcode" id="exampleInputEmail1" placeholder="ZIP code">
					  </div>
						</div>
					  <button type="submit" class="btn btn-primary float-right">Continue  <i class="fas fa-arrow-circle-right"></i></button>
					</form>
			  </div>
	   		</div>

@endsection