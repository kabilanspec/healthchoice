@extends('broker.layout.auth')

@section('content')

<div class="row">
          <div class="col-3 align-middle offset-1 px-4 py-4 highlighted_container">Thank you for visiting the <B>{{ @$broker->agency_name }}</B> Exchange.  Please follow the prompts to generate your quote.</div>
          <div class="col-7 align-self-center">
			  
			  		<div class="row">
					  <div class="col-8 offset-2 text-center">
			  	Which county do you live in?</div></div>
			  
			  		<div class="row">
					  <div class="col-8 offset-2 text-center">
					  	<form class="form-horizontal" action="{{route('broker.page.county')}}" method="GET" enctype="multipart/form-data" role="form">
                          {{csrf_field()}}

					  		<input type="hidden" value="{{ $zipcode_id }}" name="zipcode_id">
					  		<select required="" class="form-control" name="county_id">
					  			<option value="">Choose County</option>
					  			@foreach($county as $index => $counties)
                                <option value="{{ $counties->id }}">{{ @$counties->county_name }}</option>  
					  			@endforeach
					  		</select>
					  		<button type="submit" class="btn btn-success">Save County Information</button>
					  	</form>
			  	</div></div>
			  

			  </div>
	   		</div>

@endsection