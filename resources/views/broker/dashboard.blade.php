@extends('layouts.loggedin')

@section('content')

<body>
    
    <div class="container">
    	
    	
       <div class="row py-2">
       	@if (session()->has('flash_success'))
                        <div class="alert alert-success">
                            {!! session()->get('flash_success')!!}        
                        </div>
                @endif
                @if (session()->has('flash_error'))
                        <div class="alert alert-danger">
                            {!! session()->get('flash_error')!!}        
                        </div>
                @endif
          <div class="col-lg-2 offset-lg-1">

			  <p></p>
			  <br>
			  <br>
			  <br>
			  <br>
			  @if($broker->avatar)
			  <div class="card">
				  <img src="{{$broker->avatar}}" width="200px" height="100px">
				</div>
				@endif
		   </div>
		</div>
       <div class="row py-2">
       	

          <div class="col-lg-10 offset-lg-1">
			  <div class="card">
				  @foreach($datas as $key => $data)
				  <div class="modal" tabindex="-1" role="dialog" id="new-carrier-modal-{{$data['state_id']}}">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title">Add New Minnesota Carrier</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							  <form action="{{url('/broker/carrier/create')}}" method="post">
							  	{{csrf_field()}}
							  	<input type="hidden" name="state_id" value="{{$data['state_id']}}">
							    <div class="form-group">
								<label for="exampleFormControlSelect2">Select a carrier</label>
								<select class="form-control" name="carrier_id" id="exampleFormControlSelect2">
								  <option value="">Carrier Name</option>
								  @foreach($carriers as $carrier)
								  	@if(isset($data['carrier']) && !empty($data['carrier']))
									  	@if(!in_array($carrier->carrier, $data['carrier']))
									  		<option value="{{$carrier->id}}">{{$carrier->carrier}}</option>
									  	@endif
									@else
										<option value="{{$carrier->id}}">{{$carrier->carrier}}</option>
									@endif
								  @endforeach
								</select>
							  </div>
								<label for="exampleFormControlSelect1">Enrollment Year</label>
								<div class="form-group">
									<select name="enrollment" class="form-control" id="exampleFormControlSelect1">
									  <option>2019</option>
									  <option>2020</option>
									</select>
								  </div>
								  
						  <div class="form-group">
							<label for="exampleInputURL">Link to Apply</label>
							<input type="text" name="carrier_url" class="form-control" id="exampleInputURL" aria-describedby="URLHelp" placeholder="Enter application URL">
						  </div>
								  
								  
						  </div>
						  <div class="modal-footer">
							<input type="submit" class="btn btn-success" name="Save changes">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						  </form>
						</div>
					  </div>
					</div>
					

				  <div class="tab-content" id="mn-pills-tab-content">
				  	<div class="card-header">
					<ul class="nav nav-pills card-header-pills" id="mn-pills-tab">
					  <li class="nav-item">
						<a class="nav-link disabled" href="#" id="mn-pills-label-tab">{{$data['state']}}</a>
					  </li>
					  @if(isset($data['carrier']))
					  <?php $i = 0;?>
					  @foreach($data['carrier'] as $key => $carrier_name)
					  <li class="nav-item">
					  	<input type="hidden" id="{{$data['state'].'_'.$data['carrier_id'][$i]}}" name="carrier_id" value="{{$data['carrier_id'][$i]}}">
						<a class="nav-link @if($i == 0) {{'active'}} @endif" id="mn-pills-medica-tab-{{$data['state_id'].'_'.$data['carrier_id'][$i]}}" data-toggle="pill" href="#mn-pills-medica-{{$data['state_id'].'_'.$data['carrier_id'][$i]}}" role="tab" >{{$carrier_name}}</a>
					  </li>
					  <?php $i++;?>
					  @endforeach
					  @endif
					  <li class="nav-item">
						<a class="nav-link btn-success" id="mn-pills-add-tab" data-toggle="modal" href="#new-carrier-modal-{{$data['state_id']}}" role="tab"><i class="fas fa-plus-circle"></i> Add New Carrier</a>
					  </li>
					</ul>
				  </div>
				  @if(isset($data['s']))
				  <?php $i = 0;?>
				  @foreach($data['s'] as $key => $vals)
				 <div class="card-body tab-pane fade show @if($i == 0) {{'active'}} @endif" id="mn-pills-medica-{{$data['state_id'].'_'.$data['carrier_id'][$i]}}" role="tabpanel">
					<h5 class="card-title">Application URLs</h5>
					<table class="table">
						<thead class="thead-light">
						  <tr>
							<th style="width: 20%">Enrollment Year</th>
							<th style="width: 60%">URL</th>
							<th style="width: 20%">Actions</th>
						  </tr>
						</thead>
						<tbody>
							@foreach($vals as $val)
							<!-----------Edit------------->
					<div class="modal" tabindex="-1" role="dialog" id="new-carrier-edit-{{$val['broker_url_id']}}">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title">Edit <strong> {{$data['state']}} </strong>Carrier</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							  <form action="{{url('/broker/carrier_management/'.$val['broker_url_id'])}}" method="post">
							  	{{csrf_field()}}
							  	<input type="hidden" name="state_id" value="{{$data['state_id']}}">
							  	<input type="hidden" name="">
								<label for="exampleFormControlSelect1">Enrollment Year</label>
								<div class="form-group">
									<select name="enrollment" class="form-control" id="exampleFormControlSelect1">
									  <option value="2019" @if($val['enrollment'] == 2019) {{'selected'}} @endif>2019</option>
									  <option value="2020" @if($val['enrollment'] == 2020) {{'selected'}} @endif>2020</option>
									</select>
								  </div>
								  
						  <div class="form-group">
							<label for="exampleInputURL">Link to Apply</label>
							<input type="text" name="carrier_url" value="{{$val['url']}}" class="form-control" id="exampleInputURL" aria-describedby="URLHelp" placeholder="Enter application URL">
						  </div>
								  
								  
						  </div>
						  <div class="modal-footer">
							<input type="submit" data = {{$val['broker_url_id']}}  class="btn btn-success" name="Save changes">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						  </form>
						</div>
					  </div>
					</div>
                     <!--------model--->
                     <!---add application url---->
                     <div class="modal" tabindex="-1" role="dialog" id="new-carrier-app-{{$data['state'].'_'.$val['carrier_id']}}">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title">Add New <strong> {{$data['state']}} </strong>Carrier</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							  <form action="{{url('/broker/carrier/create')}}" method="post">
							  	{{csrf_field()}}
							  	<input type="hidden" name="state_id" value="{{$data['state_id']}}">
							  	<input type="hidden" name="carrier_id" value="{{$val['carrier_id']}}">
							  	<input type="hidden" name="">
								<label for="exampleFormControlSelect1">Enrollment Year</label>
								<div class="form-group">
									<select name="enrollment" class="form-control" id="exampleFormControlSelect1">
									  <option value="2019">2019</option>
									  <option value="2020">2020</option>
									</select>
								  </div>
								  
						  <div class="form-group">
							<label for="exampleInputURL">Link to Apply</label>
							<input type="text" name="carrier_url"  class="form-control" id="exampleInputURL" aria-describedby="URLHelp" placeholder="Enter application URL">
						  </div>
								  
								  
						  </div>
						  <div class="modal-footer">
							<input type="submit" data = {{$val['broker_url_id']}}  class="btn btn-success" name="Save changes">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						  </form>
						</div>
					  </div>
					</div>
                     <!---model--->
						  <tr>
							<td>{{$val['enrollment']}}</td>
							<td><a href="{{$val['url']}}" target="_blank">{{$val['url']}}</a></td>
							<td>
									<a href="#" class="btn btn-primary btn-sm mr-1" data-toggle="modal" data-target="#new-carrier-edit-{{$val['broker_url_id']}}"><i class="fas fa-edit"></i></a>
									<a href="{{url('broker/carrier_management/'.$val['broker_url_id'])}}" class="btn btn-danger btn-sm"><i class="fas fa-minus-circle"></i></a>
								</td>
						  </tr>
						  @endforeach
						</tbody>
					  </table>


					<a href="#" class="btn btn-success" data-toggle="modal" data-target="#new-carrier-app-{{$data['state'].'_'.$val['carrier_id']}}"><i class="fas fa-plus-circle"></i> Add Application URL</a>
				  </div>
				  <?php $i++;?>
				  @endforeach
				  @endif



				</div>
				@endforeach


				  </div>
		   
		   
		   </div>
		   <div id="myModal-{{$key}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Carrier</h4>
      </div>
      <form action="{{url('broker/carrier_management')}}" method="POST">
      	{{csrf_field()}}
      <div class="modal-body">
        <div class="form-group">
            <label>
                <p class="label-txt">Enrollment Year</p>
                <input type="text" class="form-control"  value="" name="enrollment" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
                
            </label>
        </div>
        <input type="hidden" name="broker_carrier_id" value="">
        <div class="form-group">
            <label>
                <p class="label-txt">Application URL ( fdgdf )</p>
                <input type="text" class="form-control"  value="" name="url" class="input">
                <div class="line-box">
                    <div class="line"></div>
                </div>
                
            </label>
        </div>
      </div>
       <div class="form-group text-center">
                    <button type="submit" class="btn btn-info submitBtn">Save</button>
                </div>
  </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
		   
       </div>
       
       <br>
       <hr>
       <br>
       
       <div class="row">
          <div class="text-center col-6 offset-3">
             <h4><img src="{{url('img/honeycomb.svg')}}" width="49px" height="25px" alt="HealthChoice" class="brand-logo">HealthChoice</a></h4>
             <p>Copyright &copy; 2019 &middot; All Rights Reserved &middot; <a href="http://ichra.online" >ICHRA</a></p>
          </div>
       </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
    <script src="{{asset('/js/jquery.min.js')}}"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    <script src="{{asset('/js/popper.min.js')}}"></script>
    <script src="{{asset('/js/bootstrap.min.js')}}"></script>

    <script type="text/javascript">
    	
    </script>
	  
  </body>



