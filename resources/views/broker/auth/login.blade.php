@extends('layouts.new')

@section('content')

<style type="text/css">
    .alert-success{
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        margin-top: 25px;
    }
</style>

            <section>
                <div class="container">
                    @if (session()->has('flash_success'))
                        <div class="alert alert-success">
                            {!! session()->get('flash_success')!!}        
                        </div>
                @endif
                @if (session()->has('flash_error'))
                        <div class="alert alert-danger">
                            {!! session()->get('flash_error')!!}        
                        </div>
                @endif
                    <div class="row">
                   
                        <div class="col-md-5">
                        
                            <form class="loginSectionform" action="{{url('broker/login')}}" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Email</p>
                                        <input type="text" name="email" value="{{old('email')}}" class="input  form-control">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Password</p>
                                        <input type="password" name="password"  class="input form-control">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="forgetSec">
                                    <a href="{{url('broker/password/reset')}}" class="float-left">Forget Password?</a>
                               </div>
                                <div class="form-group">
                                    <button type="submit" class="submitBtn">Login</button>
                                </div>
                            </form>
                       
                            <p class="dontHave">Don't have an Account? <a href="{{url('broker/register')}}">Create your Account</a></p>
                        
                        </div>
                    </div>
                </div>
            </section>


@endsection


