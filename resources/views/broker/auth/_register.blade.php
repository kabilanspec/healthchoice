@extends('layouts.new')

@section('content')

<section>
<div class="container">
    @if (session()->has('flash_success'))
                        <div class="alert alert-success">
                            {!! session()->get('flash_success')!!}        
                        </div>
                @endif
                @if (session()->has('flash_error'))
                        <div class="alert alert-danger">
                            {!! session()->get('flash_error')!!}        
                        </div>
                @endif

<div class="row">
    <h3>Agency Registration</h3>
</div>

    <div class="row" style="background-color: #ded9cc;">

        <div class="col-md-12">
            <div class="col-6 offset-md-3">
             <form class="loginSectionform" id="Registration" method="POST" action="{{ url('broker/register') }}" aria-label="{{ __('Register') }}">
                {{csrf_field()}}
                <h4 style="background-color: #c6ccc7;">Contact Information</h4>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Agency Name *</p>
                        <input type="text" class="form-control"  value="{{old('agency_name')}}" name="agency_name" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('agency_name'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('agency_name') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">First Name *</p>
                        <input type="text" class="form-control"  value="{{old('first_name')}}" name="first_name" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Last Name *</p>
                        <input type="text" class="form-control"  value="{{old('last_name')}}" name="last_name" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Email *</p>
                        <input type="text" class="form-control"  value="{{old('email')}}"  name="email" class="input" >
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Agency Address (Line 1) *</p>
                        <input type="text" class="form-control"  value="{{old('agency_address_line_1')}}" name="agency_address_line_1" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('agency_address_line_1'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('agency_address_line_1') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Agency Address (Line 2)</p>
                        <input type="text" class="form-control" name="agency_address_line_2" value="{{old('agency_address_line_2')}}" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('agency_address_line_2'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('agency_address_line_2') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">City *</p>
                        <input type="text" class="form-control"  name="city" value="{{old('city')}}" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('city'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
           
                <div class="form-group">
                    <label>
                        <p class="label-txt">Phone Local*</p>
                        <input type="text" class="form-control"  value="{{old('phone')}}" name="phone" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                
                <div class="form-group">
                    <label>
                        <p class="label-txt">Phone Tollfree</p>
                        <input type="text" class="form-control" name="phone_tollfree" value="{{old('phone_tollfree')}}" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('phone_tollfree'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('phone_tollfree') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Website *</p>
                        <input type="text" style="width:250px;display:inline;" class="form-control" value="{{old('website')}}" name="website" class="input">
                        <span style="display:inline">&nbsp;.healthchoice.app</span>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('website'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('website') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Password *</p>
                        <input type="password" value="{{old('password')}}"  class="form-control" name="password" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <p class="label-txt">Password Confirmation *</p>
                        <input type="Password" class="form-control" value="{{old('password_confirmation')}}" name="password_confirmation" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>


                <div class="form-group">
                    <label>
                        <p class="label-txt">State *</p>
                        <select id="inputState" name="state_id" class="form-control">
                          <option value="">Choose State...</option>
                          @foreach($state  as $index => $states)
                          <option value="{{ $states->id }}" @if(old('state_id') == $states->id) {{'selected'}} @endif>{{  $states->full_name }}</option>
                          @endforeach
                        </select>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('state_id'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('state_id') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>


                <div class="form-group">
                    <label>
                        <p class="label-txt">Zipcode *</p>
                        <input type="text" class="form-control" name="zipcode" value="{{old('zipcode')}}" class="input">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('zipcode'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('zipcode') }}</strong>
                            </span>
                        @endif
                    </label>
                </div>
                
                <h4 style="background-color: #c6ccc7;">Other Information</h4>
                <!-- <div class="form-group">
                    <label>
                        <p class="label-txt">Which Carriers Do You Represent? * </p>
                    </label>
                    @foreach($carriers  as $key => $carrier)
                    <div class="row">
                        <div class="col-md-4">
                        <label>{{ $carrier->carrier  }}</label>
                    </div>
                    <div class="col-md-8">
                        <input type="checkbox" value="{{ $carrier->id }}" name="carrier_id[]"  
                         @if(!empty(old('carrier_id')))@if(in_array($carrier->id, old('carrier_id'))) {{'checked'}} @endif @endif>
                    </div>
                    </div>
                    
                      
                    @endforeach
                    <div class="line-box">
                            <div class="line"></div>
                        </div>
                        @if ($errors->has('carrier_id'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('carrier_id') }}</strong>
                            </span>
                        @endif

                </div> -->
                <h4 style="background-color: #c6ccc7;">Disclaimer</h4>
                <div style="overflow: scroll;width:585px;height: 200px;">
                    <h5 style="background-color: #ebc252;text-align: center;font-weight: bold;">AGENT-BROKER PORTAL SUBSCRIPTION AGREEMENT</h5>
                <B>IMPORTANT: THIS AGENT-BROKER PORTAL SUBSCRIPTION AGREEMENT</B><p style="font-size: 12px;"> ("Agreement") is by and between Freedom Services, Inc. ("FSI") and the individual or entity REGISTERING FOR THIS SERVICE ("AGENT-BROKER"), and shall be effective upon FSI's notification of acceptance of AGENT-BROKER's subscription TO FSI's CUSTOMIZED individual health insurance quote PORTAL. THIS IS A BINDING LEGAL AGREEMENT BETWEEN AGENT-BROKER AND FSI. BY ACCESSING THIS WEBSITE, AGENT-BROKER is indicating AGENT-BROKER'S acknowledgment and acceptance of THIS AGREEMENT, without limitation or QUALIFICATION. THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE SUBJECT TO CHANGE BY FSI AT ANY TIME IN ITS DISCRETION. AGENT-BROKER'S USE OF THIS WEBSITE AFTER SUCH CHANGES ARE POSTED (OR OTHERWISE PROVIDED TO AGENT-BROKER) CONSTITUTES AGENT-BROKER's ACKNOWLEDGMENT AND ACCEPTANCE OF SUCH CHANGES</p>.
                   </div>

                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="is_agreed" value="0" class="custom-control-input" id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">I agree to the Terms and Conditions</label>
                    @if ($errors->has('is_agreed'))
                        <span class="help-block">
                            <strong style="color: red">{{ $errors->first('is_agreed') }}</strong>
                        </span>
                    @endif
                </div>
                
               
           <script src='https://www.google.com/recaptcha/api.js' async defer></script>
           <div class="g-recaptcha" data-sitekey="6LcJXrMUAAAAAFtZcoeVSyGOnpbCXDi-i1EQxqop"></div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-info submitBtn">Save & Continue</button>
                </div>
            </form>
            <p class="dontHave">Do you have an Account? <a href="{{url('broker/login')}}">Login</a></p>
  
        </div>
            
        </div>
    </div>
</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#Registration').submit(function(){
            var response = grecaptcha.getResponse();
            if(response.length == 0) {
                if($('.captcha-error').length <= 0){
                    $('.g-recaptcha').after('<p class="captcha-error" style="color:red">Invalid captcha</p>');
                }
                return false;
            }else{
                if($('.captcha-error').length > 0) {
                    $('.captcha-error').remove();
                }
            }
            return true;
        });
    });
</script>
  

@endsection
