<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ @$broker->first_name }} (Landing)</title>
      <!-- Font Awesome -->
      <script src="https://kit.fontawesome.com/d8c4db5f0e.js"></script>

    <!-- Bootstrap -->
    <link href="{{ url('frontend/css/bootstrap-4.2.1.css')}}" rel="stylesheet">
    <link href="{{ url('frontend/css/custom.css')}}" rel="stylesheet">

  </head>
  <body>
    <div class="container">
        <div class="row pt-2">
            <div class="col-xl-5 col-md-1">

              @if(@$broker->avatar)
                <img src="{{ $broker->avatar }}" width="250px" height="250px" class="img-fluid" alt=""/> </div>
              @else
              <img src="{{ url('frontend/images/BrokerLogo.png')}}" class="img-fluid" alt=""/> </div>
              @endif
              
            <div class="col-xl-3 offset-xl-1 col-md-6 align-self-center"><strong>{{ @$broker->first_name }}&nbsp;{{ @$broker->last_name }}</strong><br>{{ @$broker->agency_address_line_1 }}<br>{{ @$broker->agency_address_line_2 }},{{ @$broker->city }},{{ @$broker->zipcode }}</div>
            <div class="col-xl-3 col-md-6 align-self-center">{{ @$broker->phone }} / {{ @$broker->phone_tollfree }}<br>{{ @$broker->email }}<br>{{ @$broker->website }}.ichra.online</div>
        </div>
       <hr>
       
       @yield('content')
       <hr>
      <div class="row justify-content-center">
         
          <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center">
		  <img src="{{ url('frontend/images/logo_bcbsmn.png')}}" alt="bcbsmn" class="img-fluid" /></div>
		  <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center">
		  <img src="{{ url('frontend/images/logo_healthpartners.png')}}" alt="healthpartners" class="img-fluid" /></div>
		  <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center">
		  <img src="{{ url('frontend/images/logo_medica.png')}}" alt="medica" class="img-fluid" /></div>
		  <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center">
		  <img src="{{ url('frontend/images/logo_preferredone.png')}}" alt="preferredone" class="img-fluid" /></div>
         
         <!-- @foreach(@$broker->brokerc as $key => $car)
          <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center"><img src="{{ @$car->carrier->carrier_logo}}" alt="" class="img-fluid" /></div>
          @endforeach -->
          <!-- <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center"><img src="{{ url('frontend/images/logo_healthpartners.png')}}"  alt="" class="img-fluid" /></div>
          <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center"><img src="{{ url('frontend/images/logo_medica.png')}}" alt="" class="img-fluid" /></div>
        <div class="col-lg-2 col-sm-6 align-self-center py-2 text-center"><img src="{{ url('frontend/images/logo_preferredone.png')}}" alt="" class="img-fluid" /></div> -->
       </div>
       <br>
       <hr>
       <div class="row justify-content-center">
          <div class="text-center col-sm-7">
            <ul class="nav nav-fill">
                <li class="nav-item"><a class="nav-link" href="#">HSA FAQ</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Privacy Policy</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Quote Request Agreement</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Contact Us</a></li>
            </ul>
           </div>
          <!-- <div class="text-center col-sm-3">
              <a href="#" class="btn btn-primary">Sign Up for a Flyte HSA <i class="fas fa-feather-alt"></i></a>
          </div> -->
       </div>
       <hr>
       <div class="row">
          <div class="text-center col-6 offset-3">
             <h4><img src="{{ url('frontend/images/flyteHCM_Portal.png')}}" width="200px" height="41px" alt=""/></h4>
             <p>Copyright &copy; 2019 &middot; All Rights Reserved &middot; <a href="https://www.flytehcm.com" >Flyte HCM LLC</a></p>
          </div>
       </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
    <script src="js/jquery-3.3.1.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    <script src="{{ url('frontend/js/popper.min.js')}}"></script>
    <script src="{{ url('frontend/js/bootstrap-4.2.1.js')}}"></script>
    <script type="text/javascript" src="{{asset('asset/js/jquery.min.js')}}" ></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    @yield('scripts')
  </body>
</html>
