@extends('layouts.new')


@section('content')
@if (session()->has('flash_success'))
        <div class="alert alert-success">
            {!! session()->get('flash_success')!!}        
        </div>
@endif


            <section>


                <div class="container">
                    @if (session()->has('flash_error'))
                        <div class="alert alert-danger">
                            {!! session()->get('flash_error')!!}        
                        </div>
                @endif
                    <div class="row">
                  
                        <div class="col-md-7">
                       
                            <form class="loginSectionform" action="{{url('broker/authenticate')}}" method="POST">
                                {{csrf_field()}}
                                @if(count($broker_subscription_plan) > 0)
                                <div class="container">
                                  <h2>Plans</h2>
                                  <div class="card" onclick="updatePlan('1')" style="width:400px">
                                    <input type="radio" id="plan1" name="plan_type" value="1">
                                    
                                    <div class="card-body">
                                      <h4 class="card-title">Monthly Plan</h4>
                                      <p class="card-text"><strong>${{$broker_subscription_plan->amount}} / month</strong></p>
                                      <a href="javascript:void(0)" class="btn btn-primary">Select Plan</a>
                                    </div>
                                  </div>
                                  <br>
                                  @php 
                                        $plan_amount = $broker_subscription_plan->amount;
                                        $yearly_amount = $broker_subscription_plan->amount * 12;
                                        $discount = round($yearly_amount * (10/100));
                                        $yearly_discounted_amount = $yearly_amount - $discount;
                                  @endphp
                                  <div class="card" onclick="updatePlan('2')" style="width:400px">
                                    <input type="radio" id="plan2" name="plan_type" value="2">
                                    <div class="card-body">
                                      <h4 class="card-title">Yearly Plan</h4>
                                      <p class="card-text"><strike>${{$yearly_amount}} / year</strike><br>${{$yearly_discounted_amount}} / year</p>
                                      <a href="javascript:void(0)" class="btn btn-primary">Select Plan</a>
                                    </div>
                                    
                                  </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <!-- <label>
                                        <p class="">Plan</p>
                                        <input type="text" value="{{$broker_subscription_plan->name}}" name="plan" class="input form-control" readonly="readonly">
                                        <input type="hidden" value="{{$broker_subscription_plan->id}}" name="broker_subscription_plan" class="input" readonly="readonly">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label> -->
                                </div>
                                <div class="form-group">
                                    <!-- <label>
                                        <p class="">Amount</p>
                                        <input type="text" value="{{'$'.$broker_subscription_plan->amount}}" name="password" class="input form-control" readonly="readonly">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label> -->
                                </div>

                                @if(count($broker_subscription_plan) <= 0)
	                            	<p style="color: red">**No packages available.please contact admin.</p>
	                            @endif
                                <input type="hidden" value="{{$broker}}" name="broker_id">
                                <h4>Enter Card Details</h4>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Card name</p>
                                        <input type="text"  name="card_name" value="{{old('card_name')}}" class="input form-control">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                        @if ($errors->has('agency_name'))
						                    <span class="help-block">
						                        <strong>{{ $errors->first('agency_name') }}</strong>
						                    </span>
						                @endif
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Card number</p>
                                        <input type="text"  name="card_number" value="{{old('card_number')}}" class="input form-control">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                        @if ($errors->has('card_number'))
						                    <span class="help-block">
						                        <strong>{{ $errors->first('card_number') }}</strong>
						                    </span>
						                @endif
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Expiry month</p>
                                        <select name="month">
                                            <option value="">Month</option>
                                            @for($i = 1;$i <= 12;$i++)
                                                @if($i <= 9)
                                                <option value="{{'0'.$i}}">{{'0'.$i}}</option>
                                                @else
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endif
                                            @endfor
                                        </select>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                        @if ($errors->has('month'))
						                    <span class="help-block">
						                        <strong>{{ $errors->first('month') }}</strong>
						                    </span>
						                @endif
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">Expiry year</p>
                                        <select name="year">
                                            <option value="">Year</option>
                                            @for($i = date('Y');$i <= date('Y') + 10;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                        @if ($errors->has('year'))
						                    <span class="help-block">
						                        <strong>{{ $errors->first('year') }}</strong>
						                    </span>
						                @endif
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <p class="label-txt">CVV</p>
                                        <input type="text"  name="cvv" value="{{old('cvv')}}" class="input form-control">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                        @if ($errors->has('cvv'))
						                    <span class="help-block">
						                        <strong>{{ $errors->first('cvv') }}</strong>
						                    </span>
						                @endif
                                    </label>
                                </div>
                                
                                <div class="form-group text-center">
                                    <button type="submit" class="submitBtn">Subscribe</button>
                                </div>
                            </form>
                            
                           
                        </div>
                    </div>
                </div>
            </section>

<script type="text/javascript">
    function updatePlan(type){
        if(type){
            if(type == 1){
                $('#plan1').prop('checked',true);
                $('#plan2').prop('checked',false);
            }else if(type == 2){
                $('#plan2').prop('checked',true);
                $('#plan1').prop('checked',false);
            }else{
                $('#plan1').prop('checked',false);
                $('#plan2').prop('checked',false);
            }
        }
    }
</script>
@endsection