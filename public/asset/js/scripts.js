// TopChart 3 Start
$('.customer-logos').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        // arrows: true,
        // prevArrow: '<div class="slick-prev float-left"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
        // nextArrow: '<div class="slick-next float-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, 
        {
            breakpoint: 798,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
// TopChart 3 End
// TopChart 2 Start
$('.topChart2').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        // arrows: true,
        // prevArrow: '<div class="slick-prev float-left"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
        // nextArrow: '<div class="slick-next float-right"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, 
        {
            breakpoint: 798,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
// TopChart 2 End