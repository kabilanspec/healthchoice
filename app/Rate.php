<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'SUBPLAN_ID',
        'RATE_AREA_ID',
        'RATE_EFFECTIVE_D',
        'RATE_EXPIRATION_D',
        'RATE_AGE_MIN_I',
        'RATE_AGE_MAX_I', 
        'RATE_SINGLE_C',
        'RATE_SINGLE_TOBACCO_C',
        'RATE_FAMILY_C',
        'RATE_FAMILY_TOBACCO_C',
        'RATE_GENDER_T' ,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];


}
