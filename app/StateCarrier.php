<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateCarrier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'carrier_id', 'state_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function state()
    {
        return $this->belongsTo('App\State');
    }
}
