<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\BrokerResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Broker extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
    	'first_name',
    	'last_name',
    	'email',
    	'password',
    	'agency_name',
    	'agency_address_line_1',
    	'agency_address_line_2',
    	'city',
    	'state_id',
    	'phone',
    	'phone_tollfree',
    	'website',
    	'payment_status',
    	'broker_subscription_plan_id',
    	'status',
        'avatar',
        'zipcode',
        'subscription_date',
        'expiry_date',
        'plan_type',
        'subscription_amount',
        'unique_id',
        'agency_email'
    ];

    protected $hidden = [
    	'created_at',
    	'updated_at'
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new BrokerResetPassword($token));
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function brokerc()
    {
        return $this->hasMany('App\BrokerCarrier');
    }
}
