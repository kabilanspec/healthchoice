<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPlan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'PLAN_ID',
        'HIOS_PLAN_ID',
        'NETWORK_CODE_T',
        'SERVICE_AREA_CODE_T',
        'SUBPLAN_NAME_T',
        'COINSURANCE_T', 
        'HSA_B',
        'METAL_LEVEL_T',
        'NETWORK_NAME_T',
        'EXCHANGE_STATUS_T',
        'CATASTROPHIC_B' ,
        'OFFICE_VISIT_COPAY_B',
        'RX_COPAY_B', 
        'OV_PRIMARY_CARE_M',
        'OV_SPECIALIST_M',
        'OV_OTHER_M',
        'OV_PREVENTATIVE_M',
        'TEST_DIAGNOSTIC_M',
        'TEST_IMAGING_M',
        'RX_GENERIC_M',
        'RX_FORMULARY_M',
        'RX_NONFORMULARY_M',
        'RX_SPECIALTY_M',
        'OUTPATIENT_FACILITY_M',
        'OUTPATIENT_PHYS_FEES_M',
        'ER_ER_M',
        'ER_TRANSPORT_M',
        'ER_URGENT_M',
        'HOSPITAL_FACILITY_M',
        'HOSPITAL_PHYSSUR_M',
        'MH_OUTPATIENT_M',
        'MH_INPATIENT_M',
        'MH_SUBSTANCE_OUT_M',
        'MH_SUBSTANCE_IN_M',
        'BABY_PREPOST_M',
        'BABY_DELIVER_IN_M',
        'RECOVER_HOMEHEALTH_M',
        'RECOVER_REHAB_M',
        'RECOVER_HABIL_M',
        'RECOVER_SKILLNURSE_M',
        'RECOVER_MEDEQUIP_M',
        'RECOVER_HOSPICE_M',
        'PED_EYEEXAM_M',
        'PED_GLASSES_M',
        'PED_DENTAL_M',
        'SBC_PATH_T',
        'ACTIVE_B'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function subplanded()
    {
        return $this->hasMany('App\SubPlanDed','SUBPLAN_ID');
    }

    public function rate()
    {
        return $this->hasMany('App\Rate','SUBPLAN_ID');
    }


}
