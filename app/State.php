<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'postal_code', 'full_name', 'state_use_gender','state_code_t','	create_by','create_at','update_by','update_at','delete_by','delete_at','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
