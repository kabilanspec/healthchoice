<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
     protected $fillable = [
        'application_id', 'broker_id','zipcode','county_id','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function details()
    {
        return $this->hasMany('App\ApplicationDetails','application_id');
    }

    public function broker()
    {
        return $this->belongsTo('App\Broker');
    }

    public function county()
    {
        return $this->belongsTo('App\County');
    }
}
