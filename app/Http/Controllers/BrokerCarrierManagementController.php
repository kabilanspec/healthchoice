<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\BrokerCarrier;
use App\StateCarrier;
use App\State;
use App\Carrier;
use App\Broker;
use App\BrokerCarrierManagement;
use App\BrokerCarrierUrl;

class BrokerCarrierManagementController extends Controller
{
	public function __construct(){
       $this->middleware('broker');
    }
    public function dashboard(){
        $broker = Broker::where('id',Auth::guard('broker')->user()->id)->first();
        $carriers = Carrier::where('status','active')->get();
        $states = State::where('status','active')->get();
        $datas = array();
        $i = 0;
        foreach($states as $state){
            $datas[$i]['state'] = $state->full_name;
            $datas[$i]['state_id'] = $state->id;
            foreach($carriers as $carrier){
                $broker_carrier_urls = BrokerCarrierUrl::where('broker_id',Auth::guard('broker')->user()->id)->where('carrier_id',$carrier->id)->where('state_id',$state->id)->get();
                if(count($broker_carrier_urls) > 0){
                    $datas[$i]['carrier'][] = $carrier->carrier;
                    $datas[$i]['carrier_id'][] = $carrier->id;
                    $k = 0;
                    foreach($broker_carrier_urls as $broker_carrier_url){
                        $datas[$i]['s'][$carrier->carrier][$k]['carrier_id'] = $carrier->id;
                        $datas[$i]['s'][$carrier->carrier][$k]['broker_url_id'] = $broker_carrier_url->id;
                        $datas[$i]['s'][$carrier->carrier][$k]['enrollment'] = $broker_carrier_url->enrollment;
                        $datas[$i]['s'][$carrier->carrier][$k]['url'] = $broker_carrier_url->carrier_url;
                        $k++;
                    } 
                }
            }
            $i++;
        }
        //dd($datas);
    	return view('broker.dashboard',compact('datas','broker','carriers'));
    }
    public function store(Request $request){
        //dd($request->all());
        $this->validate($request,[
            'enrollment' => 'required',
            'carrier_url' => 'required|url',
            'carrier_id' => 'required',
            'state_id' => 'required'
        ]);
        $request->merge(['broker_id' => Auth::guard('broker')->user()->id]);
        $broker_carrier_url = BrokerCarrierUrl::create($request->all());
        return redirect('broker/dashboard/'.$request->state_id.'_'.$request->carrier_id)->with('flash_success','Carrier updated successfully');
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'enrollment' => 'required',
            'carrier_url' => 'required|url'
        ]);
        $broker_carrier = BrokerCarrierUrl::where('id',$id)->first();
        if($broker_carrier){
            $broker_carrier->enrollment = $request->enrollment;
            $broker_carrier->carrier_url = $request->carrier_url;
            $broker_carrier->save();
        }
        return back()->with('flash_success','Carrier updated successfully');
    }
    public function destroy($id){
        $broker_carrier = BrokerCarrierUrl::where('id',$id)->delete();
        return back()->with('flash_success','Carrier deleted successfully');
    }
}
