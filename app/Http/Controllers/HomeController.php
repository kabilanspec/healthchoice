<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test()
    {
        dd('test');
    }

     public function autocomplete(Request $request)
    {


      $term=$request->term;
     
      $data=State::where('full_name','LIKE','%'.$term.'%')
                     
                           ->get();
      $results=array();

      foreach ($data as $key => $v) {

          $results[]=['id'=>$v->id,'value'=>$v->full_name];

      }
      
      return response()->json($results);
            
    }
}
