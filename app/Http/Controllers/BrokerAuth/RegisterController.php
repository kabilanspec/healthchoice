<?php

namespace App\Http\Controllers\BrokerAuth;

use App\Helpers\Helper;

use App\Broker;
use App\State;
use App\Carrier;
use App\BrokerCarrier;
use App\BrokerSubscriptionPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthorizeNetController as AuthorizeNet;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/broker/subscription';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('broker.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function register(Request $request){
        //dd($request->all());
        $this->validate($request,[
            'agency_email' => 'nullable|sometimes|email|max:50|unique:brokers',
            'agency_name' => 'required|max:50',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email|max:50|unique:brokers',
            'password' => 'required|min:6|confirmed',
            'agency_address_line_1' => 'required',
            'city' => 'required|max:50',
            'state_id' => 'required',
            'phone' => 'required|integer|digits:10',
            'phone_tollfree' => 'nullable|sometimes|integer|digits:10',
            'website' => 'required|unique:brokers',
            'is_agreed' => 'required',
            'is_refund' => 'required',
            'zipcode' => 'required|integer',
            'card_name' => 'required|max:50',
            'card_number' => 'required|integer|digits_between:12,16',
            'year' => 'required',
            'month' => 'required',
            'cvv' => 'required|integer|digits_between:3,4',
            'plan_type' => 'required|integer',
            'g-recaptcha-response' => 'required'
            //'carrier_id' => 'present|array'
        ],[
            'agency_name.required' => 'Please fill out this field',
            'first_name.required' => 'Please fill out this field',
            'last_name.required' => 'Please fill out this field',
            'email.required' => 'Please fill out this field',
            'password.required' => 'Password is missing',
            'agency_address_line_1.required' => 'Please fill out this field',
            'city.required' => 'Please fill out this field',
            'phone.required' => 'Please fill out this field',
            'website.required' => 'Please fill out this field',
            'is_agreed.required' => 'Please fill out this field',
            'is_refund.required' => 'Please fill out this field',
            'state_id.required' => 'Please fill out this field',
            'password_confirmation.required' => 'Confirm password is missing',
            //'carrier_id.present' => 'Please select a carrier',
            //'carrier_id.array' => 'Carrier is invalid',  
            'phone.unique' => 'Phone number already taken',
            'website.unique' => 'Broker URL already taken',
            'email.unique' => 'Email already taken',
            'email.email' => 'Email is not valid',
            'agency_email.unique' => 'Email already taken',
            'agency_email.email' => 'Email is not valid',
            'password.confirmed' => 'Password mismatch',
            'phone.integer' => 'Phone number must be valid and must not start with 0',
            'phone.digits' => 'Phone number must be 10 digit',
            'phone_tollfree.integer' => 'Phone number must be valid and must not start with 0',
            'phone_tollfree.digits' => 'Phone number must be 10 digit',
            'cvv.digits_between' => 'Enter a valid CVV',
            'card_number.digits_between' => 'Enter a valid credit card number',
            'plan_type.required' => 'Please select a plan',
            'plan_type.integer' => 'Selected plan invalid',
            'card_name.required' => 'Please fill out this field',
            'card_number.required' => 'Please fill out this field',
            'year.required' => 'Please select a year',
            'month.required' => 'Please select a month',
            'cvv.required' => 'Please fill out this field',
            'cvv.integer' => 'Please enter a valid CVV',
            'card_number.integer' => 'Please enter a valid card number',
            'g-recaptcha-response.required' => 'Invalid Captcha'
        ]);

        $secret = '6LcJXrMUAAAAAN5SFnAmpsxs2elYwZ3xe0uws_r2';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success)
        {
            $broker =  Broker::create([
                'agency_name' => $request->agency_name,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => $request->password,
                'agency_address_line_1' => $request->agency_address_line_1,
                'agency_address_line_2' => ($request->agency_address_line_2) ? $request->agency_address_line_2 : NULL,
                'city' => $request->city,
                'phone' => $request->phone,
                'phone_tollfree' => ($request->phone_tollfree) ? $request->phone_tollfree : NULL,
                'website' => $request->website,
                'password' => bcrypt($request->password),
                'payment_status' => 'UNPAID',
                'broker_subscription_plan_id' => NULL,
                'status' => 'UNAPPROVED',
                'state_id' => $request->state_id,
                'zipcode' => $request->zipcode,
                'unique_id' => 'ICH'.time().mt_rand() 
            ]);
            if($broker){
                $broker_subscription_plan = BrokerSubscriptionPlan::first();
                    if($request->plan_type == 1){
                        $amount = $broker_subscription_plan->amount;
                    }else if($request->plan_type == 2){
                        $plan_amount = $broker_subscription_plan->amount;
                        $yearly_amount = $broker_subscription_plan->amount * 12;
                        //$discount = round($yearly_amount * (10/100));
                        //$yearly_discounted_amount = $yearly_amount - $discount;
                        //$amount = $broker_subscription_plan->yearly_discounted_amount;
                        $amount = $yearly_amount;
                    }else{
                        return redirect('/broker/register')->with('flash_error','Invalid plan');
                    }
                    if($broker_subscription_plan){
                        $request->merge([
                            'amount' => $amount
                    ]);
                    $data = array();
                    $data['amount'] = $amount;
                    $data['card_number'] = $request->card_number;
                    $data['month'] = $request->month;
                    $data['year'] = $request->year;
                    $data['cvv'] = $request->cvv;
                    $process_payment = (new AuthorizeNet)->capture($data);
                    if(isset($process_payment['transactionResponse']['responseCode']) && $process_payment['transactionResponse']['responseCode'] == 1){
                        $update_broker = Broker::where('id',$broker->id)->first();
                        $update_broker->broker_subscription_plan_id = $request->plan_type;
                        $update_broker->payment_status = 'PAID';
                        $update_broker->status = 'APPROVED';
                        $update_broker->subscription_date = date('Y-m-d');
                        if($request->plan_type == 1){
                            $update_broker->expiry_date = date('Y-m-d',strtotime('+30 days',strtotime(date('Y-m-d'))));
                        }else if($request->plan_type == 2){
                            $update_broker->expiry_date = date('Y-m-d',strtotime('+365 days',strtotime(date('Y-m-d'))));
                        }
                        $update_broker->plan_type = $request->plan_type;
                        $update_broker->subscription_amount = $request->amount;
                        if($update_broker->save()){
                            Helper::send_mail('register',$update_broker);
                            return redirect('/broker/login')->with('flash_success','User details are saved successfully, We will approve as soon as possible');
                        }else{
                            return redirect('/broker/register/')->with('flash_error','Something went wrong!');
                        }
                    }else{
                        if(isset($process_payment['messages']['message'][0]['text'])){
                            return redirect('/broker/register')->with('flash_error',$process_payment['messages']['message'][0]['text'].'Please check your card details');
                        }else if(isset($process_payment['error'])){
                            return redirect('/broker/register')->with('flash_error',$process_payment['error']);
                        }else{
                            return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error','Unauthenticated Transaction');
                        }
                    }
                }
                else{
                    return redirect('/broker/register')->with('flash_error','Invalid subscription!');
                }
            }else{
                return redirect('/broker/register')->with('flash_error','Access Denied');
            }
        }
        else
        {
            return back()->with('flash_error','Invalid Captcha');
        }
    }
    public function my_simple_crypt( $string, $action = 'e' ) {
        $secret_key = 'erwwerfe54235wfwer2w34523';
        $secret_iv = 'sfdgsdgsd345345345sfdsfsdadf';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
        return $output;
    }
    public function subscription(Request $request){
        $broker = $request->id;
        $broker_subscription_plan = BrokerSubscriptionPlan::where('is_deleted',0)->first();
        return view('broker.subscription',compact('broker_subscription_plan','broker'));
    }
    

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $broker_subscription_plan = BrokerSubscriptionPlan::where('is_deleted',0)->first();
        $state = State::where('status','active')->get();
        $carriers = Carrier::where('status','active')->get(); 
        return view('broker.auth.register',compact('state','carriers','broker_subscription_plan'));
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('broker');
    }
}
