<?php

namespace App\Http\Controllers\BrokerAuth;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Hesto\MultiAuth\Traits\LogsoutGuard;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    protected function guard()
    {
        return Auth::guard('broker');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/broker/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('broker.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('broker.auth.login');
    }

    public function login(Request $request){
        if(Auth::guard('broker')->attempt(['email' => $request->email, 'password' => $request->password])){
            if(Auth::guard('broker')->user()->payment_status == 'PAID' && Auth::guard('broker')->user()->status == 'APPROVED'){
                return redirect('broker/dashboard');
            }else{
                Auth::guard('broker')->logout();
                return redirect('broker/login')->with('flash_error','Account Inactive');
            }
        }else{
            return redirect('broker/login')->with('flash_error','Invalid credentials');
        }
        
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */

    public function logout(){
        Auth::guard('broker')->logout();
        return redirect('broker/login');
    }
}
