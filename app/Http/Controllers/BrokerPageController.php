<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


use Setting;
use Exception;
use \Carbon\Carbon;
use App\User;
use App\Plan;
use App\Rate;
use App\State;
use App\Admin;
use App\Broker;
use App\Zipcode;
use App\ZipCounty;
use App\County;
use App\BrokerCarrier;
use App\Application;
use App\ApplicationDetails;
use App\Userinfo;
use App\Applicant_info;

use DB;
use Session;
use DateTime;

class BrokerPageController extends Controller
{
    

    public function dashboard()
    {
        return view('broker.page.dashboard');
    }

    public function plan($website)
    {
        $broker = Broker::wherewebsite($website)->first();
        return view('broker.page.plan',compact('broker'));
    }

    public function effectiveplan(Request $request)
    {
        $this->validate($request,[

            'broker_id' => 'required',
            'plan' => 'required',
          ]);
       // dd($request->all());

        $plan = Zipcode::create(['broker_id' => $request->broker_id,
                          'plan_date' => $request->plan]);
        $zipcode_id = $plan->id;

        /*echo "done";*/
        $broker = Broker::whereid($request->broker_id)->first();
        return view('broker.page.quote',compact('broker','zipcode_id'));



    }


    public function quote($website)
    {

         $broker = Broker::wherewebsite($website)->first();
    	return view('broker.page.quote',compact('broker'));
    }

    public function store(Request $request)
    {
        //dd($request->all());

          $this->validate($request,[

            'broker_id' => 'required',
            'zip_id' => 'required',
            'zipcode' => 'required',
          ]);

          $county_id = ZipCounty::where('ZIP_CODE_T',$request->zipcode)->first();
          
          
         if($county_id){

          $broker = Broker::whereid($request->broker_id)->first();
          //$zipcode = $request->all();
          $zipcode = Zipcode::whereid($request->zip_id)->wherebroker_id($request->broker_id)->first();
          $zipcode->zipcode = $request->zipcode;
          $zipcode->save();
          $zipcode_id = $zipcode->id;

          $county= County::where('id',$county_id->COUNTY_ID)->get();
          if($zipcode){
            /*echo ("<script LANGUAGE='JavaScript'>
                    window.alert('Zipcode Updated Succesfully');
                    </script>");*/
            return view('broker.page.county',compact('county','zipcode_id','broker'));
            
                }else{
                    return redirect()->back()->with('flash_error','Oops!Something went wrong.');
                }

          }else{
            echo ("<script LANGUAGE='JavaScript'>
                    window.alert('No County For ".$request->zipcode.".');
                    
                    </script>");
            return redirect()->back()->with('flash_error','Oops!Something went wrong.');
          }
          


    }

    public function county(Request $request)
    {

        $zip = Zipcode::whereid($request->zipcode_id)->first();
        $zip->county_id = $request->county_id;
        $zip->save();

        $broker = Broker::whereid($zip->broker_id)->first();
        $county= County::whereid($zip->county_id)->first();

         if($zip){
            /*echo ("<script LANGUAGE='JavaScript'>
                    window.alert('Country Updated Succesfully');
                    </script>");*/
            return view('broker.page.todo',compact('county','zip','broker'));
            
        }else{
            return redirect()->back()->with('flash_error','Oops!Something went wrong.');
        }
        
        
    }

    public function todo()
    {
        
        return view('broker.page.todo');
    }

    public function applicant (Request $request)
    {
        //dd($request->all());

        $broker = Broker::whereid($request->broker_id)->first();
        $bc = BrokerCarrier::where('broker_id',$request->broker_id)->pluck('carrier_id');

        $spouse = [];
        
        $application = new Application;
        $application->application_id = str_random(10);
        $application->broker_id = $request->broker_id;
        $application->zipcode = $request->zipcode;
        $application->county_id = $request->county_id;
        $application->status = 0;
        $application->save();

        //dd($application->id);

        if($request->has('applicant_dob')){
                $userDob = $request->applicant_dob;
                $dob = new DateTime($userDob);
                $now = new DateTime();
                $difference = $now->diff($dob);
                $age = $difference->y;
            $details = new ApplicationDetails;
            $details->application_id =  $application->id;
            $details->dob = $request->applicant_dob;
            $details->gender = $request->applicant_gender;
            $details->is_smoker = $request->applicant_tobacco;
            $details->who = 'applicant';
            $details->status = 0;
            $details->age = @$age;
            $details->save();

        }
        if ($request->spouse_dob != null) {
                $userDob = $request->spouse_dob;
                $dob = new DateTime($userDob);
                $now = new DateTime();
                $difference = $now->diff($dob);
                $age = $difference->y;
            $details = new ApplicationDetails;
            $details->application_id =  $application->id;
            $details->dob = $request->spouse_dob;
            $details->gender = $request->spouse_gender;
            $details->is_smoker = $request->spouse_tobacco;
            $details->who = 'spouse';
            $details->status = 0;
            $details->age = @$age;
            $details->save();
        }

        if(@$request['child_dob']) {

        foreach ($request['child_dob'] as $key => $value) {
            if($value != null){
                $userDob = $value;
                $dob = new DateTime($userDob);
                $now = new DateTime();
                $difference = $now->diff($dob);
                $age = $difference->y;
            $details = new ApplicationDetails;
            $details->application_id =  $application->id;
            $details->dob = $value;
            $details->gender = @$request->child_gender[$key];
            $details->is_smoker = @$request->child_tobacco[$key];
            $details->who = 'child';
            $details->status = 0;
            $details->age = @$age;
            $details->save();
         }
        }

        }

        $data = Application::whereid($application->id)
                           ->with('details','broker','county')
                           ->first();
        $details = $data['details'] ;
        /*dd($details);*/
        $applicant = $details->where('who','applicant')->first();
        $self['gender'] = $applicant->gender; 
        //$age = $this->age($applicant->dob);
            $userDob = $applicant->dob;
            $dob = new DateTime($userDob);
            $now = new DateTime();
            $difference = $now->diff($dob);
            $age = $difference->y;
        $self['age'] = $age;
        $self['smoker'] = $applicant->is_smoker;
        
       if ($request->spouse_dob != null) {
        $spouse = $details->where('who','spouse')->first();
        $spouse['gender'] = $spouse->gender; 
        //$age = $this->age($spouse->dob);
            $userDob = $spouse->dob;
            $dob = new DateTime($userDob);
            $now = new DateTime();
            $difference = $now->diff($dob);
            $age = $difference->y;
        $spouse['age'] = $age;
        $spouse['smoker'] = $spouse->is_smoker; 
        }

         if($self['smoker'] == 'yes' || @$spouse['smoker'] == 'yes' ){
           /* dd('yess');*/
           if($self['smoker'] == 'yes' && @$spouse['smoker'] == 'yes'){
                $tobacco = 3;
            }elseif ($self['smoker'] == 'yes' && (@$spouse['smoker'] == 'no' || @$spouse['smoker'] == null )) {

               $tobacco = 2;
            }else{
                $tobacco = 1;
            }
         }else{
            if($request->has('applicant_dob') && $request->has('spouse_dob') ){
               $tobacco = 4;
            }else{
               
               $tobacco = 1; 
            }
            
        }
        //dd($tobacco);
       
        $child = ApplicationDetails::whereapplication_id($application->id)
                                   ->wherewho('child')
                                   ->get();
        if (!empty($child)) {
                  foreach ($child as $key => $value) {
          if($self['smoker'] == 'yes' || @$spouse['smoker'] == 'yes' || $value->is_smoker ==  'yes' ){
            if($self['smoker'] == 'yes' && @$spouse['smoker'] == 'yes' && $value->is_smoker ==  'yes' ){
                $tobacco = 3;
            }if($self['smoker'] == 'yes' && @$spouse['smoker'] == 'yes'&& $value->is_smoker ==  'no'){
                $tobacco = 3;
            }if ($self['smoker'] == 'yes' &&(@$spouse['smoker'] == 'no' || @$spouse['smoker'] == null ) && $value->is_smoker ==  'no') {
                $tobacco = 2;
            }
        }else{
            if($request->has('applicant_dob') && ($request->has('spouse_dob') || !empty($child))){
               $tobacco =4;
            }else{
                $tobacco = 1;
            }
        }
       }                   
         }                           
        
           //dd($tobacco);                   
        $max_age = ApplicationDetails::whereapplication_id($application->id)->max('age');


        
        $bronzeplan=Plan::where('PLAN_NAME_T','LIKE','%'."Bronze".'%')->whereIn('CARRIER_ID',$bc)->with('carrier','subplan','subplan.subplanded','subplan.rate')->get();
        $silverplan=Plan::where('PLAN_NAME_T','LIKE','%'."Silver".'%')->whereIn('CARRIER_ID',$bc)->with('carrier','subplan','subplan.subplanded','subplan.rate')->get();
        /*DB::enableQueryLog();*/
        $goldplan=Plan::where('PLAN_NAME_T','LIKE','%'."Gold".'%')->whereIn('CARRIER_ID',$bc)->with('carrier','subplan','subplan.subplanded','subplan.rate')
            ->get();
           

            foreach ($goldplan as $key => $value) {
                
                foreach ($value->subplan as $key1 => $sp) {
                      $rate = Rate::where('SUBPLAN_ID',$sp->id)->where('RATE_AGE_MAX_I',$max_age)->first();
                      
                      if($tobacco == 1){
                        $premium = @$rate->RATE_SINGLE_C ?? 0;
                        $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 2) {

                          $premium = @$rate->RATE_SINGLE_TOBACCO_C ?? 0;
                          //echo $premium.','; 
                          $value->subplan[$key1]->premium = $premium;

                      }elseif ($tobacco == 3) {
                          $premium = @$rate->RATE_FAMILY_TOBACCO_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 4){
                          $premium = @$rate->RATE_FAMILY_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }else{
                          $premium = @$rate->RATE_SINGLE_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }
                      
                }

                $goldplan[$key]->premium = round($value->subplan[$key1]->premium);
               
            }
            foreach ($silverplan as $key => $value) {
                
                foreach ($value->subplan as $key1 => $sp) {
                      $rate = Rate::where('SUBPLAN_ID',$sp->id)->where('RATE_AGE_MAX_I',$max_age)->first();
                      if($tobacco == 1){
                        $premium = @$rate->RATE_SINGLE_C ?? 0;
                        $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 2) {
                          $premium = @$rate->RATE_SINGLE_TOBACCO_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;

                      }elseif ($tobacco == 3) {
                          $premium = @$rate->RATE_FAMILY_TOBACCO_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 4){
                          $premium = @$rate->RATE_FAMILY_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }else{
                          $premium = @$rate->RATE_SINGLE_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }
                      
                }

                $silverplan[$key]->premium = round($value->subplan[$key1]->premium);
            }
            foreach ($bronzeplan as $key => $value) {
                
                foreach ($value->subplan as $key1 => $sp) {
                      $rate = Rate::where('SUBPLAN_ID',$sp->id)->where('RATE_AGE_MAX_I',$max_age)->first();
                      if($tobacco == 1){
                        $premium = @$rate->RATE_SINGLE_C ?? 0;
                        $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 2) {
                          $premium = @$rate->RATE_SINGLE_TOBACCO_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;

                      }elseif ($tobacco == 3) {
                          $premium = @$rate->RATE_FAMILY_TOBACCO_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 4){
                          $premium = @$rate->RATE_FAMILY_C ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }else{
                          $premium = @$rate->RATE_SINGLE_C  ?? 0;
                          $value->subplan[$key1]->premium = $premium;
                      }
                      
                }

                $bronzeplan[$key]->premium = round($value->subplan[$key1]->premium);
            }
           
        return view('broker.page.results', compact('data','self','spouse','child','broker','goldplan','silverplan','bronzeplan'));




    }

    public function compare (Request $request)
    {
        //dd($request->all());

        $county = $request->county;
        $zipcode = $request->zipcode;
        $broker = Broker::whereid($request->broker_id)->first();

        $data = Application::whereid($request->application_id)
                           ->with('details','broker','county')
                           ->first();
        $details = $data['details'] ;
        /*dd($details);*/
        $applicant = $details->where('who','applicant')->first();
        $self['gender'] = $applicant->gender; 
        //$age = $this->age($applicant->dob);
            $userDob = $applicant->dob;
            $dob = new DateTime($userDob);
            $now = new DateTime();
            $difference = $now->diff($dob);
            $age = $difference->y;
        $self['age'] = $age;
        $self['smoker'] = $applicant->is_smoker;

        if ($request->spouse_dob != null) {
        $spouse = $details->where('who','spouse')->first();
        $spouse['gender'] = $spouse->gender; 
        //$age = $this->age($spouse->dob);
            $userDob = $spouse->dob;
            $dob = new DateTime($userDob);
            $now = new DateTime();
            $difference = $now->diff($dob);
            $age = $difference->y;
        $spouse['age'] = $age;
        $spouse['smoker'] = $spouse->is_smoker; 
        }
         if($self['smoker'] == 'yes' || @$spouse['smoker'] == 'yes' ){
            
           if($self['smoker'] == 'yes' && @$spouse['smoker'] == 'yes'){
                $tobacco = 3;
            }elseif ($self['smoker'] == 'yes' && (@$spouse['smoker'] == 'no' || @$spouse['smoker'] == null )) {
               $tobacco = 2;
            }
         }else{
            if($request->has('applicant_dob') && $request->has('spouse_dob') ){
               $tobacco = 4;
            }else{
               $tobacco = 1; 
            }
            
        }    
        $child = ApplicationDetails::whereapplication_id($request->application_id)
                                   ->wherewho('child')
                                   ->get();
        if (!empty($child)) {
                  foreach ($child as $key => $value) {
          if($self['smoker'] == 'yes' || @$spouse['smoker'] == 'yes' || $value->is_smoker ==  'yes' ){
            if($self['smoker'] == 'yes' && @$spouse['smoker'] == 'yes' && $value->is_smoker ==  'yes' ){
                $tobacco = 3;
            }if($self['smoker'] == 'yes' && @$spouse['smoker'] == 'yes'&& $value->is_smoker ==  'no'){
                $tobacco = 3;
            }if ($self['smoker'] == 'yes' &&(@$spouse['smoker'] == 'no' || @$spouse['smoker'] == null ) && $value->is_smoker ==  'no') {
                $tobacco = 2;
            }
        }else{
            if($request->has('applicant_dob') && ($request->has('spouse_dob') || !empty($child))){
               $tobacco =4;
            }else{
                $tobacco = 1;
            }
        }
       }                   
         }                           
        $max_age = ApplicationDetails::whereapplication_id($request->application_id)->max('age');
        $plans = Plan::whereIn('id' , $request->plan_id)->with('subplan','subplan.subplanded','subplan.rate')->get();
        //dd(count($plans));
        foreach ($plans as $key => $value) {
                
                foreach ($value->subplan as $key1 => $sp) {
                      $rate = Rate::where('SUBPLAN_ID',$sp->id)->where('RATE_AGE_MAX_I',$max_age)->first();
                      if($tobacco == 1){
                        $premium = $rate->RATE_SINGLE_C;
                        $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 2) {
                          $premium = $rate->RATE_SINGLE_TOBACCO_C;

                          $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 3) {
                          $premium = $rate->RATE_FAMILY_TOBACCO_C;
                          $value->subplan[$key1]->premium = $premium;
                      }elseif ($tobacco == 4){
                          $premium = $rate->RATE_FAMILY_C;
                          $value->subplan[$key1]->premium = $premium;
                      }else{
                          $premium = $rate->RATE_SINGLE_C;
                          $value->subplan[$key1]->premium = $premium;
                      }
                      
                }

                $plans[$key]->premium = round($value->subplan[$key1]->premium);
            }

        return view('broker.page.compare', compact('data','self','spouse','child','plans','broker','county' ,'zipcode'));
    }

   public function age($dob)
   {
            $userDob = $dob;
            $dob = new DateTime($userDob);
            $now = new DateTime();
            $difference = $now->diff($dob);
            $age = $difference->y;
            //dd($age);
   }
    public function userinfo(Request $request)
	{
		$url = $request->url();

		$uri_segments = explode('/', $url);
		$str = $uri_segments[2];
		$broker_name = strtok($str, '.');

		//$userinfo = new Userinfo;
		$broker_website = Userinfo::where('website', $broker_name)->first();
		$state_id = $broker_website->state_id;
		$postal_code = State::where('id', $state_id)->first();
		$state_name = $postal_code->postal_code;
	
		$data = array();
		$data['state_name'] = $state_name;
		$data['zipcode'] = $request->zipcode;
		$data['county'] = $request->county;
		$data['id'] = $request->id;
		$data['name'] = $request->name;
		$data['price'] = $request->price;
		return view('broker.page.userinfo', compact('data'));
	}
	public function apply_online(Request $request)
	{
		$data = array();
		
		$data['firstname'] = $request->firstname;
		$data['lastname'] = $request->lastname;
		$data['email'] = $request->email;
		$data['phone'] = $request->phone;
		$data['address1'] = $request->address1;
		$data['address2'] = $request->address2;
		$data['city'] = $request->city;
		$data['state'] = $request->state;
		$data['plan_effective'] = $request->plan_effective;
		$data['zipcode'] = $request->zipcode;
		$data['country'] = $request->country;
		$data['id'] = $request->id;
		$data['name'] = $request->name;
		$data['price'] = $request->price;
		
		$applicant_info = new Applicant_info;
		
		$applicant_info->firstname = $data['firstname'];
		$applicant_info->lastname = $data['lastname'];
		$applicant_info->email = $data['email'];
		$applicant_info->phone = $data['phone'];
		$applicant_info->address1 = $data['address1'];
		$applicant_info->address2 = $data['address2'];
		$applicant_info->city = $data['city'];
		$applicant_info->state = $data['state'];
		$applicant_info->plan_effective = $data['plan_effective'];
		$applicant_info->zipcode = $data['zipcode'];
		$applicant_info->country = $data['country'];
		$applicant_info->plan_id = $data['id'];
		$applicant_info->plan_name = $data['name'];
		$applicant_info->plan_price = $data['price'];
		
		$applicant_info->save();
		
		return view('broker.page.viewinfo', compact('data'));

	}
	
}
