<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use App\Helpers\Helper;
use App\Broker;
use App\BrokerSubscriptionPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\AuthorizeNetController as AuthorizeNet;

class PaymentController extends Controller
{
    public function my_simple_crypt( $string, $action = 'e' ) {
        $secret_key = 'erwwerfe54235wfwer2w34523';
        $secret_iv = 'sfdgsdgsd345345345sfdsfsdadf';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
        return $output;
    }
    public function authenticate(Request $request){
    	$this->validate($request,[
    		'card_name' => 'required|max:50',
    		'card_number' => 'required|integer|digits_between:12,16',
    		'year' => 'required',
    		'month' => 'required',
    		'cvv' => 'required|integer|digits_between:3,4',
            'broker_id' => 'required',
            'plan_type' => 'required|integer'
    	],[
    		'cvv.digits_between' => 'Enter a valid CVV',
    		'card_number.digits_between' => 'Enter a valid credit card number',
            'broker_id.required' => 'Invalid broker',
            'plan_type.required' => 'Please select a plan',
            'plan_type.required' => 'Selected plan invalid'
    	]);
        $broker_id = $this->my_simple_crypt($request->broker_id,'d');
        $broker = Broker::where('id',$broker_id)->where('status','UNAPPROVED')->where('payment_status','UNPAID')->first();
        if($broker){
            $broker_subscription_plan = BrokerSubscriptionPlan::first();
                if($request->plan_type == 1){
                    $amount = $broker_subscription_plan->amount;
                }else if($request->plan_type == 2){
                    $plan_amount = $broker_subscription_plan->amount;
                    $yearly_amount = $broker_subscription_plan->amount * 12;
                    $discount = round($yearly_amount * (10/100));
                    $yearly_discounted_amount = $yearly_amount - $discount;
                    $amount = $broker_subscription_plan->yearly_discounted_amount;
                }else{
                    return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error','Invalid plan');
                }
                if($broker_subscription_plan){
                    $request->merge([
                        'amount' => $amount
                ]);
                $process_payment = (new AuthorizeNet)->capture($request->all());
                if(isset($process_payment['transactionResponse']['responseCode']) && $process_payment['transactionResponse']['responseCode'] == 1){
                    $broker = Broker::where('id',$broker_id)->first();
                    $broker->broker_subscription_plan_id = $request->plan_type;
                    $broker->payment_status = 'PAID';
                    $broker->status = 'APPROVED';
                    $broker->subscription_date = date('Y-m-d');
                    if($request->plan_type == 1){
                        $broker->expiry_date = date('Y-m-d',strtotime('+30 days',strtotime(date('Y-m-d'))));
                    }else if($request->plan_type == 2){
                        $broker->expiry_date = date('Y-m-d',strtotime('+365 days',strtotime(date('Y-m-d'))));
                    }
                    $broker->plan_type = $request->plan_type;
                    $broker->subscription_amount = $amount;
                    if($broker->save()){
                        Helper::send_mail('register',$broker);
                        return redirect('/broker/login')->with('flash_success','User details are saved successfully, We will approve as soon as possible');
                    }else{
                        return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error','Something went wrong!');
                    }
                }else{
                    if(isset($process_payment['messages']['message'][0]['text'])){
                        return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error',$process_payment['messages']['message'][0]['text'].'Please check your card details');
                    }else if(isset($process_payment['error'])){
                        return redirect('/broker/subscription')->with('flash_error',$process_payment['error']);
                    }else{
                        return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error','Unauthenticated');
                    }
                }
            }
            else{
                return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error','Invalid subscription!');
            }
        }else{
            return redirect('/broker/subscription/'.$request->broker_id)->with('flash_error','Access Denied');
        }
    }
        
}
