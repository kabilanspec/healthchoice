<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Setting;

class AuthorizeNetController extends Controller
{
    public function init_curl($credentials,$data){
        if(!empty($credentials) && !empty($data)){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $credentials['authorize_params']['api']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            $headers = array('Content-Type: application/json');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $request = curl_exec($ch);
            curl_close($ch);
            //fix for UTF-8 BOM - Reference - https://stackoverflow.com/questions/689185/json-decode-returns-null-after-webservice-call
            $format_response = substr($request, 3);
            //fix ends
            $result = json_decode($format_response,true);
            return $result;
        }
    }
    public function getCredentials(){
    	$login_id = Setting::get('authorizenet_login_id',null);
    	$transaction_key = Setting::get('authorizenet_transaction_key',null);
    	$key = Setting::get('authorizenet_key',null);
    	$environment = Setting::get('authorizenet_environment',null);
    	$sandbox_api = Setting::get('authorizenet_sandbox_api',null);
    	$production_api = Setting::get('authorizenet_production_api',null);
    	$error = array();
    	$authorize_params = array();
    	if(is_null($login_id) || is_null($transaction_key) || is_null($key) || is_null($environment)){
    		if(is_null($login_id)){
    			$error[] = 'Invalid Authorizenet login ID';
    		}
    		if(is_null($transaction_key)){
    			$error[] = 'Invalid Authorizenet transaction key';
    		}
    		if(is_null($key)){
    			$error[] = 'Invalid Authorizenet key';
    		}
    		if(is_null($environment)){
    			$error[] = 'Invalid Authorizenet key';
    		}
    	} 
    	if(empty($error)){
    		$authorize_params['login_id'] = $login_id;
	    	$authorize_params['transaction_key'] = $transaction_key;
	    	$authorize_params['key'] = $key;
    		if($environment == 0){
    			$authorize_params['api'] = $sandbox_api;
    		}else if($environment == 1){
    			$authorize_params['api'] = $production_api;
    		}else{
    			return array('status' => 1,'errors' => $error,'authorize_params' => $authorize_params);
    		}
    		return array('status' => 0,'errors' => $error,'authorize_params' => $authorize_params);
    	}
    	return array('status' => 1,'errors' => $error,'authorize_params' => $authorize_params);
    }
    public function capture($data){
    	$authorizenet_credentials = $this->getCredentials();
    	if($authorizenet_credentials['status'] == 0 && empty($authorizenet_credentials['errors'])){
    		$params = array(
    			'createTransactionRequest' => array(
    				'merchantAuthentication' => array(
    					'name' => $authorizenet_credentials['authorize_params']['login_id'],
    					'transactionKey' => $authorizenet_credentials['authorize_params']['transaction_key'],
    				),
    				'refId' => rand(1000,9999),
    				'transactionRequest' => array(
    					'transactionType' => 'authCaptureTransaction',
    					'amount' => $data['amount'],
    					'payment' => array(
    						'creditCard' => array(
    							'cardNumber' => $data['card_number'],
    							'expirationDate' => $data['year'].'-'.$data['month'],
    							'cardCode' =>  $data['cvv']
    						),
    					),
    				),
    			),
    		);
    		$response = $this->init_curl($authorizenet_credentials,$params);
            return $response;
    	}else{
            return array('status' => 1,'errors' => 'Invalid credentials');
        }
    }
}

