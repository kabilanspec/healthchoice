<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Auth;
use Setting;
use Exception;
use \Carbon\Carbon;
use App\User;
use App\State;
use App\Admin;
use App\Broker;
use App\BrokerCarrier;
use App\Carrier;
use App\BrokerSubscriptionPlan;
use DB;
use Session;


class BrokerController extends Controller
{
    public function __construct(){
       $this->middleware('broker');
    }

    public function dashboard($carrier_type = null){
        $broker = Broker::where('id',Auth::guard('broker')->user()->id)->first();
        return view('broker.dashboard',compact('broker','carrier_type'));
    }
    public function profile(){
        $state = State::where('status','active')->get();
        $carriers = Carrier::where('status','active')->get(); 
        $broker = Broker::where('id',Auth::guard('broker')->user()->id)->first();
        return view('broker.profile',compact('state','carriers','broker','carrier_ids'));
    }
    public function show(){
        $state = State::where('status','active')->get();
        $broker = Broker::where('id',Auth::guard('broker')->user()->id)->first();
        return view('broker.show',compact('state','carriers','broker','carrier_ids'));
    }
    public function change_password(Request $request){
        $this->validate($request,[
            'password' => 'required|min:6|confirmed',
        ],[
            'password.required' => 'Password is missing',
            'password_confirmation.required' => 'Confirm password is missing',
            'password.confirmed' => 'Password mismatch',
        ]);
        $broker = Broker::where('id',Auth::guard('broker')->user()->id)->first();
        if($broker){
            $broker->password = bcrypt($request->password);
            if($broker->save()){
                return redirect('/broker/logout')->with('flash_success','Password Changed Successfully');
            }else{
                return back()->with('flash_error','Sorry!Something went wrong');
            }
        }else{
            return back()->with('flash_error','Invalid Access!');
        }
    }
    public static function upload_picture($picture)
    {
        $file_name = time();
        $file_name .= rand();
        $file_name = sha1($file_name);
        if ($picture) {
            $ext = $picture->getClientOriginalExtension();
            $picture->move(public_path() . "/uploads", $file_name . "." . $ext);
            $local_url = $file_name . "." . $ext;

            $s3_url = url('/').'/uploads/'.$local_url;
            
            return $s3_url;
        }
        return "";
    }
     public function update_profile(Request $request){
        $this->validate($request,[
            //'agency_name' => 'required|max:50',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|max:50|unique:brokers,email,'.Auth::guard('broker')->user()->id,
            'agency_address_line_1' => 'required',
            'city' => 'required|max:50',
            'state_id' => 'required',
            'phone' => 'required|integer|digits:10|unique:brokers,phone,'.Auth::guard('broker')->user()->id,
            //'website' => 'required|unique:brokers,website,'.Auth::guard('broker')->user()->id,
            'avatar' => 'sometimes|mimes:jpeg,png|max:1024',
            'zipcode' => 'required|integer'
        ],[
            //'agency_name.required' => 'Please fill out this field',
            'first_name.required' => 'Please fill out this field',
            'last_name.required' => 'Please fill out this field',
            //'email.required' => 'Please fill out this field',
            'agency_address_line_1.required' => 'Please fill out this field',
            'city.required' => 'Please fill out this field',
            'phone.required' => 'Please fill out this field',
            //'website.required' => 'Please fill out this field',
            'state_id.required' => 'Please fill out this field',
            //'carrier_id.present' => 'Please select a carrier',
            //'carrier_id.array' => 'Carrier is invalid',  
            'phone.unique' => 'Phone number already taken',
            //'website.unique' => 'Broker URL already taken',
            //'email.unique' => 'Email already taken',
            //'email.email' => 'Email is not valid',
            'phone.integer' => 'Phone number must be valid and must not start with 0',
            'phone.digits' => 'Phone number must be 10 digit',
            'avatar.mimes' => 'Image should be jpeg/jpg/png',
        ]);
        $broker = Broker::where('id',Auth::guard('broker')->user()->id)->first();
        if($broker){
            //$broker->agency_name = $request->agency_name;
            $broker->first_name = $request->first_name;
            $broker->last_name = $request->last_name;
            $broker->zipcode = $request->zipcode;
            $broker->email = $request->email;
            $broker->agency_address_line_1 = $request->agency_address_line_1;
            $broker->agency_address_line_2 = ($request->agency_address_line_2) ? $request->agency_address_line_2 : NULL;
            $broker->city = $request->city;
            $broker->phone = $request->phone;
            $broker->phone_tollfree = ($request->phone_tollfree) ? $request->phone_tollfree : NULL;
            //$broker->website = $request->website;
            $broker->state_id = $request->state_id;
            if($request->has('avatar') && !empty($request->avatar)){
                $broker->avatar = $this->upload_picture($request->avatar);
            }
            $broker->save();

            $broker_carriers = BrokerCarrier::where('broker_id',$broker->id)->get();
            foreach($broker_carriers as $broker_carrier){
                if(!in_array($broker_carrier->carrier_id,empty($request->carrier_id) ? array() : $request->carrier_id)){
                    BrokerCarrier::where('id',$broker_carrier->id)->delete();
                }
            }
            // $delete_carriers = array();
            if($request->has('carrier_id') && !empty($request->carrier_id)){
                foreach ($request->carrier_id as $key => $value) {
                    $broker_carrier = BrokerCarrier::where('broker_id',$broker->id)->where('carrier_id',$value)->where('state_id',$request->state_id)->first();
                    if(!$broker_carrier){
                        $carrier = new BrokerCarrier;
                        $carrier->broker_id = $broker->id;
                        $carrier->carrier_id =  $value;
                        $carrier->state_id = $request->state_id;
                        $carrier->save();
                    }
                }
            }
            return back()->with('flash_success','Profile updated successfully!');
        }else{
            return back()->with('flash_error','Sorry!Something went wrong');
        }
    }

    public function quote()
    {
    	
    	return view('broker.quote');
    }
    
}
