<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BrokerSubscriptionPlan;

class BrokerSubscriptionPlanController extends Controller
{
    public function index(){
    	$broker_subscription_plans = BrokerSubscriptionPlan::where('is_deleted',0)->get();
    	return view('admin.broker_subscription_plans.index',compact('broker_subscription_plans'));
    }
    public function show(){
    	return view('admin.broker_subscription_plans.show');
    }
    public function store(Request $request){
    	$this->validate($request,[
    		'name' => 'required',
    		'amount' => 'required|integer'
    	]);
        $request->merge(['is_deleted' => 0]);
    	$broker_subscription_plan = BrokerSubscriptionPlan::create($request->all());
    	if($broker_subscription_plan){
    		return redirect()->back()->with('flash_success','Broker subscription plan created successfully!');
    	}else{
    		return redirect()->back()->with('flash_error','Oops!Something went wrong.');
    	}
    }
    public function edit($id){
    	$broker_subscription_plan = BrokerSubscriptionPlan::where('id',$id)->first();
    	if($broker_subscription_plan){
    		return view('admin.broker_subscription_plans.edit',compact('broker_subscription_plan'));
    	}else{
    		return redirect()->back()->with('flash_error','Invalid action.Access denied!');
    	}
    }
    public function update(Request $request,$id){
    	$this->validate($request,[
    		'name' => 'required',
    		'amount' => 'required|integer'
    	]);
    	$broker_subscription_plan = BrokerSubscriptionPlan::where('id',$id)->first();
    	if($broker_subscription_plan){
    		$broker_subscription_plan->name = $request->name;
    		$broker_subscription_plan->amount = $request->amount;
    		$broker_subscription_plan->description = $request->description;
    		if($broker_subscription_plan->save()){
    			return redirect()->back()->with('flash_success','Broker subscription plan updated successfully!');
    		}else{
	    		return redirect()->back()->with('flash_error','Oops!Something went wrong.');
	    	}
    	}else{
    		return redirect()->back()->with('flash_error','Invalid action.Access denied!');
    	}
    }
    public function destroy($id){
    	$broker_subscription_plan = BrokerSubscriptionPlan::where('id',$id)->first();
    	if($broker_subscription_plan){
    		$broker_subscription_plan->is_deleted = 1;
    		if($broker_subscription_plan->save()){
    			return redirect()->back()->with('flash_success','Broker subscription plan deleted successfully!');
    		}else{
	    		return redirect()->back()->with('flash_error','Oops!Something went wrong.');
	    	}
    	}else{
    		return redirect()->back()->with('flash_error','Invalid action.Access denied!');
    	}
    }
}
