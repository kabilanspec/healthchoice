<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use Auth;
use Setting;
use Exception;
use \Carbon\Carbon;
use App\User;
use App\State;
use App\Broker;
use App\Admin;
use App\Carrier;
use App\StateCarrier;
use App\BrokerCarrier;
use App\Application;
use App\ApplicationDetails;
use DB;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
        $this->middleware('demo', ['only' => [
                'settings_store', 
                'profile_update',
                'password_update',
        
            ]]);
        $this->perpage = Setting::get('per_page', '10');
    }


    /**
     * Dashboard.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {

        try{
              $brokers = Broker::with('state')->where('payment_status','PAID')->get();
              return view('admin.broker.index',compact('brokers'));
        }
        catch(Exception $e){
            return redirect()->route('admin.user.index')->with('flash_error','Something Went Wrong with Dashboard!');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        return view('admin.settings.application');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function settings_store(Request $request)
    {
        
        /*$this->validate($request,[
                'site_title' => 'required',
                'site_icon' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'site_logo' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            ]);*/

        if($request->hasFile('site_icon')) {
            $site_icon = Helper::upload_picture($request->file('site_icon'));
            Setting::set('site_icon', $site_icon);
        }

        if($request->hasFile('site_logo')) {
            $site_logo = Helper::upload_picture($request->file('site_logo'));
            Setting::set('site_logo', $site_logo);
        }

        if($request->hasFile('site_email_logo')) {
            $site_email_logo = Helper::upload_picture($request->file('site_email_logo'));
            Setting::set('site_email_logo', $site_email_logo);
        }

        if($request->has('site_title')) {
            Setting::set('site_title', $request->site_title);
            Setting::set('sos_number', $request->sos_number);
            Setting::set('contact_number', $request->contact_number);
            Setting::set('contact_email', $request->contact_email);
            Setting::set('site_copyright', $request->site_copyright);  
        }    

        if($request->has('social_login')) {
            Setting::set('social_login', $request->social_login);
            Setting::set('store_link_android_user', $request->store_link_android_user);
            Setting::set('store_link_android_provider', $request->store_link_android_provider);
            Setting::set('store_link_ios_user', $request->store_link_ios_user);
            Setting::set('store_link_ios_provider', $request->store_link_ios_provider);
            Setting::set('store_facebook_link', $request->store_facebook_link);
            Setting::set('store_twitter_link', $request->store_twitter_link);
        }     

        if($request->has('provider_select_timeout')) {
            Setting::set('provider_select_timeout', $request->provider_select_timeout);
            Setting::set('provider_search_radius', $request->provider_search_radius);
            Setting::set('manual_request', $request->manual_request == 'on' ? 1 : 0 );
            Setting::set('broadcast_request', $request->broadcast_request == 'on' ? 1 : 0 );
            Setting::set('track_distance', $request->track_distance == 'on' ? 1 : 0 );
            Setting::set('distance', $request->distance);
        }

        if($request->has('map_key')) {
            Setting::set('map_key', $request->map_key);
            Setting::set('fb_app_version', $request->fb_app_version);
            Setting::set('fb_app_id', $request->fb_app_id);
            Setting::set('fb_app_secret', $request->fb_app_secret);
        }

        if($request->has('mail_driver')) {
            Setting::set('send_email', $request->send_email == 'on' ? 1 : 0);
            Setting::set('mail_driver', $request->send_email == 'on' ? $request->mail_driver : '');
            Setting::set('mail_host', $request->send_email == 'on' ? $request->mail_host : '');
            Setting::set('mail_username', $request->send_email == 'on' ? $request->mail_username : '');
            Setting::set('mail_password', $request->send_email == 'on' ? $request->mail_password : '');
            Setting::set('mail_from_address', $request->send_email == 'on' ? $request->mail_from_address : '');
            Setting::set('mail_from_name', $request->send_email == 'on' ? $request->mail_from_name : '');
            Setting::set('mail_port', $request->send_email == 'on' ? $request->mail_port : '');
            Setting::set('mail_encryption', $request->send_email == 'on' ? $request->mail_encryption : '');
            Setting::set('mail_domain', $request->send_email == 'on' ? $request->mail_domain : '');
            Setting::set('mail_secret', $request->send_email == 'on' ? $request->mail_secret : '');
        }    

        if($request->has('referral_count')) {                        
            Setting::set('referral', $request->referral == 'on' ? 1 : 0);
            Setting::set('referral_amount', $request->referral == 'on' ? $request->referral_amount : 0);
            Setting::set('referral_count', $request->referral == 'on' ? $request->referral_count : 0);
        }

        Setting::save();
        
        return back()->with('flash_success','Settings Updated Successfully');
    }

    public function autocomplete(Request $request)
    {


      $term=$request->term;
     
      $data=State::where('full_name','LIKE','%'.$term.'%')
                     
                           ->get();
      $results=array();

      foreach ($data as $key => $v) {

          $results[]=['id'=>$v->id,'value'=>$v->full_name];

      }
      
      return response()->json($results);
            
    }

    //states
    public function states()
    {
        $states = State::all();
        return view('admin.states',compact('states'));
     }
     //carriers
     public function carrier_index()
     {
        $carrier = Carrier::with('statecarrier','statecarrier.state')->get();

        $states = State::where('status','active')->get();
        return view('admin.carrier',compact('carrier','states'));

     }

     public function carrier_store(Request $request)
     {
       $carrier = Carrier::firstOrNew(['carrier'=>$request->carrier]);
       $carrier->description = $request->description;
       $carrier->save();

       return back()->with('flash_success','Settings Updated Successfully');
     }

     public function sc_delete($id)
     {
        
        $sc = StateCarrier::whereid($id)->Delete();
        return back()->with('flash_success','Deleted Successfully');
     }

     public function  lists()
     {
        $carrier = Carrier::get();
        return  $carrier;
     }

     public function state_carrier(Request $request)
     {
         
           $state_carrier = StateCarrier::firstOrNew(['carrier_id'=>$request->carrier_id,'state_id'=>$request->state_id]);
           
           $state_carrier->save();

        /*$state_carrier = new StateCarrier;
        $state_carrier->carrier_id = $request->carrier_id;
        $state_carrier->state_id = $request->state_id;
        $state_carrier->save();*/

        return redirect()->back()->with('flash_success','State Added');
     }

     public function state_post_status($status){
      
      $id =  preg_replace('/[^0-9]/','',$status);
      $result = preg_replace("/[^a-zA-Z]+/", "", $status);
      $post = State::whereid($id)->first();
      $post->status = $result;
      $post->save();

      return back()->with('flash_success',"Post Updated Successsfully");
    }

     public function post_status($status){
      
      $id =  preg_replace('/[^0-9]/','',$status);
      $result = preg_replace("/[^a-zA-Z]+/", "", $status);
      $post = Carrier::whereid($id)->first();
      $post->status = $result;
      $post->save();

      return back()->with('flash_success',"Post Updated Successsfully");
    }

    //Broker List
    public function broker_list(){

        $brokers = Broker::with('state','brokerc','brokerc.carrier')->where('payment_status','PAID')->get();
        return view('admin.broker.index',compact('brokers'));
    }

    //Add Broker View
    public function broker_create(){
         
   
         $state = State::where('status','active')->get();
        $carrier = Carrier::where('status','active')->get(); 
        return view('admin.broker.create',compact('state','carrier'));
    }

    //Store Broker Data
    public function broker_store(Request $request){

         $this->validate($request,[
                'website' => 'required',
         ]);

        $broker = $request->all();
        $broker['status'] = 'UNAPPROVED';
        $broker = Broker::create($broker);

        foreach ($request->carrier_id as $key => $value) {
           
            $bc = new BrokerCarrier;
            $bc->broker_id = $broker->id;
            $bc->carrier_id = $value;
            $bc->status = 0;
            $bc->save();
        }
        
         return redirect()->back()->with('flash_success','Broker subscription plan created successfully!');
    }

    public function broker_edit($id)
    {
        
         $state = State::where('status','active')->get();
         $carrier = Carrier::where('status','active')->get(); 
         $broker = Broker::whereid($id)->with('state','brokerc','brokerc.carrier')->first();
         return view('admin.broker.edit',compact('state','carrier','broker','id'));
    }

    public function broker_update(Request $request)
    {

        $Broker = Broker::whereid($request->broker_id)->first();
        if ($request->has('first_name')) {
            $Broker->first_name = $request->first_name;
        }
        if ($request->has('last_name')) {
            $Broker->last_name = $request->last_name;
        }
        if ($request->has('email')) {
            $Broker->email = $request->email;
        }
        if ($request->has('phone')) {
            $Broker->phone = $request->phone;
        }
        if ($request->has('agency_address_line_1')) {
            $Broker->agency_address_line_1 = $request->agency_address_line_1;
        }
        if ($request->has('agency_address_line_2')) {
            $Broker->agency_address_line_2 = $request->agency_address_line_2;
        }
        if ($request->has('phone_tollfree')) {
            $Broker->phone_tollfree = $request->phone_tollfree;
        }
        if ($request->has('city')) {
            $Broker->city = $request->city;
        }
        if ($request->has('state_id')) {
            $Broker->state_id = $request->state_id;
        }
        $Broker->save();

        return back()->with('flash_success',"Broker Updated Successsfully");
    }

     public function broker_destroy($id)
    {

        try {
            Broker::find($id)->delete();
            return back()->with('message', 'Broker deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Provider Not Found');
        }
    }

    public  function broker_post_status($status){
      
      $id =  preg_replace('/[^0-9]/','',$status);
      $result = preg_replace("/[^a-zA-Z]+/", "", $status);

      $post = Broker::whereid($id)->first();
      $post->status = $result;
      $post->save();

      return back()->with('flash_success',"Status Updated Successsfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view('admin.account.change-password');
    }

    //Application  Logs

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        try {

           $Admin = Admin::find(Auth::guard('admin')->user()->id);

            if(password_verify($request->old_password, $Admin->password))
            {
                $Admin->password = bcrypt($request->password);
                $Admin->save();

                return redirect()->back()->with('flash_success','Password Updated');
            }
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }



}
