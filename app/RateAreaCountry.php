<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RateAreaCountry extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'RATE_AREA_ID',
        'COUNTY_ID',
        'SPECIFIC_ZIP_ONLY_T'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];


}
