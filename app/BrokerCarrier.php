<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerCarrier extends Model
{
    protected $fillable = [
    	'broker_id',
    	'carrier_id',
    	'status'
    ];

    protected $hidden = [
    	'created_at',
    	'updated_at'
    ];

    public function state()
    {
        return $this->hasMany('App\StateCarrier','carrier_id');
    }
    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }
}
