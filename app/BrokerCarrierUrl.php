<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerCarrierUrl extends Model
{
    protected $fillable = [
    	'enrollment',
    	'carrier_url',
    	'broker_id',
    	'state_id',
    	'carrier_id'
    ];
}
