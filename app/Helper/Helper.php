<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Mail;

class Helper
{
    public static function send_mail($type,$detail){
    	if($type == 'register'){
    		Mail::send('emails.register', ['detail' => $detail], function ($mail) use ($type,$detail) {
	            $mail->to($detail->email, $detail->first_name.' '.$detail->last_name)->subject('Registration');
	        });
    	}
        return true;
    }
}


