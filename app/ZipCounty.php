<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCounty extends Model
{
     protected $fillable = [
        'COUNTY_ID','STATE_ID','CITY_T','ZIP_CODE_T'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
