<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPlanDed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'SUBPLAN_ID',
        'DEDUCTIBLE_SINGLE_C',
        'DEDUCTIBLE_FAMILY_C',
        'DEDUCTIBLE_EMBEDDED_B',
        'OOP_SINGLE_C',
        'OOP_INDIVIDUAL_C', 
        'OOP_FAMILY_C',
     
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];


}
