<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerSubscriptionPlan extends Model
{
	protected $table = 'broker_subscription_plans';

    protected $fillable = [
    	'name',
    	'amount',
    	'description',
    	'is_deleted'
    ];

    protected $hidden = [
    	'created_at',
    	'updated_at',
    	'is_deleted'
    ];
}
