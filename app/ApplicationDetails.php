<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicationDetails extends Model
{
    protected $fillable = [
        'application_id', 'dob','gender','is_smoker','who','status','age'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
