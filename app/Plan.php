<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'PRODUCT_ID',
        'CARRIER_ID',
        'HIOS_PRODUCT_ID',
        'PLAN_NAME_T',
        'MAX_DEP_2PARENT_I',
        'MAX_DEP_1PARENT_I', 
        'DEP_STARTING_AGE_I',
        'DEP_MAX_AGE_I',
        'MAX_CHILD_ONLY_I',
        'DOM_PART_SAME_SUBSCRIBER_B',
        'SAME_SEX_SAME_SUBSCRIBER_B' ,
        'FAMILY_START_AT_I',
        'PROVIDER_LOOKUP_URL_M', 
        'FORMULARY_LOOKUP_URL_M'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function subplan()
    {
        return $this->hasMany('App\SubPlan','PLAN_ID');
    }

    public function carrier()
    {
        return $this->belongsTo('App\Carrier','CARRIER_ID');
    }


}
