<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'AdminController@broker_list')->name('broker.index');
Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
Route::get('/autocomplete', 'AdminController@autocomplete')->name('autocomplete');

Route::resource('user', 'Resource\UserResource');

Route::get('site/settings', 'AdminController@settings')->name('settings');
Route::post('settings/store', 'AdminController@settings_store')->name('settings.store');

//states
Route::get('states', 'AdminController@states')->name('states');
Route::get('state/change/status/{status}', 'AdminController@state_post_status')->name('state.post.status');

//carriers
Route::get('/carriers/list', 'AdminController@carrier_index')->name('carrier.index');
Route::post('/carriers/store', 'AdminController@carrier_store')->name('carrier.store');

Route::get('/delete/state/carrier/{id}', 'AdminController@sc_delete')->name('broker.delete');

Route::get('/lists', 'AdminController@lists')->name('lists');

Route::post('/carriers/state', 'AdminController@state_carrier')->name('statecarrier');
Route::get('carrier/change/status/{status}', 'AdminController@post_status')->name('post.status');

Route::get('/broker/list', 'AdminController@broker_list')->name('broker.index');
Route::get('/add/broker', 'AdminController@broker_create')->name('broker.create');
Route::post('/broker/store', 'AdminController@broker_store')->name('broker.store');
Route::get('/edit/broker/{id}', 'AdminController@broker_edit')->name('broker.edit');
Route::post('/broker/update', 'AdminController@broker_update')->name('broker.update');
Route::delete('/broker/delete/{id}', 'AdminController@broker_destroy')->name('broker.destroy');
Route::get('broker/change/status/{status}', 'AdminController@broker_post_status')->name('broker.post.status');
//Broker

//broker subscription plans
Route::get('/broker_subscription_plan', 'BrokerSubscriptionPlanController@index')->name('broker_subscription_plan');
Route::get('/broker_subscription_plan/show', 'BrokerSubscriptionPlanController@show')->name('broker_subscription_plan.show');
Route::post('/broker_subscription_plan/store', 'BrokerSubscriptionPlanController@store')->name('broker_subscription_plan.store');
Route::get('/broker_subscription_plan/edit/{id}', 'BrokerSubscriptionPlanController@edit')->name('broker_subscription_plan.edit');
Route::post('/broker_subscription_plan/update/{id}', 'BrokerSubscriptionPlanController@update')->name('broker_subscription_plan.update');
Route::get('/broker_subscription_plan/destroy/{id}', 'BrokerSubscriptionPlanController@destroy')->name('broker_subscription_plan.delete');
//ends

