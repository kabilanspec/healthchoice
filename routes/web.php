<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/
/*Route::get('/broker/page/dashboard', 'BrokerPageController@dashboard')->name('broker.page.dashboard');*/
Route::get('/broker/page/effectiveplan', 'BrokerPageController@effectiveplan')->name('broker.page.effectiveplan');
Route::get('/broker/page/store', 'BrokerPageController@store')->name('broker.page.store');
Route::get('/broker/page/county', 'BrokerPageController@county')->name('broker.page.county');
Route::get('/broker/page/todo', 'BrokerPageController@todo')->name('broker.page.todo');
Route::get('/broker/page/applicant', 'BrokerPageController@applicant')->name('broker.page.applicant');
Route::post('/broker/page/compare', 'BrokerPageController@compare')->name('broker.page.compare');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//under testing
Route::get('/capture', 'AuthorizeNetController@capture');


/*
|--------------------------------------------------------------------------
| Admin Authentication Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin'], function () {
	Route::get('/', 'AdminAuth\LoginController@showLoginForm');
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::get('/logout', 'AdminAuth\LoginController@logout');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'broker'], function () {
    
    Route::get('/dashboard', 'BrokerCarrierManagementController@dashboard');
    Route::post('/change_password', 'BrokerController@change_password');
	Route::get('/', 'BrokerAuth\LoginController@showLoginForm');
    Route::get('/login', 'BrokerAuth\LoginController@showLoginForm')->name('broker.login');
    Route::post('/login', 'BrokerAuth\LoginController@login');
    Route::get('/logout', 'BrokerAuth\LoginController@logout');
    Route::get('/register', 'BrokerAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'BrokerAuth\RegisterController@register')->name('broker.register');

    Route::post('/authenticate', 'PaymentController@authenticate');
    Route::get('/profile', 'BrokerController@profile');
    Route::post('/update_profile', 'BrokerController@update_profile');
    Route::get('/update_profile', 'BrokerController@show');

    Route::get('/subscription/{id}', 'BrokerAuth\RegisterController@subscription');

    Route::post('/carrier/create', 'BrokerCarrierManagementController@store');

    Route::post('/carrier_management/{id}', 'BrokerCarrierManagementController@update');
    Route::get('/carrier_management/{id}', 'BrokerCarrierManagementController@destroy');




    Route::post('/password/email', 'BrokerAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'BrokerAuth\ResetPasswordController@reset');
    Route::get('/password/reset', 'BrokerAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'BrokerAuth\ResetPasswordController@showResetForm');
    
});



Route::group(array('domain' => 'ichra.online'), function()
{
   Route::get('/', function () {
       return view('index');
   });
});

Route::group(array('domain' => '{subdomain}.ichra.online'), function () {

    Route::get('/', function ($subdomain) {
         
         $broker  =  \App\Broker::with('brokerc','brokerc.carrier')->wherewebsite($subdomain)->first();
         
         if($broker){
            return view('broker.page.dashboard',compact('broker'));
        }else{
          
               return view('index');
        }
         

    });
     Route::get('/broker/page/plan', 'BrokerPageController@plan')->name('broker.page.plan');
    Route::get('/broker/page/quote', 'BrokerPageController@quote')->name('broker.page.quote');
    //Route::post('/broker/page/county', 'BrokerPageController@county')->name('broker.page.county');
       
});


/*Route::group(array('domain' => '{subdomain}.localhost'), function () {

    Route::get('/', function ($subdomain) {
         
         $broker  =  \App\Broker::with('brokerc','brokerc.carrier')->wherewebsite($subdomain)->first();
         
         if($broker){
            return view('broker.page.dashboard',compact('broker'));
        }else{
          
               return view('index');
        }
         

    });
    Route::get('/broker/page/quote', 'BrokerPageController@quote')->name('broker.page.quote');
    
       
});*/

